# Gemu
## Music Generator
### Introduction and motivations
This project is meant to have a program that is able to compose music.

Of course, this is a difficult thing to do, not because of the programming issues but for the definition itself of what 
is music, good music, bad music, or expressive music.

Another motivation is to learn things myself. In order to teach a program about specific definitions about music, 
I have to learn them first. And by the moment I already learned some interesting things.

### Goals and achievements

I have some ideas of where to go next, but I also choose some on the fly. This program achieves to:


- [x] Output sound. Be able to write WAV files transforming the logical data into audible sound.

- [x] Name to note to pitch to sound wave mapping.

- [x] Differentiate if a note belongs or not to a basic [diatonic](https://en.wikipedia.org/wiki/Diatonic_scale) scale.

- [x] Generate and manipulate different [timbres](https://en.wikipedia.org/wiki/Timbre#Harmonics) (harmonic patterns, waveforms).

- [x] Generate lists of notes, more or less randomized in pitch and length. [Example song](https://dl.dropboxusercontent.com/u/44268373/mp3/purple_magician_from_the_school_of_pain.mp3).

- [x] Complex notes, with (constant or linear) interpolation of volume, pitch and timbre.

- [ ] Volume (or time) [envelope](https://en.wikipedia.org/wiki/Synthesizer#ADSR_envelope) (requires more-than-linear interpolation). 

- [x] Export notes to lilypond format, to take pdf partiture and midi output.

- [x] Define consonance/dissonance. How to measure if two simultaneous notes sound good or bad. Achieved but very poorly :( 

- [x] Generate lists of chords fitting a given consonance target function.

- [ ] Improve timbre quality. This may require to replicate midi sound libraries, so perhaps doesn't worth it.

- [ ] Add recurrence (repeating patterns (of pitch and/or rhythm) already present in the song).

- [x] Add runtime block sending, to use as a library. Like `gemu.getNextWavBuffer(seconds)`. Possibly with limited memory and overwriting old buffers.
    - **update**: finally made as `SoundStreamWriter(sampleRate).write(music.getSong());` which reproduces it directly.

- [x] Add support to [oneliners](https://www.youtube.com/watch?v=qlrs2Vorw2Y). [Yeah](https://bitbucket.org/jmmut/gemu/src/d07aba76c7547863ad8e4d6c902d2de51ec75e08/src/core/main.cpp?at=develop&fileviewer=file-view-default#main.cpp-172)!

- [ ] Dominate human behaviour by handling their mood (for version 2.0).

### Installing and trying

To compile gemu, check that you have installed the requirements first.
#### Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)

#### Install

    git clone https://bitbucket.org/jmmut/gemu.git
    cd gemu
    mkdir build && cd build
    conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
    conan remote add jmmut https://api.bintray.com/conan/jmmut/commonlibs
    conan install .. --build missing
    cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug
    make


And that's it. In the bin directory there will be some testing programs.

    cd bin
    ./gemu


Executions like that will generate some files:

* `.wav`: playable audio. If you want to convert them to mp3, google is your [friend](http://audioformat.com/wav-to-mp3).
* `.ly`: lilypond text file, with `$ lilypond <name>.ly` you'll obtain the partiture and the midi file.
* `.txt`: human-readable music. It's not in any particular format, just easy to read.
* `.dat`: plotable data with `$ echo "plot \"<name>.dat\" w l" | gnuplot`. Usually sound waves or consonance evaluation. Realise that plotting huge files can be slow.
* `.mez`: old format of my own to regenerate the wav files. It was made in my student years so yes, it's so ugly I'm not even releasing it. I plan to be able to load these when gemu generates something valuable.

Try `./gemu --help` to see some options, and head over the [wiki](https://bitbucket.org/jmmut/gemu/wiki/Home) to know more about internals.
