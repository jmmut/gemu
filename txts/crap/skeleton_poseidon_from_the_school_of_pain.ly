\version "2.16.2"
{
<<
{ \tempo 4=120  }
{ \tempo 4=120  }
{ \tempo 4=120 f'2 c'2 f2 f2 f2 f'2 c'2 g2 f2 f'2 a'2 c'2 c'2 c'2 f2  }
{ \tempo 4=120 a''16 a''16 d'''16 c'''16 a''16 g''16 f''16 e''16 b'16 f'16 f'16 g16 f8 g16 f8 f8 f16 g16 f16 f16 c'16 a16 f16 f16 f8 f8 f16 f16 f16 f16 g8 f8 c'16 d'16 a16 f'16 c''16 g'8 f'8 g8 f8 f8 g8 a8 g8 f16 f16 f16 g16 b16 f'16 c'16 c'16 d'16 e'8 d'8 g8 f8 f8 f16 f16 f16 f16 f16 f16 g8 b8 a16 g16 f16 a16 g16 f16 f16 a16 g16 b8 a16 f16 f16 f16 a16 f16 g16 f16 f16 a16 f16 f16 f16 g16 f'16 a'16 d''16 e''16 b'16 e''16  }

>>
}