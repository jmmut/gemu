\version "2.16.2"
{
<<
{ \tempo 4=120 f'2 a2 g2 f2 c'2 a2 b2 e'2 e'2 f'2 d'2 c'2 c'2 f'2 g'2  }
{ \tempo 4=120 d''16 a'16 f''8 g''16 a''16 d''16 c''16 d'16 f'16 d'8 f'16 f'16 d'16 c'16 c'16 b16 c'16 a16 f16 f16 f16 f16 g16 g16 g16 f16 f16 f16 f16 f16 f16 f16 g16 a16 g16 a16 a16 a16 f16 g16 f16 c'8 g'16 c''16 c''16 c''16 e'8 f'16 f''16 f''16 f''16 c''16 b'16 d''16 d''16 c''8 e'8 e'16 g'16 g'16 f'16 d'8 a'8 g'16 f'16 d'16 e'16 d'16 a16 d'16 d'16 d'16 e'16 c'16 a16 g16 b16 a8 g8 f8 a8 d'16 b16 b16 b8 a16 f16 a16 g16 f16 b16 c'16 g16 d'16 d'16 g16 f16 f16 g16 f16 f16 g8 g8 f8  }

>>
}