\version "2.16.2"
{
<<
{ \tempo 4=120 d'2 c'2 d'8 g8 f8 f8 f4 f4 f4 a4 g4 g'4 d''4 f''4 f''4 g''4 g'''8 a'''8 a'''8 f'''8 a'''8 g'''8 g'''8 d'''8 g'''8 a'''8 g'''8 b''8 a''8 f''8 d''8 f''8 c''8 g''8 c'''8 c''8 a'8 g'8 g'8 c''4 a'4 e'8 a'8 a8  }
{ \tempo 4=120 c''8 f''8 f''8 c''8 f'8 e'8 c'8 c'8 f8 g8 g8 f4 b4 g8 f8 d'8 g'8 f'8 f'8 c'8 c'8 f8 f8 a8 b8 a8 d'8 b'8 f''8 f''8 f'4 f4 f4 f4 a4 g4 a8 d'8 d'4 g4 g'4 a'4 d''4 d''8 b'4 c''8  }

>>
}