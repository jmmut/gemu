\version "2.16.2"
{
<<
{ \tempo 4=120 g'2 d'2 d'2 d'2 f'2 a'2 e''2 c''2 a''2 d'''2 c'''2 g'''2 e'''2 b''2 g''2  }
{ \tempo 4=120 f''16 g''16 b''16 a''16 a''16 d'''16 f'''16 f'''16 g'''16 a'''16 f'''16 c'''16 f'''16 c'''16 c'''16 e'''16 f'''16 f'''16 e'''16 d'''16 d'''16 c'''16 f''16 d''16 f''16 g''16 c'''16 e'''16 a'''16 e'''16 d'''16 a''16 a''16 e'''16 f'''16 f'''16 f'''16 d'''16 d'''16 e'''16 a''16 a''8 b''8 a''8 f''8 g''16 f''16 g''16 a''16 e''16 d''8 e''8 f''16 g''16 g''16 e''16 a'8 f'8 f'8 e'8 a'8 g'16 d'16 c'16 a16 a16 a16 a16 a16 b16 d'16 c'16 g16 a16 f16 g16 f16 g16 a16 c'16 a16 a16 f'8 e'16 e'16 a'16 c''8 f''8 d''16 g''16 a''16 a''16 d''16 d''16 c''16 e''16 d''16 g'16 g'16 g'16 f'16 a'16 c''16 b'16 e''16 g''16  }

>>
}