\version "2.16.2"
{
<<
{ \tempo 4=120 f'2 a2 f2 g2 f2 f2 a2 a2 a2 g2  }
{ \tempo 4=120 d'16 d'16 d''16 d''16 b'8 a8 a16 d'16 a16 f16 g16 f'16 d''16 d''8 c'''8 b''8 a'16 b'16 g''16 e''16 b'8 a''8 c'''16 c''16 d''16 c''16 f'16 b'16 g'16 c'16 c'16 e'16 c''16 e'16 c'16 g16 f16 a16 d''8 f'16 g'16 b'16 c''16 b'8 d''8 a'16 b16 g8 f16 f16 f16 f8 f8 g8 f8 f8 f8 f8 d'8 d'8  }

>>
}