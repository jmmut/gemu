#!/bin/bash

# advice: run as `./runTests.sh 2> /dev/null | grep TestSuite` to view only summaries

cd bin/

./test.appender      2> /dev/null | grep "TestSuite: "
./test.iterator      2> /dev/null | grep "TestSuite: "
./test.entropy       2> /dev/null | grep "TestSuite: "
./test.interpolator  2> /dev/null | grep "TestSuite: "


