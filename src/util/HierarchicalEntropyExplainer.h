/**
 * @file HierarchicalEntropyExplainer.h
 * @author jmmut 
 * @date 2016-04-17.
 */

#ifndef GEMU_HIERARCHICALENTROPYEXPLAINER_H
#define GEMU_HIERARCHICALENTROPYEXPLAINER_H

// the next pragmas solve: boost/accumulators/statistics/times2_iterator.hpp:26:14: warning: ‘template<class _Operation> class std::binder1st’ is deprecated
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <boost/serialization/array_wrapper.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#pragma GCC diagnostic pop

#include "Walkers.h"

namespace randomize {

class HierarchicalEntropyExplainer {
public:

    HierarchicalEntropyExplainer(std::vector<double> entropies, int basicBlockLength)
            : entropies(entropies), basicBlockLength(basicBlockLength) {
    }

    struct Properties {
        double maxEntropy;
        double maxEntropyIndex;
        double minEntropy;
        double minEntropyIndex;
        /**
         * This is the scale of least entropy. The scale is returned as the number of elements in
         * the sequence that is repeated. a pattern of 0 0 0 0 1 1 1 1 will say 4
         */
        int mostStructuredScale;
        /**
         * Scale with most entropy. Least likely period to have structure
         */
        int leastStructuredScale;
        /**
         * The least and most structured scale can be contextualized with the next two values: average
         * and standard deviation across scales. if the difference from the minimum entropy to
         * the average is greater than a standard deviation, it is more extreme than 68.4 % of the other
         * scales. if it is greater than 2 deviations, it is more extreme than 95 % of the other scales
         */
        double averageEntropyAcrossScales;
        double deviationEntropyAcrossScales;
    };

    Properties computeProperties();
    void computeDescription(std::ostream &os);
private:
    Properties properties;
    std::vector<double> entropies;
    int basicBlockLength;

    void entropyAcrossScales();
    void minmaxEntropies();
    int scaleToPeriod(std::vector<double>::iterator scaleIndex);

    template<typename T>
    std::vector<size_t> sort_indexes(const std::vector<T> &v) {

        // initialize original index locations
        std::vector<size_t> idx(v.size());
        for (size_t i = 0; i != idx.size(); ++i) {
            idx[i] = i;
        }

        // sort indexes based on comparing values in v
        std::sort(idx.begin(), idx.end(),
                [&v](size_t i1, size_t i2) {
                    return v[i1] < v[i2];
                }
        );

        return idx;
    }
};

};  // randomize

#endif //GEMU_HIERARCHICALENTROPYEXPLAINER_H
