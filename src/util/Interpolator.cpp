#include <log/log.h>
#include "util/Interpolator.h"

namespace randomize {


LinearInterpolator::LinearInterpolator(std::initializer_list<double> points) {
    setPoints(points);
}



LinearInterpolator::LinearInterpolator(std::initializer_list<std::pair<double, double>> points) {
    setPoints(points);
}

void LinearInterpolator::setPoints(std::initializer_list<double> points) {
    size_t i = 0;
    for (auto point : points) {
        times.push_back(i / (points.size() - 1.0));
        values.push_back(point);
        i++;
    }
    init();
}

void LinearInterpolator::setPoints(std::initializer_list<std::pair<double, double>> points) {
    for (auto point : points) {
        times.push_back(point.first);
        values.push_back(point.second);
    }
    if (not std::is_sorted(times.begin(), times.end())) {
        throw utils::exception::StackTracedException(
                "LinearInterpolator must receive sorted points, not this:"
                        + utils::container_to_string(times));
    }
    init();
}

void LinearInterpolator::init() {
    size_t arity = values.size();
    if (arity == 0) {
        valueFunction = &LinearInterpolator::uninitialized;
    } else if (arity == 1) {
        valueFunction = &LinearInterpolator::constantValue;
    } else if (arity == 2) {
        valueFunction = &LinearInterpolator::simpleInterpolation;
    } else if (arity < MAX_POINTS) {
        valueFunction = &LinearInterpolator::complexInterpolation;
    } else {
        throw utils::exception::StackTracedException(
                "Can't initialize randomize::LinearInterpolator with"
                        + std::to_string(values.size()) + " points.");
    }
}

double LinearInterpolator::value(double point) {
    return (this->*valueFunction)(point);
}

double LinearInterpolator::uninitialized(double) {
    throw utils::exception::StackTracedException(
            "tried to use an uninitialized randomize::LinearInterpolator.");
}

double LinearInterpolator::constantValue(double) {
    return values[0];
}

double LinearInterpolator::simpleInterpolation(double time) {
    return (1 - time) * values[0] + time * values[1];
}

double LinearInterpolator::complexInterpolation(double time) {
    auto startTime = times.begin();
    auto finishTime = startTime +1;
    if (time > 0) {
        finishTime = std::lower_bound(times.begin(), times.end(), time);
        startTime = finishTime -1;
    }
    auto start = values.begin() + (startTime - times.begin());
    auto finish = values.begin() + (finishTime - times.begin());
    time = (time - *startTime) / (*finishTime - *startTime);

    return (1 - time) * (*start) + time * (*finish);
}

/*

void LinearInterpolator::func() {
    if (values.size() == 0) {
        valueFunction = nullptr;
    } else if (values.size() == 1) {
        valueFunction = &LinearInterpolator::ConstantValue;
    } else if (values.size() == 2) {
        valueFunction = &LinearInterpolator::LinearValue;
    } else if (values.size() == 4) {
        polynom[3] = 4.5*(-values[0] + 3*values[1] -3*values[2] + values[3]);
        polynom[2] = 3*values[0] -4.5*values[1] + 1.5*values[3] -4*polynom[3]/3;
        polynom[1] = -values[0] + values[3] -polynom[3] -polynom[2];
        polynom[0] = values[0];
        valueFunction = &LinearInterpolator::PolynomicValue;
    } else if (values.size() < MAX_POINTS) {
        valueFunction = &LinearInterpolator::BezierValue;
    } else {
        std::length_error e("Can't initialize randomize::LinearInterpolator with" + std::to_string(values.size()) + " points.");
        throw e;
    }
}
LinearInterpolator::LinearInterpolator2(std::initializer_list<std::pair<double, double>> points) {
    // if no points passed, no interpolation at max volume
    instants.push_back(0);
    values.push_back(1);
    for (auto point : points) {
        instants.push_back(point.first);
        values.push_back(point.second);
    }
    if (values.size() == 0) {
        valueFunction = nullptr;
    } else if (values.size() == 1) {
        valueFunction = &LinearInterpolator::ConstantValue;
    } else if (values.size() == 2) {
        valueFunction = &LinearInterpolator::LinearValue;
    } else if (values.size() == 4) {
        polynom[3] = 4.5 * (-values[0] + 3 * values[1] - 3 * values[2] + values[3]);
        polynom[2] = 3 * values[0] - 4.5 * values[1] + 1.5 * values[3] - 4 * polynom[3] / 3;
        polynom[1] = -values[0] + values[3] - polynom[3] - polynom[2];
        polynom[0] = values[0];
        valueFunction = &LinearInterpolator::PolynomicValue;
    } else if (values.size() < MAX_POINTS) {
        valueFunction = &LinearInterpolator::BezierValue;
    } else {
        std::length_error e(
                "Can't initialize randomize::LinearInterpolator with" + std::to_string(values.size()) +
                        " points.");
        throw e;
    }
}

double Interpolator::value2(double point) {
    if (valueFunction == nullptr) {
        std::logic_error e("tried to use an uninitialized randomize::LinearInterpolator.");
        throw e;
    }
    return (this->*valueFunction)(point);
}

void LinearInterpolator::setPoints(double constant) {
    values.push_back(constant);
    valueFunction = &LinearInterpolator::ConstantValue;
}

void LinearInterpolator::setPoints(double start, double end) {
    values.push_back(start);
    values.push_back(end);
    valueFunction = &LinearInterpolator::LinearValue;
}

void LinearInterpolator::setPoints(std::vector<double> points) {
    values = points;
    valueFunction = &LinearInterpolator::BezierValue;
}

double LinearInterpolator::ConstantValue(double point) const {
    return values[0];
}

double LinearInterpolator::LinearValue(double point) const {
    return (1 - point) * values[0] + point * values[1];
}

double LinearInterpolator::BezierValue(double point) const {
    std::logic_error e("unimplemented: double Interpolator::BezierValue(double point) const");
    throw e;
}

double LinearInterpolator::PolynomicValue(double t) const {
    double value = polynom[3]*t*t*t + polynom[2]*t*t + polynom[1]*t + polynom[0];
    return value;
}

*/
};  // randomize

