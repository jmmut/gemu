#ifndef __Interpolator_H_
#define __Interpolator_H_

#include <vector>
#include <exception>
#include <stdexcept>
#include <string>
#include <functional>
#include <algorithm>
#include <utils/exception/StackTracedException.h>
#include <utils/Walkers.h>

namespace randomize {


/**
* Abstract class that makes an interpolation of a function y=f(t), defined with points belonging to
* the function.
*
* This is supposed to be used in the range [0, 1], although children may loosen this restriction
* and allow extrapolation.
*
* May be inherited with other types of interpolation.
*/
class Interpolator {
public:
    virtual double value(double time) = 0;

    virtual void setPoints(std::initializer_list<double> values) = 0;
    virtual void setPoints(std::initializer_list<std::pair<double, double>> values) = 0;

    virtual ~Interpolator() {};
};

/**
 * Linear interpolation of y=f(t) defined with several equidistant points.
 *
 * If the initializer_list<double> constructor is used, the first value will be assigned to f(0)
 * and the last point will be f(1). The other middle values will be equally spaced between first
 * and last.
 *
 * If the initalizer_list<pair<double, double>> constructor is used, the first number is the time
 * point (the independent variable, t) and the second is the value (the dependent variable, f(t)). This
 * allows for non-equally spaced points. e.g: {{0, 5}, {0.2, 20}, {1, 10}} is a function that grows
 * quickly and then slowly goes down.
 *
 * If zero points are provided, an exception will be thrown if asked to interpolate.
 * If one point is provided, it is a constant, so no real interpolation.
 * If two points are provided, a simple y=(1-x)*p0 + x*p1 line is interpolated.
 * If more points are provided, the previous simple interpolation is applied in the proper range.
 */
class LinearInterpolator : public Interpolator {
public:
    static const int MAX_POINTS = 5;

    LinearInterpolator() : valueFunction(&LinearInterpolator::uninitialized) { }

    /** valueequidistant points */
    LinearInterpolator(std::initializer_list<double> values);
    /** points arbitrarily distributed, first value is the independent variable, second is the amount */
    LinearInterpolator(std::initializer_list<std::pair<double, double>> values);

    /**
    * computes the value in the given interpolation point.
    * @param point usually in the range [0, 1]. out of this scope will return extrapolation.
    */
    virtual double value(double point) override;

    virtual void setPoints(std::initializer_list<double> values) override;
    virtual void setPoints(std::initializer_list<std::pair<double, double>> values) override;

private:
    // if taken interpolation function as y=f(t), values[n].first is t, and values[n].second is f(t)
    std::vector<double> times;
    std::vector<double> values;

    void init();
//    std::function<double(LinearInterpolator*, double)> valueFunction;
    double (LinearInterpolator::*valueFunction) (double point);

    double uninitialized(double unused);
    double constantValue(double unused);
    double simpleInterpolation(double time);
    double complexInterpolation(double time);
    /** case of 4 values, we can use a polynomic interpolation */
//    double polynom[4];
//    double PolynomicValue(double point) const;


};

/*
template<typename Impl>
class InterpolatorT : public Impl {
public:
    using Impl::Impl;

    double value(double point) override { return Impl::value(point); }

    void setPoints(std::initializer_list<double> values) override {
        Impl::setPoints(values);
    }
    void setPoints(std::initializer_list<std::pair<double, double>> values) override {
        Impl::setPoints(values);
    }
};
*/

};  //randomize


#endif //__Interpolator_H_
