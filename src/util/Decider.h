#ifndef __Decider_H_
#define __Decider_H_

#include <functional>
#include <iostream>
#include <random>

using std::function;
namespace randomize {


template<typename T>
class Decider {
private:
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<> dis;
    T value;
    float decreaseProbability;
    float increaseProbability;
    T minValue;
    T maxValue;
    function<void(T &value)> decrease;
    function<void(T &value)> increase;

public:
    Decider(T constantValue);

    Decider(T defaultValue, T minValue, T maxValue, float decreaseProbability, float increaseProbability,
            function<void(T &value)> decrease, function<void(T &value)> increase);

    Decider(const Decider &decider);

    Decider<T> &operator=(const Decider<T> &);

    T operator()();
};

//
//using std::function;
//
//
//template<typename T>
//Decider<T>::Decider(T defaultValue, float decreaseProbability, float increaseProbability
//        , T minValue, T maxValue, function<void (T &value)> increase, function<void (T &value)> decrease)
//        :rd(),
//         gen(rd),
//         dis(0, 1),
//         value(defaultValue) {
//}
//
///**
//* Makes the decision of which should be the next value.
//*
//* Internally, takes a random number and tests where it falls:
//* * decrease: [0, decreaseProbability)
//* * increase: [decreaseProbability, decreaseProbability + increaseProbability)
//* * do nothing: [decreaseProbability + increaseProbability, 1]
//*/
//template<typename T>
//T Decider<T>::operator()() {
//    float random = dis(gen);
//    std::cout << "random = " << random << std::endl;
//    if (random < decreaseProbability) {
//        T testValue = value;
//        decrease(testValue);
//        if (testValue >= minValue) {
//            value = testValue;
//        }
//    }
//    random -= decreaseProbability;
//    if (random < increaseProbability){
//        T testValue = value;
//        increase(testValue);
//        if (testValue <= maxValue) {
//            value = testValue;
//        }
//    }
//    return value;
//}

}; // randomize

#endif //__Decider_H_

