/**
 * @file EntropyCalculator.h
 * @author jmmut 
 * @date 2016-03-25.
 */

#ifndef GEMU_ENTROPYCALCULATOR_H
#define GEMU_ENTROPYCALCULATOR_H

#include <string>
#include <map>
#include <memory>
#include <cmath>
#include "util/MergeableTree.h"
#include "log/log.h"
#include "utils/exception/StackTracedException.h"

namespace randomize {


template<typename SYMBOL>
class EntropyCalculator {

public:
    void add(const SYMBOL &symbol) {
        symbols[symbol]++;
        totalCount++;
    }

    double compute() {
        const double toProbability = 1.0/double(totalCount);
        double entropy = 0;
        for(std::pair<SYMBOL, int> entry : symbols) {
            double probability = entry.second*toProbability;
            if (probability != 0) {
                entropy -= probability * log2(probability);
            }
        }
        return entropy;
    }

private:
    std::map<SYMBOL, long> symbols;
    long totalCount = 0;
};


struct Node {
    double flatEntropy;
    std::vector<double> averageEntropies;
    friend std::ostream& operator<<(std::ostream &os, const Node &node) {
        os << "{" << node.flatEntropy << ", "
        << randomize::utils::container_to_string(node.averageEntropies) << "}";
        return os;
    }
};

template<typename SYMBOL>
class HierarchicalEntropyCalculator {

public:
    void add(const SYMBOL &symbol) {
        symbols[symbol]++;
        totalCount++;
        hierarchicalAdd(symbol);
    }

    void hierarchicalAdd(const SYMBOL &symbol) {
        unsigned long treeIndex = whereToInsert();
        regionSymbols.resize(treeIndex + 1);
        regionSymbols[treeIndex][symbol]++;
        treeSizes.resize(treeIndex + 1);
        treeSizes[treeIndex]++;
        if (haveToCloseTree()) {
            trees.push_back(closeTree());
        }
        int merges = 0;
        while (haveToMerge(merges)) {
            merge();
            merges++;
        }
    }

    unsigned long whereToInsert() {
        long completedTreesMask = (totalCount - 1) / basicBlockLength;
        return analyzeBits(completedTreesMask).size();
    }

    bool haveToCloseTree () {
        return (totalCount % basicBlockLength) == 0; // moar or less
    }

    /**
     * compute entropies of last tree and create the MergeableTree node.
     */
    std::shared_ptr<MergeableTree<Node>> closeTree() {
        if (regionSymbols.size() >= 1) {
            unsigned long whichTree = regionSymbols.size() - 1;
            long mapValuesAmount = treeSizes[whichTree];
            const double toProbability = 1.0/double(mapValuesAmount);
            double entropy = 0;
            for(std::pair<SYMBOL, long> entry : regionSymbols[whichTree]) {
                double probability = entry.second*toProbability;
                if (probability != 0) {
                    entropy -= probability * log2(probability);
                }
            }

            std::shared_ptr<MergeableTree<Node>> tree(new MergeableTree<Node>());
//            MergeableTree tree; // to use move push_back(&&). we can't do this if we later do a pop_back()
            tree->setNode({entropy, {}});
            return tree;
        } else {
            std::string message("cannot close a tree if there are no trees.");
            LOG_ERROR("%s", message.c_str());
            throw randomize::utils::exception::StackTracedException(message);
        }
    }

    /**
     * We have to merge if the completedTreesMask has
     * ((more bits set to 0 below the smallest bit set to 1) than (merges))
     */
    bool haveToMerge(int merges) {
//        long completedTreesMask = (totalCount-1) / basicBlockLength;
        long completedTreesMask = (totalCount) / basicBlockLength;
        if (completedTreesMask * basicBlockLength != totalCount) {
            // do not have to merge if we didn't just finish a block
            return false;
        }

        std::vector<long> bitPositions = analyzeBits(completedTreesMask);

        return bitPositions.size() > 0 && bitPositions[0] > merges;
    }

    void merge() {
        if (regionSymbols.size() >= 2) {
            if (regionSymbols.size() != treeSizes.size()) {
                std::string message("state got incoherent. different sizes of trees and maps");
                LOG_ERROR("%s", message.c_str());
                throw randomize::utils::exception::StackTracedException(message);
            }

            // merge the maps
            long sourceIndex = regionSymbols.size() - 1, targetIndex = regionSymbols.size() - 2;
            for(auto& it : regionSymbols[sourceIndex]) {
                regionSymbols[targetIndex][it.first] += it.second;
            }
            regionSymbols.pop_back();
            treeSizes[targetIndex] += treeSizes[sourceIndex];
            treeSizes.pop_back();

            // compute compute the flat entropy
            // create the father node with the entropy values
            std::shared_ptr<MergeableTree<Node>> newRoot = closeTree();

            // compute the average entropy(ies) with the sons
            std::vector<double> entropies;
            entropies.push_back(
                    (trees[targetIndex]->getNode().flatEntropy
                     + trees[sourceIndex]->getNode().flatEntropy)
                    /2);
            std::vector<double> targetEntropies = trees[targetIndex]->getNode().averageEntropies;
            std::vector<double> sourceEntropies = trees[sourceIndex]->getNode().averageEntropies;
            if (targetEntropies.size() != sourceEntropies.size()) {
                std::string message("state got incoherent: different sizes of average entropies between sibling trees");
                LOG_ERROR("%s", message.c_str());
                throw randomize::utils::exception::StackTracedException(message);
            }

            for (unsigned int i = 0; i < trees[targetIndex]->getNode().averageEntropies.size(); ++i) {
                entropies.push_back((targetEntropies[i] + sourceEntropies[i]) / 2);
            }

            // merge trees: append both sons
            newRoot->setNode({newRoot->getNode().flatEntropy, entropies});
            newRoot->addSon(*trees[targetIndex]);
            newRoot->addSon(*trees[sourceIndex]);
            trees.pop_back();
            trees.pop_back();
            trees.push_back(newRoot);

        } else {
            std::string message("cannot merge less than two trees");
            LOG_ERROR("%s", message.c_str());
            throw randomize::utils::exception::StackTracedException(message);
        }
    }

    std::vector<long> analyzeBits(unsigned int mask) {
        std::vector<long> positions;
        const unsigned int bitsPerByte = 8;
        for (unsigned int i = 0; i < sizeof(mask)*bitsPerByte; ++i) {
            if (mask & (1 << i)) {
                positions.push_back(i);
            }
        }
        return positions;
    }

    std::vector<std::shared_ptr<MergeableTree<Node>>> &getTrees() {
        return trees;
    }

    double compute() {
        const double toProbability = 1.0/double(totalCount);
        double entropy = 0;
        for(std::pair<SYMBOL, int> entry : symbols) {
            double probability = entry.second*toProbability;
            if (probability != 0) {
                entropy -= probability * log2(probability);
            }
        }
        return entropy;
    }

    long &getBasicBlockLength() {
        return basicBlockLength;
    }

    std::vector<double> normalize(Node node) {
        std::vector<double> normalized;
        unsigned long levels = node.averageEntropies.size();
        normalized.push_back(node.flatEntropy / log2(basicBlockLength * (1 << levels)));
        for (unsigned int i = 0; i < levels; ++i) {
            normalized.push_back(
                    node.averageEntropies[i] / log2(basicBlockLength * (1 << (levels - i - 1))));
        }
        return normalized;
    }

    std::vector<double> getRootEntropiesNormalized() {
        return normalize(trees[0]->getNode());
    }
private:
    long basicBlockLength = 2;
    std::map<SYMBOL, long> symbols;
    std::vector<std::shared_ptr<MergeableTree<Node>>> trees;
    std::vector<long> treeSizes;
    std::vector<std::map<SYMBOL, long>> regionSymbols;
    long totalCount = 0;
};

};  // randomize

#endif //GEMU_ENTROPYCALCULATOR_H
