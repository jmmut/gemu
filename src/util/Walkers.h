/**
 * @file Walkers.h
 * @author jmmut 
 * @date 2015-08-30.
 */

#ifndef GEMU_WALKERS_H
#define GEMU_WALKERS_H

#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>

/*

template<typename T, typename F>
auto join(T t, F joiner) -> decltype(joiner) {
    decltype(joiner) result;
    for (auto it = begin(t); it != end(t); it++) {
        result = joiner(*it, result);
    }
    return result;
    accumulate();
}
*/
template<typename T>
std::string vector_to_string(std::vector<T> v) {
//    string joined = join<vector<T>, string, function<string(T, string)>>(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
//    string joined = join(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
    std::string joined = std::accumulate(begin(v), end(v), std::string(), [] (std::string s, T elem) {
        return s + " " + std::to_string(elem) + ",";
    });
    joined[0] = '[';
    joined[joined.size()-1] = ']';
    return joined;
}


template<typename C>
std::string container_to_string(C v) {
//    string joined = join<vector<T>, string, function<string(T, string)>>(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
//    string joined = join(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
    std::string joined = std::accumulate(std::begin(v), std::end(v), std::string(), [] (std::string s, decltype(*std::begin(v)) elem) {
        return s + " " + std::to_string(elem) + ",";
    });
    joined[0] = '[';
    joined[joined.size()-1] = ']';
    return joined;
}

template<typename R, typename T>
std::vector<R> change(std::vector<T> t, std::function<R(T)> transformer) {
    std::vector<R> v;
    for (auto it = begin(t); it != end(t); it++) {
        v.push_back(transformer(*it));
    }
    return v;
}

template<typename R, typename T>
std::vector<R> change2(std::vector<T> t, std::vector<R> result, std::function<R(T)> transformer) {
    std::transform(std::begin(t), std::end(t), std::begin(result), transformer);
    return result;
}

template<typename R, typename C, typename V>
std::vector<R> change3(C t, std::function<R(V)> transformer) {
//    transform(std::begin(t), std::end(t), std::begin(result), transformer);
//    return result;
    std::vector<R> v;
    for (const auto &it = std::begin(t); it != std::end(t); it++) {
        v.push_back(transformer(*it));
    }
    return v;
}
/*
vector<int> transform(vector<Track> t, function<int(Track)> transformer) {
    vector<int> v;
    for (auto it = begin(t); it != end(t); it++) {
        v.push_back(transformer(*it));
    }
    return v;
}
*/

#endif //GEMU_WALKERS_H
