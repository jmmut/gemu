#include "util/Decider.h"

using std::function;

namespace randomize {
template<typename T>
Decider<T>::Decider(T constantValue) : Decider(constantValue, constantValue, constantValue, 0, 0, nullptr, nullptr) {
}


template<typename T>
Decider<T>::Decider(const Decider &d)
        : Decider<T>(d.value, d.minValue, d.maxValue, d.decreaseProbability, d.increaseProbability, d.decrease,
                     d.increase) {
}


template<typename T>
Decider<T>::Decider(T defaultValue, T minValue, T maxValue, float decreaseProbability, float increaseProbability,
                    function<void(T &value)> decrease, function<void(T &value)> increase)
        : rd(), gen(rd()), dis(0, 1), value(defaultValue), decreaseProbability(decreaseProbability),
          increaseProbability(increaseProbability), minValue(minValue), maxValue(maxValue), decrease(decrease),
          increase(increase) {
//    this->decrease = decrease;
//    this->increase = increase;
}

template<typename T>
Decider<T> &Decider<T>::operator=(const Decider<T> &decider) {
    value = decider.value;
    minValue = decider.minValue;
    maxValue = decider.maxValue;
    increase = decider.increase;
    decrease = decider.decrease;
    increaseProbability = decider.increaseProbability;
    decreaseProbability = decider.decreaseProbability;
    return *this;
}

/**
* Makes the decision of which should be the next value.
*
* Internally, takes a random number and tests where it falls:
* * decrease: [0, decreaseProbability)
* * increase: [decreaseProbability, decreaseProbability + increaseProbability)
* * do nothing: [decreaseProbability + increaseProbability, 1]
*/
template<typename T>
T Decider<T>::operator()() {
    float random = dis(gen);
//    float random = 0;
//    std::cout << "random = " << random << std::endl;
    if (random < decreaseProbability) {
        T testValue = value;
        if (decrease != nullptr) {
            decrease(testValue);
            if (testValue >= minValue) {
                value = testValue;
            }
        }
    } else if (random < increaseProbability + decreaseProbability) {
        T testValue = value;
        if (increase != nullptr) {  // TODO replaceable by `if(increase) {` ??
            increase(testValue);
            if (testValue <= maxValue) {
                value = testValue;
            }
        }
    }
    return value;
}

template
class Decider<int>;

template
class Decider<float>;
//template int Decider<int>::operator()();

};  // randomize



