/**
 * @file HierarchicalEntropyExplainer.cpp
 * @author jmmut 
 * @date 2016-04-17.
 */

#include "HierarchicalEntropyExplainer.h"

namespace randomize {


HierarchicalEntropyExplainer::Properties HierarchicalEntropyExplainer::computeProperties() {

    minmaxEntropies();
    entropyAcrossScales();

    return properties;
}

using namespace boost::accumulators;

void HierarchicalEntropyExplainer::entropyAcrossScales() {
    accumulator_set<double, stats<tag::mean, tag::variance>> acc;

    for (auto value : entropies) {
        acc(value);
    }
    properties.averageEntropyAcrossScales = mean(acc);
    properties.deviationEntropyAcrossScales = std::sqrt(variance(acc));
}

void HierarchicalEntropyExplainer::minmaxEntropies() {

    using Iterator = std::vector<double>::iterator;
    std::pair<Iterator, Iterator> minmax = std::minmax_element(entropies.begin(), entropies.end());

    properties.minEntropyIndex = minmax.first - entropies.begin();
    properties.minEntropy = *minmax.first;
    properties.mostStructuredScale = scaleToPeriod(minmax.first);

    properties.maxEntropyIndex = minmax.second - entropies.begin();
    properties.maxEntropy = *minmax.second;
    properties.leastStructuredScale = scaleToPeriod(minmax.second);
}

void HierarchicalEntropyExplainer::computeDescription(std::ostream &os) {
    os << "The most recognizable pattern has length " << properties.mostStructuredScale
    << " with an average entropy of " << properties.minEntropy << ". ";
    double mostStructuredDeviation = properties.averageEntropyAcrossScales - properties.minEntropy;
    double deviations = mostStructuredDeviation/properties.deviationEntropyAcrossScales;

    os << "It is a ";
    if (deviations < 0.5) {
        os << "very subtle";
    } else if (deviations < 1) {
        os << "subtle";
    } else if (deviations < 1.5) {
        os << "clearly recognizable";
    } else {
        os << "obvious";
    }
    os << " pattern. ";


    os << "If I had to sort the pattern lengths, the order would be : ";
    std::vector<int> lengths;

    for (auto elem : sort_indexes(entropies)) {
        lengths.push_back(scaleToPeriod(entropies.begin() + elem));
    }
    os << container_to_string(lengths);
    os << std::endl;
}

int HierarchicalEntropyExplainer::scaleToPeriod(std::vector<double>::iterator scaleIndex) {
    int scale = entropies.end() - scaleIndex - 1;
    return static_cast<int>(pow(2, scale) * basicBlockLength);
}


};  // randomize
