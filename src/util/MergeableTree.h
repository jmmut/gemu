/**
 * @file MergeableTree.h
 * @author jmmut 
 * @date 2016-03-28.
 */

#ifndef GEMU_MERGEABLETREE_H
#define GEMU_MERGEABLETREE_H

#include "utils/Walkers.h"

namespace randomize {

/**
 * Use this class if you want to create nodes and join them with a parent node.
 * NODE is a template parameter and needs to have this interface at least:

struct Node {
    friend std::ostream& operator<<(std::ostream &os, const Node &node);
}

 */
template<typename NODE>
class MergeableTree {
public:
    MergeableTree<NODE>& setNode(NODE node) {
        this->node = node;
        return *this;
    }
    MergeableTree<NODE>& addSon(MergeableTree<NODE>& otherTree) {
        sons.push_back(otherTree);
        return *this;
    }

    friend std::ostream& operator<<(std::ostream &os, const MergeableTree<NODE> &tree) {
        os << "{node:" << tree.node << ", sons:[";
        for (const auto &son : tree.sons) {
            os << " ," << son;
        }
        return os << "]}";
    }

    NODE &getNode() {
        return node;
    }
    std::vector<MergeableTree<NODE>>& getSons() {
        return sons;
    }

private:
    NODE node;
    std::vector<MergeableTree<NODE>> sons;
};

};  // randomize
#endif //GEMU_MERGEABLETREE_H
