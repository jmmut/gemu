/**
 * @file entropy.test.cpp.cpp
 * @author jmmut 
 * @date 2016-03-25.
 */

#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include <util/EntropyCalculator.h>
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "util/HierarchicalEntropyExplainer.h"
#include "utils/test/TestSuite.h"
#include "utils/SignalHandler.h"
#include "log/log.h"


using namespace randomize;
using namespace std;

const double DELTA_DOUBLE = 0.000001;

bool equal(double first, double second, double delta = DELTA_DOUBLE) {
    return std::abs(first - second) < delta;
}

int substructures(int argc, char **argv) {
    vector<int> message{
            0, 1, 0, 1, 2, 3, 2, 3, 0, 1, 0, 1, 2, 4, 2, 4
    };

    HierarchicalEntropyCalculator<int> entropy;
    for (auto symbol : message) {
        entropy.add(symbol);
    }
    vector<double> entropies = entropy.getRootEntropiesNormalized();
    HierarchicalEntropyExplainer explainer(entropies, entropy.getBasicBlockLength());

    HierarchicalEntropyExplainer::Properties properties = explainer.computeProperties();

    int fails = 0;
    fails += properties.maxEntropy != 1;
    fails += properties.maxEntropyIndex != 3;
    fails += properties.minEntropy != 0.5;
    fails += properties.minEntropyIndex != 2;
    fails += properties.mostStructuredScale != 4;
    fails += properties.leastStructuredScale != 2;
    fails += !equal(properties.averageEntropyAcrossScales, 0.68229175);
    fails += !equal(properties.deviationEntropyAcrossScales, 0.192849040);

    return fails;
}

int description(int argc, char **argv) {
    vector<int> message{
            0, 1, 0, 1, 2, 3, 2, 3, 0, 2, 0, 2, 1, 3, 1, 3
    };

    HierarchicalEntropyCalculator<int> entropy;
    for (auto symbol : message) {
        entropy.add(symbol);
    }
    vector<double> entropies = entropy.getRootEntropiesNormalized();
    HierarchicalEntropyExplainer explainer(entropies, entropy.getBasicBlockLength());

//    HierarchicalEntropyExplainer::Properties properties =
    explainer.computeProperties();

    cout << container_to_string(message) << ": ";
    explainer.computeDescription(cout);
    return 0;
}

int description2(int argc, char **argv) {
    vector<int> message{
            0, 1, 0, 1, 0, 1, 0, 1, 2, 3, 2, 3, 2, 3, 2, 3
    };

    HierarchicalEntropyCalculator<int> entropy;
    for (auto symbol : message) {
        entropy.add(symbol);
    }
    vector<double> entropies = entropy.getRootEntropiesNormalized();
    HierarchicalEntropyExplainer explainer(entropies, entropy.getBasicBlockLength());

//    HierarchicalEntropyExplainer::Properties properties =
    explainer.computeProperties();

    cout << container_to_string(message) << ": ";
    explainer.computeDescription(cout);
    return 0;
}
int interactive(int argc, char **argv) {
    if (argc >= 2) {
        cout << "interactive hierarchical test (CTRL+D to finish): " << endl;
        string line, symbol;
        cin.clear();
        while (getline(cin, line)) {
            stringstream ss(line);
            HierarchicalEntropyCalculator<std::string> entropy;
            if (argc == 3) {
                entropy.getBasicBlockLength() = std::stoi(argv[2]);
            }
            while (ss >> symbol) {
                entropy.add(symbol);
            }

            vector<double> entropies = entropy.getRootEntropiesNormalized();
            cout << "normalized: "
            << container_to_string(entropies)
            << endl;

            for (shared_ptr<MergeableTree<Node>> tree : entropy.getTrees()) {
                cout << *tree << endl;
            }

            HierarchicalEntropyExplainer explainer(entropies, entropy.getBasicBlockLength());
//            HierarchicalEntropyExplainer::Properties properties =
            explainer.computeProperties();
            explainer.computeDescription(cout);

        }
    }
    return 0;
}

int main (int argc, char ** argv) {

    SigsegvPrinter::activate();
    LOG_LEVEL(argc == 2? log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL);

    randomize::utils::test::TestSuite suite(argc, argv, {
            substructures, description, description2, interactive
    }, "entropy explainer");

    return suite.countFailed();
}
