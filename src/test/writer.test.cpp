/**
 * @file soundStream.test.cpp
 * @author jmmut 
 * @date 2015-09-12.
 */

#include <iostream>
#include "core/MusicGenerator.h"
#include "io/SoundStream.h"
#include "io/SoundStreamWriter.h"
#include "io/WavWriter.h"
#include "io/TextSongWriter.h"
#include "io/LilypondWriter.h"

using namespace randomize::gemu;
using std::string;

void endian(float element);

unsigned char f(int t) {
//    return 4*(t+1);
    return (t>>13)*t*(t*(t>>10));
}

int main(int argc, char **argv) {



    LOG_LEVEL(LOG_INFO_LEVEL);
/*
    endian(0.3262);
    SoundStream ss;

    int t = 0;
    if (argc > 1) {
        while (true) {
            std::cout << f(t);
            t++;
        }
    } else {
        while (true) {
            ss << f(t);
            t++;
            if (t > 100000) {
                break;
            }
        }
    }
*/
    MusicGenerator m;
    m.generateDissonance(20);

    int samplesPerSecond = 22050;
    string name = "writer";
    string textName = name + ".txt";
    if (!writeToFile(textName, m.getSong().toString())) {
        LOG_ERROR("could not open file %s to write", textName.c_str());
    }
//    m.write(std::make_shared<SoundStreamWriter>(samplesPerSecond));
//    m.write(std::make_shared<WavWriter>(name + ".wav", samplesPerSecond));
    SoundStreamWriter(samplesPerSecond).write(m.getSong());
    WavWriter(name + ".wav", samplesPerSecond).write(m.getSong());
    LilypondWriter(name + ".ly").write(m.getSong());
    TextSongWriter(name + ".txt", TextSongWriter::BASIC).write(m.getSong());
    return 0;
}

