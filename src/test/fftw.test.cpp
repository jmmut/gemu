/**
 * @file fftw.test.cpp
 * @author jmmut 
 * @date 2016-04-19.
 */

#include <fftw3.h>

#define NUM_POINTS 44100


/* Never mind this bit */

#include <iostream>
#include <math.h>
#include <utils/time/TimeRecorder.h>
#include <log/log.h>
#include <core/Gemu.h>

#define REAL 0
#define IMAG 1

using randomize::gemu::writeToFile;
using std::ostream;
using std::endl;

void acquire_from_somewhere(double* signal) {
    /* Generate two sine waves of different frequencies and
    * amplitudes.
    */

    writeToFile("fftw.samples.dat", [&](ostream &file){
        for (unsigned int i = 0; i < NUM_POINTS; ++i) {
            double theta = (double) i * 2 * M_PI / (double) NUM_POINTS;

            signal[i] = 0.5 * sin(440.0 * theta)
//        + 1.0 * cos(32.0 * theta)
                    ;

            file << signal[i] << endl;


//        signal[i][IMAG] = 0.0;
//        signal[i][IMAG] = 1.0 * sin(10.0 * theta) +
//                          0.5 * sin(25.0 * theta);
        }
    });
}

void do_something_with(fftw_complex* result) {
//    for (i = 0; i < NUM_POINTS/2 +1; ++i) {
//        double mag = sqrt(result[i][REAL] * result[i][REAL] +
//                          result[i][IMAG] * result[i][IMAG]);
//
//        printf("%g\n", mag);
//    }
//    for (i = 0; i < NUM_POINTS/2; ++i) {
//        double mag = result[i][REAL];
//        printf("%g\n", mag);
//    }
    writeToFile("fftw.real.dat", [&](ostream &file){
        for (unsigned int i = 0; i < NUM_POINTS/2 +1; ++i) {
            file << result[i][REAL] << endl;
        }
    });
    writeToFile("fftw.imag.dat", [&](ostream &file){
        for (unsigned int i = 0; i < NUM_POINTS/2 +1; ++i) {
            file << result[i][IMAG] << endl;
        }
    });
}


int main(int argc, char **argv) {

//    fftw_complex signal[NUM_POINTS];
    double signal[NUM_POINTS];
    fftw_complex result[NUM_POINTS];

    randomize::utils::time::TimeRecorder timeRecorder;
    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL);

    timeRecorder.start();
    fftw_plan plan = fftw_plan_dft_r2c_1d(NUM_POINTS,
            signal,
            result,
            FFTW_ESTIMATE);

    acquire_from_somewhere(signal);
    fftw_execute(plan);
    timeRecorder.stop();

    LOG_DEBUG("%s", timeRecorder.toString().c_str())
    do_something_with(result);

    fftw_destroy_plan(plan);

    return 0;
}
