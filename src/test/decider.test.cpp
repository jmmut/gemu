#include <iostream>
#include "util/Decider.h"
using namespace std;
using randomize::Decider;
int main() {
    Decider<int> decider(5, 2, 9, 0.1, 0.1, [](int &value) {
        value--;
    }, [](int &value) {
        value++;
    });

    for (int i = 0; i < 100; ++i) {
        cout << "decider() = " << decider() << endl;

    }
}
