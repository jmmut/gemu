/**
 * @file entropy.test.cpp.cpp
 * @author jmmut 
 * @date 2016-03-25.
 */

#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "util/EntropyCalculator.h"
#include "utils/test/TestSuite.h"
#include "utils/SignalHandler.h"
#include "log/log.h"


using namespace randomize;
using namespace std;
using utils::exception::StackTracedException;

template<typename T>
inline bool equals(T a, T b, T error) {
    if (std::abs(a - b) >= error) {
        LOG_DEBUG("%f != %f", a, b);
        return false;
    }
    return true;
}

int main (int argc, char ** argv) {

    SigsegvPrinter::activate();
    LOG_LEVEL(argc == 2? log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL);

    randomize::utils::test::TestSuite suite(argc, argv, {
            [](int argc, char ** argv) {
                Interpolator * interpolator = new LinearInterpolator();
                try {
                    interpolator->value(0);
                } catch (StackTracedException e) {
                    // ok it detected that it can not interpolate without values
                    return 0;
                }
                return 1;
            },

            [](int argc, char ** argv) {
                Interpolator *interpolator = new LinearInterpolator({0.5, 0.9});
                int fails = 0;

                fails += not equals(interpolator->value(0), 0.5, 0.001);
                fails += not equals(interpolator->value(1), 0.9, 0.001);
                fails += not equals(interpolator->value(0.5), 0.7, 0.001);
                fails += not equals(interpolator->value(0.6), 0.74, 0.001);
                return fails;
            },

            [](int argc, char ** argv) {
                Interpolator *interpolator = new LinearInterpolator({0.5, 1.0, 1.5});
                int fails = 0;
                fails += not equals(interpolator->value(0), 0.5, 0.001);
                fails += not equals(interpolator->value(1), 1.5, 0.001);
                fails += not equals(interpolator->value(0.5), 1.0, 0.001);
                fails += not equals(interpolator->value(0.6), 1.1, 0.001);
                return fails;
            },

            [](int argc, char ** argv) {
                Interpolator *interpolator = new LinearInterpolator({0.5, 1.2, 1.5, 1.7});
                int fails = 0;
                fails += not equals(interpolator->value(0), 0.5, 0.001);
                fails += not equals(interpolator->value(1), 1.7, 0.001);
                fails += not equals(interpolator->value(0.5), 1.35, 0.001);
                fails += not equals(interpolator->value(0.6), 1.44, 0.001);
                return fails;
            },

            [](int argc, char ** argv) {
                Interpolator *interpolator = new LinearInterpolator(
                        {{0.0, 0.0}, {0.1, 1}, {0.8, 0.9},  {1.0, 0.0}});
                int fails = 0;
                fails += not equals(interpolator->value(0), 0.0, 0.001);
                fails += not equals(interpolator->value(1), 0.0, 0.001);
                fails += not equals(interpolator->value(0.45), 0.95, 0.001);
                fails += not equals(interpolator->value(0.9), 0.45, 0.001);
                return fails;
            }
    }, "interpolator");

    return suite.countFailed();
}
