
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "io/TextSongWriter.h"
#include "io/LilypondWriter.h"

using namespace randomize::gemu;
using namespace std;

int main(int argc, char ** argv) {
    MusicGenerator musicGenerator;

    LOG_LEVEL(argc > 1 ? randomize::log::parseLogLevel(argv[argc-1]) : LOG_INFO_LEVEL);

    /*
    double start = 0;
    double end = -1;
    if (argc >= 3) {
        try {
            start = stod(argv[1]);
            end = stod(argv[2]);
        } catch (const std::invalid_argument& ia) {
            LOG_ERROR("first and second paramenters must be the position to write readable points")
        }
    }
    */

    musicGenerator.generateHarmonicSequenceTest();
    string name = "harmonic";

    WavWriter(name + ".wav", 22050).write(musicGenerator.getSong());
    LilypondWriter(name + ".ly").write(musicGenerator.getSong());
    TextSongWriter(cout, TextSongWriter::BASIC).write(musicGenerator.getSong());
    return 0;
}
