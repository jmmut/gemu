/**
 * @file entropy.test.cpp.cpp
 * @author jmmut 
 * @date 2016-03-25.
 */

#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "util/EntropyCalculator.h"
#include "utils/test/TestSuite.h"
#include "utils/SignalHandler.h"
#include "log/log.h"


using namespace randomize;
using namespace std;

double showEntropy(const vector<string> &message);

int basicTest (int argc, char ** argv) {
    vector<string> symbols{"A", "B", "C", "D", "E"};
    vector<string> message{
            symbols[0],
            symbols[1],
            symbols[0],
            symbols[2],
            symbols[3],
            symbols[0],
            symbols[0],
            symbols[0],
            symbols[4],
            symbols[3],
    };

    showEntropy(message);

    message[1] = symbols[0];
    message[3] = symbols[0];
    message[4] = symbols[0];
    showEntropy(message);

    message[8] = symbols[0];
    message[9] = symbols[0];
    showEntropy(message);

    cout << "angel test" << endl;

    message = {symbols[0], symbols[0], symbols[0], symbols[0],
               symbols[0], symbols[0], symbols[0], symbols[1]};
    showEntropy(message);

    message = {symbols[0], symbols[0], symbols[0], symbols[1],
               symbols[0], symbols[1], symbols[0], symbols[1]};
    showEntropy(message);

    message = {symbols[0], symbols[0]};
    showEntropy(message);



    ////// real test:  distribution: {0 0 0 0 1 1 2 3} has 1.75 bits of entropy
    message.clear();
    message = {symbols[0], symbols[0], symbols[0], symbols[0],
               symbols[1], symbols[1], symbols[2], symbols[3]};

    return showEntropy(message) != 1.75;
}


int interactiveTest (int argc, char ** argv) {
    if (argc == 2) {
        cout << "interactive test (CTRL+D to finish): " << endl;
        string line, symbol;
        cin.clear();
        while (getline(cin, line)) {
            stringstream ss(line);
            EntropyCalculator<string> entropy;
            while (ss >> symbol) {
                entropy.add(symbol);
            }
            cout << entropy.compute() << endl;
        }
    }
    return 0;
}
int interactiveHierarchicalTest (int argc, char ** argv) {
    if (argc >= 2) {
        cout << "interactive hierarchical test (CTRL+D to finish): " << endl;
        string line, symbol;
        cin.clear();
        while (getline(cin, line)) {
            stringstream ss(line);
            HierarchicalEntropyCalculator<std::string> entropy;
            if (argc == 3) {
                entropy.getBasicBlockLength() = std::stoi(argv[2]);
            }
            while (ss >> symbol) {
                entropy.add(symbol);
            }
            cout << "normalized: "
            << container_to_string(entropy.normalize(entropy.getTrees()[0]->getNode()))
            << endl;

            for (shared_ptr<MergeableTree<Node>> tree : entropy.getTrees()) {
                cout << *tree << endl;
            }
        }
    }
    return 0;
}

int glitchEntropy(int argc, char ** argv) {
    gemu::MusicGenerator music;
    function<unsigned char(double)> callback = [] (double time) -> unsigned char {
        static int t = -1;
        t++;
//        return t;
//        return (t >> 13) * t * (t * (t >> 10));
        return t * ((t >> 13 | t >> 9) & 63 & t >> 5);
//        return t * ((t >> 13 | t >> 9) & 127 & t >> 5);
//        return t * (t >> 15 | t >> 9 );
//        return t * ( t >> 7 );
    };

    int seconds = 60;
    int samplesPerSecond = 22050;
    int samples = seconds*samplesPerSecond;

    HierarchicalEntropyCalculator<unsigned char> entropy;
    entropy.getBasicBlockLength() = samplesPerSecond/2;

    for (int j = 0; j < samples; ++j) {
        entropy.add(callback(0.0));
    }

    cout << "glitch entropy: " << entropy.compute() << endl;
    std::vector<double> entropies = entropy.normalize(entropy.getTrees()[0]->getNode());
    cout << "glitch region entropy: " << container_to_string(entropies) << endl;
    ofstream f;
    f.open("entropyGlitch.dat");
    for (auto value : entropies) {
        f << value << endl;
    }
    f.close();

    music.generateGlitch(seconds, callback);

    gemu::WavWriter("entropyGlitch.wav", samplesPerSecond).write(music.getSong());
    return 0;
}

int hierarchicalEntropyBasic(int argc, char **argv) {
    std::vector<std::string> values = {"0", "0", "0", "0", "0", "0", "0", "1"};
    HierarchicalEntropyCalculator<std::string> entropy;

    for (auto symbol : values) {
        entropy.add(symbol);
    }

    double globalEntropy = entropy.compute();
    stringstream ss;
    for (shared_ptr<MergeableTree<Node>> tree : entropy.getTrees()) {
        ss << *tree << endl;
    }
    string verifiedResult = "{node:{0.543564, [0.405639, 0.250000]}, "
            "sons:[ ,{node:{0, [0.000000]}, sons:[ ,{node:{0, }, sons:[]} ,{node:{0, }, sons:[]}]} "
            ",{node:{0.811278, [0.500000]}, sons:[ ,{node:{0, }, sons:[]} ,{node:{1, }, sons:[]}]}]}";

    cout << ss.str() << endl;

    bool fail = globalEntropy != entropy.getTrees()[0]->getNode().flatEntropy
                || strncmp(ss.str().c_str(), verifiedResult.c_str(), verifiedResult.size());
    return fail;
}

int hierarchicalEntropyMedium(int argc, char **argv) {
    std::vector<std::string> values = {"0", "0", "0", "0", "0", "1", "1", "1",
                                       "1", "0", "0", "1", "0", "0", "1", "1"};
    HierarchicalEntropyCalculator<std::string> entropy;


    for (auto symbol : values) {
        entropy.add(symbol);
    }

    double globalEntropy = entropy.compute();
    stringstream ss;
    for (shared_ptr<MergeableTree<Node>> tree : entropy.getTrees()) {
        ss << *tree << endl;
    }
    string verifiedResult = "{node:{0.988699, [0.977217, 0.702820, 0.375000]}, sons:[ "
            ",{node:{0.954434, [0.405639, 0.250000]}, sons:[ "
            ",{node:{0, [0.000000]}, sons:[ ,{node:{0, }, sons:[]} ,{node:{0, }, sons:[]}]} "
            ",{node:{0.811278, [0.500000]}, sons:[ ,{node:{1, }, sons:[]} ,{node:{0, }, sons:[]}]}]} "
            ",{node:{1, [1.000000, 0.500000]}, sons:[ "
            ",{node:{1, [1.000000]}, sons:[ ,{node:{1, }, sons:[]} ,{node:{1, }, sons:[]}]} "
            ",{node:{1, [0.000000]}, sons:[ ,{node:{0, }, sons:[]} ,{node:{0, }, sons:[]}]}]}]}";

    cout << ss.str() << endl;

    bool fail = globalEntropy != entropy.getTrees()[0]->getNode().flatEntropy
                || strncmp(ss.str().c_str(), verifiedResult.c_str(), verifiedResult.size());
    return fail;
}
int hierarchicalEntropyNormalization(int argc, char **argv) {
    std::vector<std::string> values = {"0", "1", "2", "3", "4", "5", "6", "7",
                                       "8", "9", "10", "11"};
    HierarchicalEntropyCalculator<std::string> entropy;
    entropy.getBasicBlockLength() = 3;

    for (auto symbol : values) {
        entropy.add(symbol);
    }

    std::vector<double> entropies = entropy.normalize(entropy.getTrees()[0]->getNode());
    std::vector<double> verified = {1, 1, 1};

    bool fail = entropies.size() != verified.size();

    for (unsigned int i = 0; i < entropies.size(); ++i) {
        fail += entropies[i] != verified[i];
    }
    return fail;
}

int hierarchicalEntropyAnalyzeBits(int argc, char ** argv) {

    int fails = 0;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(0).size() != 0;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(1).size() != 1;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(1)[0] != 0;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(2).size() != 1;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(2)[0] != 1;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(3).size() != 2;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(3)[0] != 0;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(3)[1] != 1;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(7).size() != 3;
    fails += HierarchicalEntropyCalculator<std::string>().analyzeBits(5).size() != 2;

    return fails;
}


double showEntropy(const vector<string> &message) {
    EntropyCalculator<string> entropy;

    for (auto symbol : message) {
        entropy.add(symbol);
    }
    double result = entropy.compute();
    cout << result << endl;
    return result;
}

int main (int argc, char ** argv) {

    SigsegvPrinter::activate();
    LOG_LEVEL(argc == 2? log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL);

    randomize::utils::test::TestSuite suite(argc, argv, {
            basicTest,
            interactiveTest,
            glitchEntropy,
            hierarchicalEntropyAnalyzeBits,
            hierarchicalEntropyBasic,
            hierarchicalEntropyMedium,
            hierarchicalEntropyNormalization,
            interactiveHierarchicalTest,
    }, "entropy");

    return suite.countFailed();
}
