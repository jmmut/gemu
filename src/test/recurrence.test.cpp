//
// Created by jmmut on 22/03/15.
//
#include <sstream>
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "io/LilypondWriter.h"

using namespace randomize::gemu;
using namespace std;

int main (int argc, char ** argv) {
    MusicGenerator musicGenerator;
    string name = "recurrence";

    if (argc == 2) {
        int log_level = stoi(argv[1]);
        LOG_LEVEL(log_level)
        LOG_LEVEL(LOG_ERROR_LEVEL)
    } else {
        LOG_LEVEL(LOG_DEBUG_LEVEL)
    }
    LOG_DEBUG("debug")
    LOG_INFO("info")
    LOG_WARN("warn")
    LOG_ERROR("error")

    vector<bool> recurrenceTracking  = musicGenerator.generateRecurrence(20);

    WavWriter(name + ".wav", 22050).write(musicGenerator.getSong());
    LilypondWriter(name + ".ly").write(musicGenerator.getSong());

    stringstream data;
    stringstream data2;
//    LOG_LEVEL(LOG_VERBOSE_LEVEL)
    int i = 0;
    for (vector<shared_ptr<ComplexNote>> chord : musicGenerator) {
        double conson = consonance(chord);
        cout << "consonance(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote())
                << ") = " << conson << endl;
        data << i << " " << conson << endl;
        data2 << i << " " << recurrenceTracking[i] << endl;
        i++;
    }
    writeToFile("plot.dat", data.str());
    writeToFile("plot2.dat", data2.str());

}
