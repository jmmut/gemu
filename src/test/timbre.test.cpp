
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "io/LilypondWriter.h"
#include "io/TextSongWriter.h"


using namespace randomize::gemu;
using namespace std;

int main() {
    MusicGenerator musicGenerator;
    string name("timbre.test");


    LOG_LEVEL(LOG_VERBOSE_LEVEL)

    musicGenerator.generateHarmonicNoteTest();

    TextSongWriter(cout, TextSongWriter::BASIC).write(musicGenerator.getSong());
    WavWriter(name + ".wav", 22050).write(musicGenerator.getSong());
    LilypondWriter(name + ".ly").write(musicGenerator.getSong());

    return 0;
}

