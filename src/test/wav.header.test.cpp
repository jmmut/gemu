#include <io/WavSong.h>
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "io/LilypondWriter.h"
#include "io/TextSongWriter.h"


using namespace randomize::gemu;
using namespace std;

int main(int argc, char ** argv) {
    if (argc != 2) {
        cout << "pass a wav as argument" << endl;
        return 1;
    }
    string name(argv[1]);

    WavSong wavSong;

    wavSong.CargarWAV(name);

    cout << wavSong.headerToString(3) << endl;

    return 0;
}

