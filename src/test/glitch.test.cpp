//
// Created by jmmut on 22/03/15.
//
#include "io/SoundStreamWriter.h"
#include "io/WavWriter.h"
#include "io/LilypondWriter.h"
#include "io/TextSongWriter.h"
#include "core/MusicGenerator.h"

using namespace randomize::gemu;
using namespace std;

int main (int argc, char ** argv) {
    MusicGenerator musicGenerator;
    std::string name = "glitch";

    if (argc >= 2) {
        int logLevel = stoi(argv[1]);
//        LOG_LEVEL(log_level)
        LOG_LEVEL(logLevel);
    } else {
        LOG_LEVEL(LOG_DEBUG_LEVEL)
    }

    LOG_DEBUG("debug")
    LOG_INFO("info")
    LOG_WARN("warn")
    LOG_ERROR("error")

    bool print = false;

    if (argc >= 3) {
        print = true;
    }

    int samplesPerSecond = 22050;
    int i = 0;

    musicGenerator.generateGlitch<unsigned char (double)>(20, [&i] (double time) {
        return 4*i++;
//        return (i >> 13) * i * (i * (i++ >> 10));
    });

    LOG_INFO("finished generating");

    TextSongWriter(cout, TextSongWriter::BASIC).write(musicGenerator.getSong());
    WavWriter(name + ".wav", 22050).setDebug(print, 0, 10000.0/samplesPerSecond).write(musicGenerator.getSong());
    LilypondWriter(name + ".ly").write(musicGenerator.getSong());
    SoundStreamWriter(samplesPerSecond).write(musicGenerator.getSong());
}
