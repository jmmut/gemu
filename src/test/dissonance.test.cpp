#include <iostream>
#include <map>
#include <string>
#include "util/Decider.h"
#include "core/ComplexNote.h"
#include "core/SimpleNote.h"
#include "core/MusicGenerator.h"
#include "utils/time/TimeRecorder.h"
#include "io/WavSong.h"


using namespace std;
using namespace randomize::gemu;
using randomize::utils::time::TimeRecorder;

int main() {
    LOG_LEVEL(LOG_VERBOSE_LEVEL)
    vector<shared_ptr<ComplexNote>> chord;
    vector<double> instants{0, 0};
    double duration = 2;
    cout << "EPSILON_DISSONANCE = " << EPSILON_DISSONANCE << endl;
    cout << "toFrequency(D4) = " << toFrequency("D4") << endl;
    cout << "toFrequency(D5) = " << toFrequency("D5") << endl;
/*
    chord.emplace_back(new SimpleNote(duration, toNote("C4"), 1, 511));
    chord.emplace_back(new SimpleNote(duration, toNote("G4"), 1, 63));
    cout << "dissonance(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote()) << ") = "
            << dissonance(chord) << endl;

    chord.clear();
    chord.emplace_back(new SimpleNote(duration, toNote("C4"), 1, 511));
    chord.emplace_back(new SimpleNote(duration, toNote("C5"), 1, 15));
    cout << "dissonance(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote()) << ") = "
            << dissonance(chord) << endl;

    chord.clear();
    chord.emplace_back(new SimpleNote(duration, toNote("G4"), 1, 255));
    chord.emplace_back(new SimpleNote(duration, toNote("C5"), 1, 63));
    cout << "dissonance(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote()) << ") = "
            << dissonance(chord) << endl;


    chord.clear();
    chord.emplace_back(new SimpleNote(duration, toNote("C4"), 1, 1023));
    chord.emplace_back(new SimpleNote(duration, toNote("E4"), 1, 511));
    cout << "dissonance(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote()) << ") = "
            << dissonance(chord) << endl;

    cout << endl << endl << endl;

*/

    multimap<double, vector<shared_ptr<ComplexNote>>> dissonances;
    double epsilon = SQRT_12_2 * 0.005;
    Song song(duration*12, 60, string(""));

    for (int j = 0; j < 5; ++j) {
//    for (int j = 1; j < 2; ++j) {
        vector<TimeRecorder> trs;
        trs.resize(6);
        trs[0].start();
        trs[1].start();
        dissonances.clear();
        trs[1].stop();
        cout << trs[1].toString("dissonances clear") << endl;
        cout << "epsilon * j = " << epsilon * j << endl;
        song.name = string("epsilon") + to_string(epsilon * j);

        trs[1].start();
        WavSong wavSong(song);
        trs[1].stop();
        cout << trs[1].toString("wav song malloc") << endl;

        trs[1].start();
        for (int i = toNote("Cs4"); i < toNote("Cs5"); i++) {
            chord.clear();
            chord.emplace_back(new SimpleNote(duration, toNote("C4"), 0.25, (1 << 22)-1));
            chord.emplace_back(new SimpleNote(duration, i, 0.25, (1 << 22)-1));
            if (j == 1 && i == toNote("Fs4")) {
                cout << "---" << endl;
            }
            dissonances.insert(pair<double, vector<shared_ptr<ComplexNote>>>(dissonance(chord, instants, epsilon * j)
                    , chord));
        }
        trs[1].stop();
        cout << trs[1].toString("dissonance insertion") << endl;
        int k = 0;
        trs[1].start();
        for(auto dis: dissonances) {
            cout << dis.first << ": " << toName(dis.second[0]->getNote()) + ", " + toName(dis.second[1]->getNote()) << endl;
            for (int i = 0; i < 2; ++i) {
                wavSong.setCursor(k*duration);
                trs[2].start();
                wavSong.writeNote(*dis.second[i]);
                trs[2].stop();
                cout << trs[2].toString("write note") << endl;
            }
            k++;
        }
        trs[1].stop();
        cout << trs[1].toString("wav note writing") << endl;

        trs[1].start();
        wavSong.Guardar(song.name + ".wav");
        trs[1].stop();
        cout << trs[1].toString("wav saving") << endl;

        trs[0].stop();
        cout << trs[0];
        cout << endl << endl;

    }



    return 0;
}
