/**
 * @file bmp.writer.test.cpp
 * @author jmmut 
 * @date 2016-06-15.
 */

#include <io/WavSong.h>
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "io/LilypondWriter.h"
#include "io/TextSongWriter.h"
#include "io/BmpWriter.hpp"


using namespace randomize::gemu;
using namespace std;

int main(int argc, char ** argv) {
    string filename = string(argv[0]) + ".bmp";

    randomize::BmpWriter bmpWriter(filename, 50, 100, 3, 6);

    bmpWriter.write(255, 127, 0);
    bmpWriter.write(255, 127, 0);
    bmpWriter.write(255, 127, 0);
    bmpWriter.write(255, 255, 0);
    bmpWriter.write(255, 127, 0);
    bmpWriter.write(255, 127, 0);
    bmpWriter.write(255, 127, 0);
    bmpWriter.write(255, 127, 0);
    bmpWriter.write(255, 127, 0);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);
    bmpWriter.write(0, 127, 255);

    return 0;
}

