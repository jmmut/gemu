//
// Created by jmmut on 22/03/15.
//
#include <sstream>
#include "io/WavWriter.h"
#include "io/LilypondWriter.h"
#include "core/MusicGenerator.h"

using namespace randomize::gemu;
using namespace std;

int main () {
    MusicGenerator musicGenerator;
    string name = "dissonance.plot";

    LOG_LEVEL(LOG_DEBUG_LEVEL)
    musicGenerator.generateDissonance(20);

    WavWriter(name + ".wav", 22050).write(musicGenerator.getSong());
    LilypondWriter(name + ".ly").write(musicGenerator.getSong());

    stringstream data;
//    LOG_LEVEL(LOG_VERBOSE_LEVEL)
    for (vector<shared_ptr<ComplexNote>> chord : musicGenerator) {
        double conson = consonance(chord);
        cout << "consonance(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote())
                << ") = " << conson << endl;
        data << conson << endl;
    }
    writeToFile("plot.dat", data.str());

}
