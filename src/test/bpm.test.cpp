/**
 * @file bpm.test.cpp
 * @author jmmut 
 * @date 2016-05-16.
 */

#include <iostream>
#include <string>
#include <utils/SignalHandler.h>
#include "io/WavSong.h"
#include "core/BeatsPerMinute.h"
#include "utils/test/TestSuite.h"

using namespace std;
using namespace randomize;
using namespace randomize::gemu;

int firstOrder(int argc, char **argv) {
    vector<float> data = {1.1, 2.2, 3.3, 2.4, 1.5, 2.6, 3.7, 2.8, 1.9, 2.11, 2.12, 3.13, 2.14, 1.15,
                          2.16, 2.17, 2.18, 3.19, 2.21};

    double bpm = periodInSamples(1, [&] (function<void(size_t, float)> next) {
        size_t i = 0;
        for (auto value : data) {
            next(i++, value);
        }
    });
    int fails = 0;
    fails = bpm != 5.0;
    cout << "bpm = " << bpm << endl;
    return fails;
}

int secondOrder(int argc, char **argv) {
    vector<float> data = {1, 2, 4, 2, 3, 2, 4, 2, 3, 2, 4, 1, 2, 1, 3, 2.5, 2.8, 1, 2};

    double bpm = periodInSamples(2, [&] (function<void(size_t, float)> next) {
        size_t i = 0;
        for (auto value : data) {
            next(i++, value);
        }
    });
    int fails = 0;
    fails = bpm != 4.0;
    cout << "bpm = " << bpm << endl;
    return fails;
}

int main(int argc, char **argv) {

    SignalHandler::activate(SIGABRT);

    if (argc != 3) {
        cout << "If you don't pass a wav as argument and a nesting level, I will do just a test" << endl;
//        try {
            utils::test::TestSuite suite(argc, argv, {firstOrder, secondOrder}, "BeatsPerMinute");
            return suite.countFailed();
//        } catch (randomize::utils::exception::StackTracedException se) {
//            cout << "caught exception: " << se.what() << endl;
//            return 1;
//        }
    }
    string name(argv[1]);

    WavSong wavSong;

    wavSong.CargarWAV(name);
    BeatsPerMinute bpm(wavSong, 0, wavSong.getSamples());
    double period = bpm.periodInSamples(stoul(argv[2]));
    double beats = bpm.bpm(bpm.periodInSeconds(period));
    cout << "period = " << period << endl;
    cout << "bpm: " << beats << endl;
    return 0;
}
