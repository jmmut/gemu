//
// Created by jmmut on 2015-04-21.
//


#include "core/MusicGenerator.h"
#include "utils/test/TestSuite.h"

using namespace randomize::gemu;
using namespace std;

void printChordDraftSong(Song &song, Song::Appender &appender);
bool differentNote(shared_ptr<ComplexNote> note, string noteName);
bool equals(Chord notes, vector<string> names);

/**
 * 5 [de fg ]
 * 3 [d e f ]
 */
int test2 (int, char**) {
    bool ok = true;
    Song song(10, 120, "test");
    Chord last;
    Chord draft;

    song.tracks.push_back(Track());
    song.tracks.push_back(Track());

    Song::Appender appender(&song);

    appender.pushBack(0, make_shared<SimpleNote>(0.5, toNote("D5"), 1, 1));
//    cout << "\ndraft: " << toString(appender.getDraftOfNewChord(), music.tempo());   // does not work
    appender.pushBack(1, make_shared<SimpleNote>(1, toNote("D3"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (last[0]->getNote() != toNote("D5") || last[1]->getNote() != toNote("D3")
        || draft[0] != nullptr || draft[1] == nullptr || draft[1]->getNote() != toNote("D3")) {
        ok = false;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(0, make_shared<SimpleNote>(1, toNote("E5"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (last[0]->getNote() != toNote("E5") || last[1]->getNote() != toNote("D3")
        || draft[0] == nullptr || draft[0]->getNote() != toNote("E5") || draft[1] != nullptr) {
        ok = false;
        printChordDraftSong(song, appender);
    }
    appender.pushBack(1, make_shared<SimpleNote>(1, toNote("E3"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (last[0]->getNote() != toNote("E5") || last[1]->getNote() != toNote("E3")
        || draft[0] != nullptr || draft[1] == nullptr || draft[1]->getNote() != toNote("E3")) {
        ok = false;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(0, make_shared<SimpleNote>(0.5, toNote("F5"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (last[0]->getNote() != toNote("F5") || last[1]->getNote() != toNote("E3")
        || draft[0] != nullptr || draft[1] != nullptr) {
        ok = false;
        printChordDraftSong(song, appender);
    }
    appender.pushBack(1, make_shared<SimpleNote>(1, toNote("F3"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (last[0]->getNote() != toNote("F5") || last[1]->getNote() != toNote("E3")
        || draft[0] != nullptr || draft[1] == nullptr || differentNote(draft[1], "F3")) {
        ok = false;
        printChordDraftSong(song, appender);
    }
    appender.pushBack(0, make_shared<SimpleNote>(1, toNote("G5"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (last[0]->getNote() != toNote("G5") || last[1]->getNote() != toNote("F3")
        || draft[0] != nullptr || draft[1] != nullptr) {
        ok = false;
        printChordDraftSong(song, appender);
    }
//    track.emplace_back(new SimpleNote(1, toNote("C4"), 1, 1));
//    track.emplace_back(new SimpleNote(0.5, toNote("C5"), 1, 1));
//    track.emplace_back(new SimpleNote(2, toNote("C6"), 1, 1));
//    track.emplace_back(new SimpleNote(0.125, toNote("D4"), 1, 1));
//    track.emplace_back(new SimpleNote(0.25, toNote("E4"), 1, 1));
//    track.emplace_back(new SimpleNote(1, toNote("F4"), 1, 1));
//    music.getSong().tracks.push_back(track);
//    track2.emplace_back(new SimpleNote(2, toNote("C#4"), 1, 1));
//    track2.emplace_back(new SimpleNote(1, toNote("C#5"), 1, 1));
//    track2.emplace_back(new SimpleNote(0.25, toNote("C#6"), 1, 1));
//    track2.emplace_back(new SimpleNote(0.5, toNote("D#4"), 1, 1));
//    track2.emplace_back(new SimpleNote(0.25, toNote("F#4"), 1, 1));
//    track2.emplace_back(new SimpleNote(1, toNote("G#4"), 1, 1));
//    music.getSong().tracks.push_back(track2);

//    cout << music.toString();
//    for(auto chord : music) {
//        for (auto note : chord) {
//            cout << toName(note->getBaseFrequency()) << ", ";
//        }
//        cout << endl;
//    }

/*
//    for(vector<shared_ptr<ComplexNote>> chord : music) {
    for (MusicGenerator::Iterator it(music.begin()); it != music.end(); ++it) {
//    for(auto chord : music) {
        for (auto note : *it) {
            cout << toName(note->getBaseFrequency()) << ", ";
        }
        cout << endl;
        if (i > 0) {
            i--;
            cout << " adding ";
            track.emplace_back(new SimpleNote(1, toNote("C4"), 1, 1));
            music.getSong().tracks.push_back(track);
        }
        for (auto note : *it) {
            cout << toName(note->getBaseFrequency()) << ", ";
        }
        cout << endl;
        cout << endl;
    }
    */
    return !ok;
}

/**
 * 5 [de fg ]
 * 3 [d e f ]
 * 2 [d  ef ]
 */
int testTripleTrack(int argc, char ** argv) {
    int fails = 0;
    Song song(10, 120, "test");
    Chord last;
    Chord draft;

    song.tracks.resize(3);
    Song::Appender appender(&song);

    appender.pushBack(0, make_shared<SimpleNote>(0.5, toNote("D5"), 1, 1));
//    cout << "\ndraft: " << toString(appender.getDraftOfNewChord(), music.tempo());   // does not work
    appender.pushBack(1, make_shared<SimpleNote>(1, toNote("D3"), 1, 1));
    appender.pushBack(2, make_shared<SimpleNote>(1.5, toNote("D2"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (!equals(last, {"D5", "D3", "D2"}) || !equals(draft, {"", "D3", "D2"})) {
//    if (differentNote(last[0], "D5") || draft[0] != nullptr
//        || differentNote(last[1], "D3") || draft[1] == nullptr || differentNote(draft[1], "D3")
//        || differentNote(last[2], "D2") || draft[2] == nullptr || differentNote(draft[2], "D2")
//            ) {
        fails++;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(0, make_shared<SimpleNote>(1, toNote("E5"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (differentNote(last[0], "E5") || draft[0] == nullptr || differentNote(draft[0], "E5")
        || differentNote(last[1], "D3") || draft[1] != nullptr
        || differentNote(last[2], "D2") || draft[2] == nullptr || differentNote(draft[2], "D2")
            ) {
        fails++;
        printChordDraftSong(song, appender);
    }
    appender.pushBack(1, make_shared<SimpleNote>(1, toNote("E3"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (differentNote(last[0], "E5") || draft[0] != nullptr
        || differentNote(last[1], "E3") || draft[1] == nullptr || differentNote(draft[1], "E3")
        || differentNote(last[2], "D2") || draft[2] != nullptr
            ) {
        fails++;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(0, make_shared<SimpleNote>(0.5, toNote("F5"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (differentNote(last[0], "E5") || draft[0] == nullptr || differentNote(draft[0], "F5")
        || differentNote(last[1], "E3") || draft[1] == nullptr || differentNote(draft[1], "E3")
        || differentNote(last[2], "D2") || draft[2] != nullptr
            ) {
        fails++;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(2, make_shared<SimpleNote>(0.5, toNote("E2"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (differentNote(last[0], "F5") || draft[0] != nullptr
        || differentNote(last[1], "E3")|| draft[1] != nullptr
        || differentNote(last[2], "E2")|| draft[2] != nullptr
            ) {
        fails++;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(1, make_shared<SimpleNote>(1, toNote("F3"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (differentNote(last[0], "F5") || draft[0] != nullptr
        || differentNote(last[1], "E3") || draft[1] == nullptr || differentNote(draft[1], "F3")
        || differentNote(last[2], "E2") || draft[2] != nullptr
            ) {
        fails++;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(0, make_shared<SimpleNote>(1, toNote("G5"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (not equals(last, {"F5", "E3", "E2"}) or not equals(draft, {"G5", "F3", ""})) {
        fails++;
        printChordDraftSong(song, appender);
    }

    appender.pushBack(2, make_shared<SimpleNote>(1, toNote("F2"), 1, 1));
    last = appender.getLastCompleteChord();
    draft = appender.getDraftOfNewChord();
    if (not equals(last, {"G5", "F3", "F2"}) or not equals(draft, {"", "", ""})) {
        fails++;
        printChordDraftSong(song, appender);
    }

    return fails;
}

bool differentNote(shared_ptr<ComplexNote> note, string noteName) {
    return note->getNote() != toNote(noteName);
}

void printChordDraftSong(Song &song, Song::Appender &appender) {
    cout << "\nlast chord: " << toString(appender.getLastCompleteChord(), song.tempo);
    cout << "\ndraft: " << toString(appender.getDraftOfNewChord(), song.tempo);
    cout << song.toString();
}

int metaTest(int argc, char ** argv) {
    int fails = 0;
    Track t;
    t.push_back(make_shared<SimpleNote>(0.5, toNote("E2"), 1, 1));
    t.push_back(make_shared<SimpleNote>(0.5, toNote("E3"), 1, 1));
    t.push_back(make_shared<SimpleNote>(0.5, toNote("E4"), 1, 1));
    fails += !equals(t, {"E2", "E3", "E4"});

    t.clear();
    t.push_back(make_shared<SimpleNote>(0.5, toNote("E2"), 1, 1));
    t.push_back(nullptr);
    t.push_back(make_shared<SimpleNote>(0.5, toNote("E4"), 1, 1));
    fails += !equals(t, {"E2", "", "E4"});

    return fails;
}


bool equals(Chord notes, vector<string> names) {
    if (notes.size() != names.size()) {
        return false;
    }
    for (unsigned int i = 0; i < notes.size(); ++i) {

        if (notes[i] == nullptr && names[i].size() == 0) {
            // if both are missing, it's a correct match
            continue;
        } else if ((static_cast<int>(notes[i] == nullptr)
                    + static_cast<int>(names[i].size() == 0))
                   == 1) {
            // if one is missing but not the other, it's a mismatch
            return false;
        } else if (differentNote(notes[i], names[i])) {
            return false;
        } else {
            continue;
        }
    }
    return true;
}

int main(int argc, char ** argv) {

    randomize::utils::test::TestSuite suite(argc, argv, {
            test2,
            metaTest,
            testTripleTrack,
    }, "appender");
    return suite.countFailed();
}

