//
// Created by jmmut on 2015-04-21.
//


#include "core/MusicGenerator.h"
#include <cassert>
#include "utils/test/TestSuite.h"

using namespace randomize::gemu;
using namespace std;

int test1(int argc, char ** argv) {
//    shared_ptr<ComplexNote> shortNote(new SimpleNote(1, toNote("E4"), 0.5, 1));
//    shared_ptr <ComplexNote> longNote(new SimpleNote(3, toNote("E3"), 0.5, 1));
    MusicGenerator music;
    music.generateRecurrence(8);
    cout << "music.toString() = " << music.getSong().toString() << endl;
    for (Song::Iterator it(music.begin()); it != music.end(); ++it) {
        cout << "cursors: ";
        for (auto cursor : it.getCursors()) {
            cout << cursor << ", ";
        }
        cout << endl << "<";
        for (auto note : *it) {
            cout << toName(note->getBaseFrequency()) << ", ";
        }
        cout << ">" << endl;
    }
    cout << music.getSong().toString() << endl;

    return 0;
}

/**
 * [c       c   c               de f       ]
 * [c               c       c d   f g       ]
 *  ^       ^   ^   ^       ^ ^ ^^^^^
 *  11 valid chords
 */
int test2 (int argc, char ** argv) {
    MusicGenerator music;
    Track track, track2;
    track.emplace_back(new SimpleNote(1, toNote("C4"), 1, 1));
    track.emplace_back(new SimpleNote(0.5, toNote("C5"), 1, 1));
    track.emplace_back(new SimpleNote(2, toNote("C6"), 1, 1));
    track.emplace_back(new SimpleNote(0.125, toNote("D4"), 1, 1));
    track.emplace_back(new SimpleNote(0.25, toNote("E4"), 1, 1));
    track.emplace_back(new SimpleNote(1, toNote("F4"), 1, 1));
    music.getSong().tracks.push_back(track);
    track2.emplace_back(new SimpleNote(2, toNote("C#4"), 1, 1));
    track2.emplace_back(new SimpleNote(1, toNote("C#5"), 1, 1));
    track2.emplace_back(new SimpleNote(0.25, toNote("C#6"), 1, 1));
    track2.emplace_back(new SimpleNote(0.5, toNote("D#4"), 1, 1));
    track2.emplace_back(new SimpleNote(0.25, toNote("F#4"), 1, 1));
    track2.emplace_back(new SimpleNote(1, toNote("G#4"), 1, 1));
    music.getSong().tracks.push_back(track2);
    int numChords = 0;
    for(auto chord : music) {
        for (auto note : chord) {
            cout << toName(note->getBaseFrequency()) << ", ";
        }
        cout << endl;
        numChords++;
    }

/*
//    for(vector<shared_ptr<ComplexNote>> chord : music) {
    for (MusicGenerator::Iterator it(music.begin()); it != music.end(); ++it) {
//    for(auto chord : music) {
        for (auto note : *it) {
            cout << toName(note->getBaseFrequency()) << ", ";
        }
        cout << endl;
        if (i > 0) {
            i--;
            cout << " adding ";
            track.emplace_back(new SimpleNote(1, toNote("C4"), 1, 1));
            music.getSong().tracks.push_back(track);
        }
        for (auto note : *it) {
            cout << toName(note->getBaseFrequency()) << ", ";
        }
        cout << endl;
        cout << endl;
    }
    */
    return numChords != 11;
}

/**
 * [c       d   e               fg a       ]
 * [c               d       e   f ga       ]
 *  ^       ^   ^   ^       ^   ^^^^
 *  9 valid positions
 */
int test3(int argc, char ** argv) {
    bool ok = true;

    MusicGenerator music;
    Track track, track2;
    track.emplace_back(new SimpleNote(1,     toNote("C3"), 1, 1));
    track.emplace_back(new SimpleNote(0.5,   toNote("D3"), 1, 1));
    track.emplace_back(new SimpleNote(2,     toNote("E3"), 1, 1));
    track.emplace_back(new SimpleNote(0.125, toNote("F3"), 1, 1));
    track.emplace_back(new SimpleNote(0.25,  toNote("G3"), 1, 1));
    track.emplace_back(new SimpleNote(1,     toNote("A3"), 1, 1));
    music.getSong().tracks.push_back(track);
    track2.emplace_back(new SimpleNote(2,     toNote("C5"), 1, 1));
    track2.emplace_back(new SimpleNote(1,     toNote("D5"), 1, 1));
    track2.emplace_back(new SimpleNote(0.5,   toNote("E5"), 1, 1));
    track2.emplace_back(new SimpleNote(0.25,  toNote("F5"), 1, 1));
    track2.emplace_back(new SimpleNote(0.125, toNote("G5"), 1, 1));
    track2.emplace_back(new SimpleNote(1,     toNote("A5"), 1, 1));
    music.getSong().tracks.push_back(track2);
    Song::Iterator iterator(&music.getSong());

    ok = ok && ((*iterator)[0]->getNote() == toNote("C3"));
    ok = ok && ((*iterator)[1]->getNote() == toNote("C5"));
    ++iterator;
    ok = ok && ((*iterator)[0]->getNote() == toNote("D3"));
    ok = ok && ((*iterator)[1]->getNote() == toNote("C5"));
    ++iterator;
    ok = ok && ((*iterator)[1]->getNote() == toNote("C5"));
    ++iterator;
    ok = ok && ((*iterator)[0]->getNote() == toNote("E3"));
    ok = ok && ((*iterator)[1]->getNote() == toNote("D5"));

    ++iterator;
    ++iterator;
    ++iterator;
    ok = ok && ((*iterator)[0]->getNote() == toNote("G3"));
    ok = ok && ((*iterator)[1]->getNote() == toNote("F5"));

    ++iterator;
    ok = ok && ((*iterator)[0]->getNote() == toNote("G3"));
    ok = ok && ((*iterator)[1]->getNote() == toNote("G5"));

    ++iterator;
    ok = ok && ((*iterator)[0]->getNote() == toNote("A3"));
    ok = ok && ((*iterator)[1]->getNote() == toNote("A5"));

    return !ok;
}

/**
 * [c       d   e               fg a       ]
 * [c               d       e   f ga       ]
 * [c    d      e               f          ]
 *  ^    ^  ^   ^   ^       ^   ^^^^
 *  10 valid positions
 */
int tripleTrack(int argc, char **argv) {
    int fails = 0;

    MusicGenerator music;
    std::vector<Track> tracks(3);
    tracks[0].emplace_back(new SimpleNote(1,     toNote("C3"), 1, 1));
    tracks[0].emplace_back(new SimpleNote(0.5,   toNote("D3"), 1, 1));
    tracks[0].emplace_back(new SimpleNote(2,     toNote("E3"), 1, 1));
    tracks[0].emplace_back(new SimpleNote(0.125, toNote("F3"), 1, 1));
    tracks[0].emplace_back(new SimpleNote(0.25,  toNote("G3"), 1, 1));
    tracks[0].emplace_back(new SimpleNote(1,     toNote("A3"), 1, 1));
    music.getSong().tracks.push_back(tracks[0]);
    tracks[1].emplace_back(new SimpleNote(2,     toNote("C5"), 1, 1));
    tracks[1].emplace_back(new SimpleNote(1,     toNote("D5"), 1, 1));
    tracks[1].emplace_back(new SimpleNote(0.5,   toNote("E5"), 1, 1));
    tracks[1].emplace_back(new SimpleNote(0.25,  toNote("F5"), 1, 1));
    tracks[1].emplace_back(new SimpleNote(0.125, toNote("G5"), 1, 1));
    tracks[1].emplace_back(new SimpleNote(1,     toNote("A5"), 1, 1));
    music.getSong().tracks.push_back(tracks[1]);
    tracks[2].emplace_back(new SimpleNote(0.625, toNote("C6"), 1, 1));
    tracks[2].emplace_back(new SimpleNote(0.875, toNote("D6"), 1, 1));
    tracks[2].emplace_back(new SimpleNote(2,     toNote("E6"), 1, 1));
    tracks[2].emplace_back(new SimpleNote(1.375, toNote("F6"), 1, 1));
    music.getSong().tracks.push_back(tracks[2]);
    Song::Iterator iterator(&music.getSong());

    Chord c = *iterator;
    cout << "c.size() = " << c.size() << endl;
    cout << "c[0]->getNote() = " << c[0]->getNote() << endl;
    cout << "c[1]->getNote() = " << c[1]->getNote() << endl;
    cout << "c[2]->getNote() = " << c[2]->getNote() << endl;
    fails += ((*iterator)[0]->getNote() != toNote("C3"));
    fails += ((*iterator)[1]->getNote() != toNote("C5"));
    fails += ((*iterator)[2]->getNote() != toNote("C6"));
    ++iterator;
    fails += ((*iterator)[0]->getNote() != toNote("C3"));
    fails += ((*iterator)[1]->getNote() != toNote("C5"));
    fails += ((*iterator)[2]->getNote() != toNote("D6"));
    ++iterator;
    fails += ((*iterator)[0]->getNote() != toNote("D3"));
    fails += ((*iterator)[1]->getNote() != toNote("C5"));
    fails += ((*iterator)[2]->getNote() != toNote("D6"));
    ++iterator;
    fails += ((*iterator)[0]->getNote() != toNote("E3"));
    fails += ((*iterator)[1]->getNote() != toNote("C5"));
    fails += ((*iterator)[2]->getNote() != toNote("E6"));
    ++iterator;
    fails += ((*iterator)[0]->getNote() != toNote("E3"));
    fails += ((*iterator)[1]->getNote() != toNote("D5"));
    fails += ((*iterator)[2]->getNote() != toNote("E6"));

    ++iterator;
    ++iterator;
    ++iterator;
    fails += ((*iterator)[0]->getNote() != toNote("G3"));
    fails += ((*iterator)[1]->getNote() != toNote("F5"));
    fails += ((*iterator)[2]->getNote() != toNote("F6"));

    ++iterator;
    fails += ((*iterator)[0]->getNote() != toNote("G3"));
    fails += ((*iterator)[1]->getNote() != toNote("G5"));
    fails += ((*iterator)[2]->getNote() != toNote("F6"));

    ++iterator;
    fails += ((*iterator)[0]->getNote() != toNote("A3"));
    fails += ((*iterator)[1]->getNote() != toNote("A5"));

    return fails;
}


int main(int argc, char **argv) {
    randomize::utils::test::TestSuite(argc, argv, {
            test1,
            test2,
            test3,
            tripleTrack,
    }, "iterator");

    return 0;
}

