//
// Created by jmmut on 2015-04-09.
//

#include "io/TextSongWriter.h"
#include "io/WavWriter.h"
#include "io/LilypondWriter.h"
#include "core/MusicGenerator.h"

using namespace randomize::gemu;
using namespace std;

int main() {
    MusicGenerator musicGenerator;
    string name("pattern.test");


    LOG_LEVEL(LOG_VERBOSE_LEVEL)

    musicGenerator.generateHarmonicPatternTest();


    WavWriter(name + ".wav", 22050)
            .setDebug(true, musicGenerator.seconds() / 2, musicGenerator.seconds() / 10 * 6)
            .write(musicGenerator.getSong());
    LilypondWriter(name + ".ly").write(musicGenerator.getSong());
    TextSongWriter(cout, TextSongWriter::VERBOSE).write(musicGenerator.getSong());

    return 0;
}
