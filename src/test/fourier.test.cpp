/**
 * @file fourier.cpp
 * @author jmmut 
 * @date 2016-04-20.
 */

#include <string>
#include <iostream>
#include <core/Fourier.h>
#include <tclap/CmdLine.h>
#include <utils/SignalHandler.h>
#include <core/SimpleNote.h>
#include <io/LilypondWriter.h>
#include <io/WavWriter.h>
#include <io/TextSongWriter.h>
#include <io/BmpWriter.hpp>

using std::string;
using std::vector;
using std::endl;
using std::ofstream;
using std::cout;
using namespace randomize::gemu;

int main(int argc, const char *const *argv) {
    TCLAP::CmdLine cmd("This program transforms a wav file into its Fourier transform, "
            "trying to fit the discovered frequencies into musical notes");

    TCLAP::ValueArg<string> logLevelArg("l", "log-level",
            "set the log level: " + randomize::utils::container_to_string_ss(randomize::log::getNames()),
            false, "info", "string", cmd);

    TCLAP::ValueArg<string> fileArg("f", "file", "set the wav file to analyze",
            true, "info", "string", cmd);
    TCLAP::ValueArg<string> outputArg("o", "output", "base name for all output files",
            true, "info", "string", cmd);


    TCLAP::ValueArg<double> thresholdArg("t", "threshold", "set the volume threshold. suggestion: "
                    "0.1 or 0.05. Play with this value if the program finds too many or too few notes",
            false, -1, "float", cmd);

    TCLAP::ValueArg<double> batchSizeArg("b", "batch-length", "set the batch length in seconds.",
            false, 0.1, "float", cmd);

    TCLAP::ValueArg<double> tempoArg("", "tempo", "set the tempo. If not set, it will "
                    "be 60/(batchlength*4). This is treating a batch as a sixteenth note at 60 ppm.",
            false, 0.1, "float", cmd);
    TCLAP::ValueArg<double> timbreArg("", "timbre", "set the timbre for the wav file, 1 by default",
            false, 1.0, "float", cmd);
    TCLAP::ValueArg<double> volumeArg("v", "volume", "set the volume for the wav file, 1 by default",
            false, 1.0, "float", cmd);

    TCLAP::SwitchArg writeSamplesArg("s", "samples", "write the samples as well.", cmd);
    TCLAP::SwitchArg writeWavArg("w", "wav",
            "write the wav file as well with the discovered frequencies as sines", cmd);
    TCLAP::SwitchArg writePartitureArg("p", "partiture",
            "write the lilypond file as well. for partitures and midi", cmd);
    TCLAP::SwitchArg ignoreErrorsArg("", "ignore-errors",
            "try to finish the execution even if there are errors", cmd);

    cmd.parse(argc, argv);

    SigsegvPrinter::activate();
    LOG_LEVEL(randomize::log::parseLogLevel(logLevelArg.getValue().c_str()));
    string wavFile = fileArg.getValue();
    string outputName = outputArg.isSet() ? outputArg.getValue() : wavFile;
    double threshold = thresholdArg.getValue();
    double batchSize = tempoArg.isSet() and not batchSizeArg.isSet() ?
            60.0 / 4 / tempoArg.getValue() : batchSizeArg.getValue();
    double tempo = tempoArg.isSet()? tempoArg.getValue() : 60.0 / (batchSize * 4);
    bool writeSamples = writeSamplesArg.getValue();
    bool writePartiture = writePartitureArg.getValue();
    bool writeWav = writeWavArg.getValue();
    int maxTracks = writePartiture? 16 : 50;
    double volume = volumeArg.getValue();
    double timbre = timbreArg.getValue();
    bool ignoreErrors = ignoreErrorsArg.getValue();

    WavSong wavSong;
    if (!wavSong.CargarWAV(wavFile)) {
        std::cout << "could not load file " << wavFile << endl;
        return 1;
    }

    if (writeSamples) {
        writeToFile(outputName + ".fourier.samples.dat", [&](ofstream& file) {
            wavSong.forEachSample([&](float sample){
                file << sample << endl;
            });
        });
    }

    Fourier fourier(wavSong);
    fourier.setBatchSize(batchSize);
    fourier.setTimbre(timbre);
    Song song;
    song.seconds = wavSong.getSeconds();
    song.name = outputName;
    song.tempo = tempo;
    song.tracks.resize(maxTracks);
    Song::Appender appender(&song);
    vector<double> maxes;
    double bmpZoom = 1;
    auto sizes = fourier.getSpectrumSizes();
    cout << "sizes.first = " << sizes.first << endl;
    cout << "sizes.second = " << sizes.second << endl;
    randomize::BmpWriter bmpWriter(outputName + ".fourier.spectrum.bmp",
            bmpZoom * sizes.first, bmpZoom * sizes.second*4, sizes.first, sizes.second);

    sizes.first = 0;
    sizes.second = 0;

    ofstream highest;
    openFileOrThrow(outputName + ".fourier.highest.dat", highest);

    while(fourier.transformNextBatch()) {

        Fourier::BatchAnalysis analysis = fourier.guessNotes(threshold);

        if (randomize::log::log_level == LOG_DEBUG_LEVEL) {
            //                writeToFile(outputName + ".fourier.real.dat", [&](ofstream &file) {
            //                    fourier.forEachReal([&](double sample) {
            //                        file << sample << endl;
            //                    });
            //                });
            //                writeToFile(outputName + ".fourier.imag.dat", [&](ofstream &file) {
            //                    fourier.forEachImaginary([&](double sample) {
            //                        file << sample << endl;
            //                    });
            //                });
            //                writeToFile(outputName + ".fourier.module.dat", [&](ofstream &file) {
            //                    fourier.forEachComplex([&](double real, double imaginary, unsigned int) {
            //                        file << std::sqrt(real * real + imaginary * imaginary) << endl;
            //                    });
            //                });
            //                writeToFile(outputName + ".fourier.angle.dat", [&](ofstream &file) {
            //                    fourier.forEachComplex([&](double real, double imaginary, unsigned int) {
            //                        file << std::atan2(real, imaginary) << endl;
            //                    });
            //                });
            highest << analysis.integral << endl;
        }

        if (randomize::log::log_level < LOG_INFO_LEVEL) {
            cout << "note:volume found on [" << std::fixed << std::setprecision(3)
                    << fourier.getLastBatchStartCursor() << "-" <<
                    fourier.getLastBatchEndCursor() << "]: ";
            int i = 0;
            for (auto note : analysis.chord) {
                cout << toName(note->getNote()) << ":" << analysis.volumes[i++] << " ";
            }
            cout << endl;
        }

        if (analysis.chord.size() <= song.tracks.size() || ignoreErrors) {
            for (unsigned int j = 0; j < song.tracks.size(); ++j) {
                if (j < analysis.chord.size()) {
                    appender.pushBack(j, analysis.chord[j]);
                } else {
                    appender.pushBack(j, std::make_shared<SilenceNote>(batchSize));
                }
            }
        } else {
            throw randomize::utils::exception::StackTracedException(
                    "too many simultaneous notes. try higher threshold");
        }

        // draw spectrum
        for (double value : analysis.transform) {
            bmpWriter.write(255 * value, 255 * std::sqrt(value), 127);
        }
        sizes.first = analysis.transform.size();
        sizes.second++;
    }


    cout << "sizes.first = " << sizes.first << endl;
    cout << "sizes.second = " << sizes.second << endl;

    if (writePartiture) {
        LilypondWriter(outputName + ".ly").writeAsChords(song);
        TextSongWriter(outputName + ".json", TextSongWriter::JSON).write(song);
    }
    if (writeWav) {
        WavWriter(outputName + ".wav", wavSong.getHeader().samplerate).setVolume(volume).write(song);
    }
    return 0;
}
