/**
 * @file soundStream.test.cpp
 * @author jmmut 
 * @date 2015-09-12.
 */

#include <iostream>
#include "core/MusicGenerator.h"
#include "io/SoundStream.h"
#include "io/SoundStreamWriter.h"
#include "utils/SignalHandler.h"

using namespace randomize::gemu;
//void endian(float element);

unsigned char f(int t) {
//    return 4*(t+1);
    return (t>>13)*t*(t*(t>>10));
}

int main(int argc, char **argv) {

//    endian(0.3262);
//    add the sigsegvprinter
    SigsegvPrinter::activate();

    randomize::sound::SoundStream ss;

    LOG_LEVEL(LOG_DEBUG_LEVEL);

    int t = 0;
    if (argc > 1) {
        while (true) {
            std::cout << f(t);
            t++;
        }
    } else {
        while (true) {
            ss << f(t);
            t++;
            if (t > 100000) {
                break;
            }
        }
    }

    return 0;
}

/*

template <typename T>
struct toByes {
    union {
        T f;
        unsigned char bytes[sizeof(T)];
    };
    unsigned long size = sizeof(T);
};

void endian(float element) {
//    cout <<

    int i;
    printf("%X\n", (*((int *) &element)));
//    std::cout << std::hex << element << std::endl;
    toByes<float> ms4;
    ms4.f = element;
    for (i = ms4.size-1; i >= 0; i--) {
        std::cout << std::hex << (int)ms4.bytes[i] << " ";
    }
    std::cout << std::endl;

    printf("%A\n", element);
    for (i = 0; i < 4; i++) {
        std::cout << std::hex << (((*((int *) &element)) & (0xFF << i*8)) >> i*8 ) << " ";
    }
    std::cout << std::endl;
}
*/

