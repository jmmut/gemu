/**
 * @file MergeableTree.cpp
 * @author jmmut 
 * @date 2016-03-28.
 */


#include <string>
#include <vector>
#include <iostream>
#include "core/MusicGenerator.h"
#include "io/WavWriter.h"
#include "util/MergeableTree.h"
#include "utils/test/TestSuite.h"
#include "utils/SignalHandler.h"
#include "log/log.h"


using namespace randomize;
using namespace std;

struct TestNode {
    double a;
    std::vector<double> b;
    friend std::ostream& operator<<(std::ostream &os, const TestNode &node) {
        return os << "{" << node.a << ", " << container_to_string(node.b) << "}";
    }
};

int basicTest (int argc, char ** argv) {
    MergeableTree<TestNode> tree;

    tree.setNode({0.4, {0.5, 0.6}})
            .addSon(MergeableTree<TestNode>().setNode({0.8, {0.2, 0.1}}))
            .addSon(MergeableTree<TestNode>().setNode({0.9, {0.25, 0.15}}));

    cout << tree << endl;
    stringstream ss;
    ss << tree;

     string verifiedResult = "{node:{0.4, [0.500000, 0.600000]}, sons:[ "
             ",{node:{0.8, [0.200000, 0.100000]}, sons:[]} "
             ",{node:{0.9, [0.250000, 0.150000]}, sons:[]}]}";

    return strncmp(ss.str().c_str(), verifiedResult.c_str(), verifiedResult.size());
}


int main (int argc, char ** argv) {

    SigsegvPrinter::activate();
    SignalHandler::activate(SIGABRT);

    LOG_LEVEL(argc == 2? log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL);

    randomize::utils::test::TestSuite suite(argc, argv, {
            basicTest
    }, "entropy");

    return suite.countFailed();
}
