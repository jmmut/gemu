/**
 * @file TextSongWriter.h
 * @author jmmut 
 * @date 2015-10-11.
 */

#ifndef GEMU_TEXTSONGWRITER_H
#define GEMU_TEXTSONGWRITER_H

#include <fstream>
#include "io/SongWriter.h"

namespace randomize {
namespace gemu {

class TextSongWriter : public SongWriter {
public:
    enum Type {BASIC, VERBOSE, MEZ, JSON};

private:
    std::string name;
    Type type;
    bool useOstream;
    std::ostream &out;

public:
    TextSongWriter(const std::string &name, const Type &type) : name(name), type(type), out(std::cout){
        useOstream = false;
    }

    TextSongWriter(std::ostream& ostream, const Type &type) : type(type), out(ostream) {
        useOstream = true;
    }

    virtual void write(randomize::gemu::Song song) override;

    void writeBasic(randomize::gemu::Song song, std::ostream &songString);
    void writeVerbose(randomize::gemu::Song song, std::ostream &songString);
    void writeMez(randomize::gemu::Song song, std::ostream &songString);
    void writeJSON(Song song, std::ostream &songString);
};

};  // gemu
};  // randomize


#endif //GEMU_TEXTSONGWRITER_H
