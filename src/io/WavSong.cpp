#include <assert.h>
#include "io/WavSong.h"

using namespace std;

namespace randomize {

namespace gemu {


WavSong::WavSong() {
    Init();
}

//WavSong::WavSong(float segundos, int muestras_por_segundo, int bits_por_muestra, int num_canales) {
//    Init();
//    cursor_fin = int(segundos * muestras_por_segundo) * num_canales;
//    CreateWAV(segundos, muestras_por_segundo, bits_por_muestra, num_canales);
//}

WavSong::WavSong(const Song &song, double volume, unsigned int samplesPerSecond, unsigned int bitsPerSample, unsigned int numChannels)
        : song(song) {
    Init();
    CreateWAV(song.seconds, samplesPerSecond, bitsPerSample, numChannels);
    this->volume = volume;
    LOG_EXP(headerToString(3).c_str(), "%s")
}

WavSong::~WavSong() {
    Empty();
}

void WavSong::Init() {
//	w = h = cursor_ini = cursor_fin = despl = 0;
    virtual_cur_fin = virtual_cur_ini = 0;
//	ondas = NULL;
    data = nullptr;
    header = nullptr;

}

/**
* free allocated memory and reset some variables.
*/
void WavSong::Empty() {
//	w = h = cursor_ini = cursor_fin = despl = 0;
    virtual_cur_fin = virtual_cur_ini = 0;
    volume = 1;
    if (data != nullptr) {
        free(data);
    }

    if (header != nullptr) {
        delete header;
    }

//	if (ondas != NULL)
//		SDL_FreeSurface (ondas);
//	ondas = NULL;
    data = nullptr;
    header = nullptr;
}
////////////////////////////////////////// ENTRADA/SALIDA


bool WavSong::CargarWAV(string nom) {
    ifstream f;
    bool ok = true;
    Cabecera c2;

    Empty();

    f.open(nom.c_str(), ios::binary);

    if (!f) {
        cerr << "ERROR: cargarWAV: no se pudo abrir el fichero " << nom << endl;
        return false;
    }

    f.read((char *) (&c2), sizeof(Cabecera));    // leemos cabecera del WAV

    if (c2.bitspersample != 8 && c2.bitspersample != 16 && c2.bitspersample != 32)    //corregimos si bps era incorrecto
    {
        cout << "AVISO: CargarWAV: los bits por muestra no son ni 8 ni 16 ni 32. los hago de 32\n";
        c2.bitspersample = 32;
        //ok = false;
    }

    f.seekg(0, ios::end);

    header = new Cabecera((int) f.tellg() - sizeof(Cabecera), c2.samplerate, c2.bitspersample, c2.numchannels);    // corregimos si sc2size era incorrecto

    data = calloc(header->subchunk2size, 1);    // dame subchunk2size bytes en blanco

    f.seekg(sizeof(Cabecera), ios::beg);
    f.read((char *) data, header->subchunk2size);    // leemos los el cuerpo del WAV

    f.close();

    cursor_ini = 0;
    cursor_fin = header->subchunk2size / (header->bitspersample / 8);

    return ok;
}


bool WavSong::Guardar(string nom) {
    ofstream f;
    int bytes_por_numero(header->bitspersample / 8);
    Cabecera c2((cursor_fin - cursor_ini) * bytes_por_numero, header->samplerate, header->bitspersample, header->numchannels);

    if (header == NULL || data == NULL)
        cout << "Why in the world are you trying to save an empty song??" << endl;

    f.open(nom.c_str(), ios::binary);

    if (!f) {
        cerr << "ERROR: guardar: no se pudo abrir " << nom << endl;
        return false;
    }

    f.write((char *) &c2, sizeof(Cabecera));
    f.write(&((char *) data)[cursor_ini * bytes_por_numero], c2.subchunk2size);

    f.close();

    return true;
}


////////////////////////////////////////// CREAR
void WavSong::CreateWAV(double seconds, unsigned int samplesPerSeconds, unsigned int bitsPerSample, unsigned int numChannels) {
    if (seconds < 0) {
        LOG_WARN("asked for negative seconds (seconds). nothing is created.")
//        cout << "AVISO: WavSong::CreateWAV: se piden duracion negativa. no se crea nada.\n";
        return;
    }

    if (bitsPerSample != 8 && bitsPerSample != 16 && bitsPerSample != 32) {
        LOG_WARN("bits per sample is not 8 nor 16 nor 32. nothing is created. a good value is 32")
//        cout << "AVISO: WavSong::CreateWAV: los bits por muestra no son ni 8 ni 16 ni 32. no se crea nada.\n";
        return;
    }

    if (numChannels != 1 && numChannels != 2) {
        LOG_WARN("asked for bad number of channels. nothing is created. a good value is 1")
//        cout << "AVISO: WavSong::CreateWAV: se pide un numero de canales negativo. no se crea nada.\n";
        return;
    }

    Empty();

    header = new WavHeader(int(ceil(seconds * samplesPerSeconds)) * bitsPerSample * numChannels / 8
            , samplesPerSeconds, bitsPerSample, numChannels);

    data = calloc(header->subchunk2size, 1);    // ask for subchunk2size bytes zeroed

    cursor_ini = 0;
    cursor_fin = header->subchunk2size / (header->bitspersample / 8);
}

/*
void WavSong::CrearOndas(Sint16 width, Sint16 height)
{
    if (ondas != NULL)
    {
        //cout << "sobreescribiendo dibujo de ondas" << endl;
        SDL_FreeSurface(ondas);
        ondas = NULL;
    }

    ondas = SDL_CreateRGBSurface(SDL_SWSURFACE, width, height, 32, 0xff0000, 0xff00, 0xff, 0);//xff000000);
    if (ondas == NULL)
        cerr << "ERROR: WavSong::CreaOndas: no se pudo crear surface\n";

    w = width;
    h = height;

    UpdateOndas();
}
*/


/**
  dest += orig;
  Tiene en cuenta un coeficiente de volume y un desplazamiento.
  La variable despl se modifica para no tener en cuenta los canales y tratarlos como 
  si fueran muestras independientes, lo cual hace el calculo más simple.
  Permite canciones en 8, 16 o 32 bits.
  El recorrido en el bucle es el siguiente:
  si orig desplazado cabe en dest
    si despl es positivo:
        for  0 ..  nmuestras1
            despl .. nmuestras1+despl = 0 .. nmuestras1
    si despl es negativo:
        for -despl .. nmuestras1
            0 .. nmuestras1+despl = -despl .. nmuestras1
  si no cabe
    si despl es positivo:
        for  0 ..  nmuestras2-despl
            despl .. nmuestras2 = 0 .. nmuestras2-despl
    si despl es negativo:
        for -despl .. nmuestras2-despl
            0 .. nmuestras2 = -despl .. nmuestras2-despl
  @param orig puntero a cancion con los data a escribir
  @param dest puntero a cancion donde hay que escribir
  @param despl numero de muestras a desplazar (desplazar +samplerate = desplazar 1 segundo a la derecha)
  @param vol coeficiente de volume (1 lo deja igual)
  */
bool WavSong::Sumar (

        const WavSong * orig)
{
    bool ok = false;


    unsigned int i;//, inicio, fin;
    //int nmuestras1 = orig->cab->subchunk2size/(orig->cab->bitspersample/8);	// para simplificar el calculo
    //int nmuestras2 = dest->cab->subchunk2size/(dest->cab->bitspersample/8);	// 1 muestra con 2 canales cuenta como 2 muestras
    int nmuestras1 = orig->

            cursor_fin - orig->cursor_ini;
    int nmuestras2 = cursor_fin -

                     cursor_ini;
    //int desplazamiento = orig->despl * orig->cab->numchannels;

    if (orig->header->bitspersample != header
            ->bitspersample)
        cerr <<
        "ERROR: sumar: no coinciden los bits por muestra\n";
    else if (orig->header->numchannels
             != header->
            numchannels)
        cerr <<
        "ERROR: sumar: no coincide el numero de canales\n";
    else
    {
        if (
                nmuestras1 > nmuestras2)
        {
            cout <<


            "AVISO: sumar: la cancion no cabe, se recortará\n";nmuestras2 = nmuestras1;
            cursor_fin =cursor_ini + nmuestras2;
        }

        ok = true;


        int* orig32 = (int*)
                orig->
                        data;
        int* dest32 = (int*) data;

        switch (orig->header->bitspersample)
        {
            case 8:
                //for (i = inicio; i < fin; i++)
                //*((char*)dest->data + i+despl) += (*((char*)orig->data + i)) * vol;
                break;
            case 16:
                //for (i = inicio; i < fin; i++)
                //*((short int*)dest->data + i+despl) += (*((short int*)orig->data + i)) * vol;
                break;
            case 32:
                for (i = cursor_ini; i < cursor_fin; i++)
                    dest32
                    [i] +=
                            orig32[i] * orig->volume;
                break;
            default:
                cerr <<
                "ERROR: sumar: las muestras no son de 8, ni 16 ni 32 bits\n";
                ok = false;
        }
    }


/* hay que definir antes lo del tiempo compartido


    int i, inicio, fin;
    //int nmuestras1 = orig->cab->subchunk2size/(orig->cab->bitspersample/8);	// para simplificar el calculo
    //int nmuestras2 = dest->cab->subchunk2size/(dest->cab->bitspersample/8);	// 1 muestra con 2 canales cuenta como 2 muestras
    int nmuestas1 = orig->cursor_fin - orig->cursor_ini;
    int nmuestas2 = cursor_fin - cursor_ini;

    int desplazamiento = orig->despl * orig->cab->numchannels;

    if (orig->cab->bitspersample != dest->cab->bitspersample)
        cerr << "ERROR: sumar: no coinciden los bits por muestra\n";
    else if (orig->cab->numchannels != dest->cab->numchannels)
        cerr << "ERROR: sumar: no coincide el numero de canales\n";
    else
    {
        if (nmuestras1 + desplazamiento > nmuestras2)
            cerr << "AVISO: sumar: la cancion no cabe, se recortará\n";

        ok = true;
        inicio = despl<0 ? -despl : 0;	// max (-despl, 0)
        fin = nmuestras1<nmuestras2-despl ? nmuestras1 : nmuestras2-despl;	// min (a, b)

        switch (orig->cab->bitspersample)
        {
            case 8:
                for (i = inicio; i < fin; i++)
                    *((char*)dest->data + i+despl) += (*((char*)orig->data + i)) * vol;
                break;
            case 16:
                for (i = inicio; i < fin; i++)
                    *((short int*)dest->data + i+despl) += (*((short int*)orig->data + i)) * vol;
                break;
            case 32:
                for (i = inicio; i < fin; i++)
                    *((int*)dest->data + i+despl) += (*((int*)orig->data + i)) * vol;
                break;
            default:
                cerr << "ERROR: sumar: las muestras no son de 8, ni 16 ni 32 bits\n";
                ok = false;
        }
    }

    // */return ok;
}




void WavSong::Reproducir ()
{
    /*
    if (can->cab->bitspersample == 32)
    {
        convertir (can, 16, &c2);
        liberar = true;
    }
    else
    {
        if (can->cab->bitspersample != 16)
        {
            cerr << "ERROR: reproducir: no se puede reproducir\n";
            return;
        }
        c2 = can;
        liberar = false;
    }

    //mostrarCabecera(c2->cab, 2);	//c2 debe estar con una cancion de 16 bits

    deseado = new SDL_AudioSpec;
    obtenido = new SDL_AudioSpec;

    deseado->freq = cab->samplerate;
    deseado->format = AUDIO_S32SYS;
    deseado->channels = cab->numchannels;
    deseado->samples = 4096;
    deseado->callback = retrollamada;
    deseado->userdata = this;


    if ( SDL_OpenAudio(deseado, obtenido) < 0 ) {
        fprintf(stderr, "Unable to open audio: %s\n", SDL_GetError());
        delete deseado;
        delete obtenido;
        return;
    }

    SDL_PauseAudio(0);
    */
}
////////////////////////////////////////// AUXILIARES

bool WavSong::Usada ()
{
    return header !=NULL;
}

string WavSong::headerToString(int degree) {
    stringstream output;
    if (header == nullptr) {
        output << "\nthere is no header";
    } else {
        switch (degree) {
            case 3:
                output << "\nc->chunkid = " << header->chunkid;
                output << "\nc->chunksize = " << header->chunksize;
                output << "\nc->format = " << header->format;
                output << "\nc->subchunk1id = " << header->subchunk1id;
                output << "\nc->subchunk1size = " << header->subchunk1size;
                output << "\nc->audioformat = " << header->audioformat;
                output << "\nc->subchunk2id = " << header->subchunk2id;
            case 2:
                output << "\nc->numchannels = " << header->numchannels;
                output << "\nc->byterate = " << header->byterate;
                output << "\nc->blockalign = " << header->blockalign;
                output << "\nc->subchunk2size = " << header->subchunk2size;
            case 1:
                output << "\nc->samplerate = " << header->samplerate;
                output << "\nc->bitspersample = " << header->bitspersample;
                output << "\ninferred seconds = " << getSeconds();
                break;
            default:
                break;
        }
    }
    output << "\n";
    return output.str();
}

double WavSong::getSeconds() const {
    return header->subchunk2size/(double)(header->blockalign * header->samplerate);
}

unsigned int WavSong::getSamples() const {
    return header->subchunk2size/header->blockalign;
}


void WavSong::UpdateOndas()
{
    /*
    Sint32 * cursor;
    Uint32 max = 0x7fffffff;	// 2^31 -1
    int volporpix;
    int muestrasporpix;
    Sint16 x, y;
    int nmuestras;
    int i;

    if (ondas == NULL || cab == NULL || data == NULL || h == 0 || w == 0)
        return;

    //nmuestras = cab->subchunk2size/cab->blockalign;
    volporpix = max/(h/2);
    nmuestras = cursor_fin - cursor_ini;
    muestrasporpix = nmuestras/w + (nmuestras%w == 0? 0: 1);	// funcion techo

    cout << "muestrasporpix = " << muestrasporpix << endl;	// DEPURACION
    cout << "volporpix = " << volporpix << endl;	// DEPURACION


    SDL_LockSurface(ondas);

    SDL_FillRect(ondas, NULL, 0x808080);
    for (cursor = &((Sint32*)data)[cursor_ini], i = 0; i < nmuestras; cursor++, i++)
    {
        y = h/2 - *cursor/volporpix;
        x = i/muestrasporpix;
        ((Uint32*)ondas->pixels)[x + y*w] = 0x0;	//negro
        if (x > w)
        {
            cout << "se sale" << endl;
            cout << "x = " << x << endl;	// DEPURACION
            cout << "i = " << i << endl;	// DEPURACION
            cout << "muestrasporpix = " << muestrasporpix << endl;	// DEPURACION
            cout << "w = " << w << endl;	// DEPURACION
        }
    }
    SDL_UnlockSurface(ondas);
    */
}

void WavSong::setVolumen(float vol)
{
    volume = vol;
    if (vol < 0)
        cout <<

        "AVISO: WavSong::setVolumen: volume negativo: lo hago, pero, seguro que quieres invertir la onda verticalmente?" << endl;
}

/*
void WavSong::setTramo(Sint16 ini, Sint16 fin, Sint16 max)
{
int muestras = cursor_fin - cursor_ini;
cout << "muestras = " << muestras << endl;	// DEPURACION
//int muestras_por_pixel = muestras / max;
//cout << "muestras_por_pixel = " << muestras_por_pixel << endl;	// DEPURACION

if (ini < 0 || ini > max)
ini = 0;

if (fin > max)
fin = max;

cout << "ini = " << ini << endl;	// DEPURACION
cout << "fin = " << fin << endl;	// DEPURACION
cursor_fin = cursor_ini + fin * muestras / max;
cursor_ini += ini * muestras / max;
cout << "cursor_ini = " << cursor_ini << endl;	// DEPURACION
cout << "cursor_fin = " << cursor_fin << endl;	// DEPURACION
}*/

void WavSong::unsetTramo()
{
    cursor_ini = 0;
    if (header !=
        NULL)
        cursor_fin = header->subchunk2size / (header->bitspersample
                                              /8);
    UpdateOndas();
}

void WavSong::setDesplazamiento(int desplaz)
{
    despl = desplaz;
}

/**
* TODO: test bounds of arrays are not reached
* @param instant in seconds
* @return seconds written
*/
double WavSong::writeNote(const ComplexNote &note) {
    unsigned int samples = getSampleCursor(note.seconds);
    unsigned int totalSamples = getSamples();
    unsigned int sampleCursor = getSampleCursor(this->cursor);
    if (sampleCursor >= totalSamples) {
        std::stringstream ss;
        ss << "sampleCursor should be less than totalSamples: ";
        ss << sampleCursor << " < " << totalSamples << " failed.";
        throw randomize::utils::exception::StackTracedException(ss.str());
    }
    if (sampleCursor + samples > totalSamples) {
        LOG_ERROR("clipping note to avoid writing outside buffer space, wrote %d samples, have to "
                "write %d, and max is %d", sampleCursor, samples, totalSamples);
        samples = totalSamples - sampleCursor;
    }
    double secondsPerSample = 1.0 / header->samplerate;
    double timeCursor = 0;
    int intVolume = calculateVolume(volume);
    LOG_VERBOSE("int volume = %d", intVolume)

    int *buffer = &((int*) data)[sampleCursor];    // TODO prepare to 16 bits integers
    int max = 0;
    unsigned int i;
    for (i = 0; i < samples; ++i, timeCursor += secondsPerSample) {
        double noteValue = note.value(timeCursor);
        int value = int(floor(noteValue * intVolume));
        buffer[i] += value;
        max = std::max(buffer[i], max);
    }//    return tempo * samples / cab->samplerate;
    this->cursor += timeCursor;
    LOG_VERBOSE("written until %f", this->cursor)
    LOG_VERBOSE("max amplitude was %d", max)
    LOG_VERBOSE("written %d samples", i)
    return timeCursor;
}

unsigned int WavSong::getSampleCursor(const double seconds) {
    return (unsigned int) round(header->samplerate * seconds);
}

void WavSong::print(ostream & o, double start, double end) {
    double endBound = getSeconds();
    if (end < 0 || end > endBound) {
        end = endBound;
    }
    unsigned int sampleStart = getSampleCursor(start);
    unsigned int sampleEnd = getSampleCursor(end);
    if (sampleEnd < sampleStart) {
        logic_error error("start point must be less than end point");
        throw error;
    }
    unsigned int samples = sampleEnd - sampleStart;

    int *buffer = &((int *) data)[sampleStart];    // TODO prepare to 16 bits integers
    for (unsigned int i = 0; i < samples; ++i) {
        o << buffer[i] << endl;
    }
    o << endl;
}

/**
* computes the correct max int value for a given coefficient (volume) and bits per sample.
*/
int WavSong::calculateVolume(double coefficient) {
    if (header->bitspersample == 32)
        return int(2147000000*coefficient);  // (~= 2^31) es la amplitud maxima escribible en 32 bits

    if (header->bitspersample == 16)
        return int(32700*coefficient);       // (2^ - 1) es la amplitud maxima escribible en 16 bits

    if (header->bitspersample == 8)
        return int(255*coefficient);        // (2^31 - 1) es la amplitud maxima escribible en 8 bits (sin signo)

    LOG_ERROR("song is not of 8 nor 16 nor 32 bits.")
    return 0;
}

/**
* set position cursor (units are seconds).
* the cursor will be used in operations such as writing notes.
*/
bool WavSong::setCursor(double cursor) {
    if (cursor < 0 || cursor > this->song.seconds) {
        return false;
    }
    LOG_DEBUG("setting cursor from %f to %f", this->cursor, cursor);
    this->cursor = cursor;
    return true;
}

double WavSong::getCursor() const {
    return cursor;
}

/**
 * possible improvements:
 * - range specification
 *      - operator[] and size()
 *      - Iterator subclass
 */
//void WavSong::forEachSample(std::function<void(float sample)> userFunction) const {
//
//    if (header->bitspersample == 32) {
//        for (unsigned int i = 0; i < getSamples(); ++i) {
//            int sample32bits = ((int *) data)[i];
//            double sample = (double) sample32bits / (double) 0xFFFFFFFF; // or FFFFFFFF ?
//            if (sample > 1) {
//                LOG_DEBUG("quotient should be over 0xFFFFFFFF");
//            }
//            userFunction(sample);
//        }
//    } else {
//        throw randomize::utils::exception::StackTracedException(
//                "unimplemented: foreach with 32 bits per sample");
//    }
//}


void WavSong::forEachSample(std::function<void(float sample)> userFunction) const {
    forEachSample(0, getSamples(), userFunction);
}

void WavSong::forEachSample(std::size_t start, std::size_t end,
        std::function<void(float sample)> userFunction) const {

    forEachSampleIndexed(start, end, [&](size_t, float sample) {
        userFunction(sample);
    });
}
void WavSong::forEachSampleIndexed(std::size_t start, std::size_t end,
        std::function<void(size_t index, float sample)> userFunction) const {

    if (start >= 0 && end <= getSamples()) {
        if (header->bitspersample == 32) {
            for (std::size_t i = start; i < end; ++i) {
                int sample32bits = ((int *) data)[i*header->numchannels];
                double sample = (double) sample32bits / (double) 0xFFFFFFFF; // or FFFFFFFF ?
                if (sample > 1) {
                    LOG_DEBUG("quotient should be over 0xFFFFFFFF");
                }
                userFunction(i, sample);
            }
        } else if (header->bitspersample == 16) {
            for (std::size_t i = start; i < end; ++i) {
                unsigned short int sample16bits = ((unsigned short int *) data)[i*header->numchannels];
                double sample = (double) sample16bits / (double) 0xFFFF; // or FFFFFFFF ?
                if (sample > 1) {
                    LOG_DEBUG("quotient should be over 0xFFFF");
                }
                userFunction(i, sample);
            }
        } else {
            throw randomize::utils::exception::StackTracedException(
                    "unimplemented: foreach with other than 32 or 16 bits per sample: "
            + to_string(header->bitspersample));
        }
    } else {
        std::stringstream ss;
        ss << "invalid range supplied: [" << start << ", " << end << "). ";
        ss << "valid range is [0, " << getSamples() << ")";
        throw randomize::utils::exception::StackTracedException(ss.str());
    }
}



const WavHeader &WavSong::getHeader() const {
    return *header;
}



};  // gemu
};  // randomize

