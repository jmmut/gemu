/**
 * @file SoundStream.h
 * @author jmmut 
 * @date 2015-09-12.
 */

#ifndef GEMU_SOUNDSTREAM_H
#define GEMU_SOUNDSTREAM_H

#include <queue>
#include <mutex>
#include "SDL.h"
#include <log/log.h>
//#include <atomic>
#include <stdexcept>
#include <condition_variable>

namespace randomize {
namespace sound {

/**
 * use like this:

SoundStream ss;
ss.setFrequency(samplesPerSecond);
while (true) {
    ss << f(t);
    t++;
    if (t > 100000) {   // comment for a true endless song
        break;
    }
}

 */
class SoundStream {
public:
    SoundStream();
    ~SoundStream();

    /**
     * main way to write data. The other ones are just wrappers.
     * @param value float in range of [-1, 1]
     */
    SoundStream &operator<<(float value);
    SoundStream &operator<<(double value);
    SoundStream &operator<<(int value);
    SoundStream &operator<<(unsigned char value);

    void setFrequency(int freq);
    int getFrequency();
    const static int MAX_ELEMS = 50000;

private:
    void start(SDL_AudioSpec audioSpec);
    void callback(Uint8* stream, int len);

    std::queue<float> buffer;
    mutable std::mutex m_mutex;
    std::condition_variable cond_var;
    SDL_AudioSpec have;
    SDL_AudioDeviceID dev;
    bool started;
};

};  // sound
};  // randomize


#endif //GEMU_SOUNDSTREAM_H
