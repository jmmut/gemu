/**
 * @file TextSongWriter.cpp
 * @author jmmut 
 * @date 2015-10-11.
 */

#include "core/Song.h"
#include "TextSongWriter.h"

namespace randomize {
namespace gemu {


void TextSongWriter::write(randomize::gemu::Song song) {
    std::ostream *o;
    std::ofstream songString;

    if (useOstream) {
        o = &out;
    } else {
        songString.open(name);
        if (!songString) {
            throw std::runtime_error("could not open file " + name);
        }
        o = &songString;
    }

    switch (type) {
        case BASIC:
            writeBasic(song, *o);
            break;
        case VERBOSE:
            writeVerbose(song, *o);
            break;
        case MEZ:
            writeMez(song, *o);
            break;
        case JSON:
            writeJSON(song, *o);
            break;
    }
}

void TextSongWriter::writeBasic(randomize::gemu::Song song, std::ostream &songString) {
    songString << song.toString();
}

void TextSongWriter::writeVerbose(randomize::gemu::Song song, std::ostream &songString) {
    songString << song.toVerboseString();
}

void TextSongWriter::writeMez(randomize::gemu::Song song, std::ostream &songString) {

    songString << "n 0 22050 32 " << song.seconds << " p ";
    for (const Track &track : song.tracks) {
        songString << " a 0 " << song.tempo << " 0.25 0 " << (track.size() > 0 ? track[0]->DEFAULT_TIMBRE : 127);
        for (const auto &note : track) {
//            song += string(" a 0 60 0.25 0 " + toString(note.timbre));
            if (note->getNote() < 0) {
                songString << "\ns " << toPulses(note->seconds, song.tempo);
            } else {
                songString << "\ne " << note->getNote() << " " << toPulses(note->seconds, song.tempo);
            }
        }
        songString << " p 0";
    }
    songString << " x 2 " << song.name << " 0 x";
}

void TextSongWriter::writeJSON(randomize::gemu::Song song, std::ostream &songString) {

    songString << "{\n  \"name\":\"" << song.name << "\",\n"
        << "  \"tempo\":" << song.tempo << ",\n"
        << "  \"seconds\":" << song.seconds << ",\n"
        << "  \"tracks\":[";

    bool notTheFirst = false;
    for (const Track &track : song.tracks) {
        if (notTheFirst) {
            songString << ",";
        }
        notTheFirst = true;
        songString << "\n    \"" << track.toString(song.tempo) << "\"";
    }

    songString << "]\n}\n";
}


};  // gemu
};  // randomize

