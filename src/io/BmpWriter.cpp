/**
 * @file BmpWriter.cpp
 * @author jmmut 
 * @date 2016-06-15.
 */

#include "utils/exception/StackTracedException.h"
#include "BmpWriter.hpp"


namespace randomize {

BmpWriter::BmpWriter(std::string filename, Uint32 actualWidth, Uint32 actualHeight,
        Uint32 logicalWidth, Uint32 logicalHeight)
        : filename(filename), actualWidth(actualWidth), actualHeight(actualHeight),
        logicalWidth(logicalWidth), logicalHeight(logicalHeight),
        cursor(0) {

    if (!SDL_WasInit(SDL_INIT_VIDEO)) {
        if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
            LOG_ERROR("Unable to initialize SDL: %s\n", SDL_GetError());
            throw std::runtime_error("Unable to initialize SDL: " + std::string(SDL_GetError()));
        }
    } else {
        LOG_WARN("SDL video was already initialized. If you encounter errors, maybe BmpWriter"
                " should not close video subsystem");
    }

    surface = SDL_CreateRGBSurface(0, actualWidth, actualHeight, 32, 0, 0, 0, 0);
    map = new Uint32[logicalWidth * logicalHeight];
    memset(map, 0, logicalWidth * logicalHeight);
}

void BmpWriter::write(Uint8 red, Uint8 green, Uint8 blue) {
    Uint32 width_i, height_i;
    width_i = cursor % logicalWidth;
    height_i = cursor / logicalWidth;

    if (width_i >= logicalWidth || height_i >= logicalHeight) {
        std::stringstream ss;
        ss << "tried to write out of range: (" << width_i << ", " << height_i << ") of a total of ("
                << logicalWidth << ", " << logicalHeight << ")";
        throw utils::exception::StackTracedException(ss.str());
    }
    map[cursor] = SDL_MapRGB(surface->format, red, green, blue);
    cursor++;
}


void BmpWriter::flush() {
    Uint32 pos, w, h;
    for (w = 0; w < actualWidth; w++) {
        for (h = 0; h < actualHeight; h++) {
            pos = w * logicalWidth / actualWidth + h * logicalHeight / actualHeight * logicalWidth;
            *(Uint32 *) (((Uint8 *) surface->pixels) + w * 4 + h * surface->pitch) = map[pos];
        }
    }
    SDL_SaveBMP(surface, filename.c_str());
}

BmpWriter::~BmpWriter() {
    try {
        flush();
    } catch (std::exception e) {
        std::cerr << "an exception was thrown in ~BmpWriter: " << e.what() << std::endl;
    } catch (std::exception *e) {
        std::cerr << "an exception was thrown in ~BmpWriter: " << e->what() << std::endl;
    } catch (...) {
        std::cerr << "an unknown exception was thrown in ~BmpWriter" << std::endl;
    }
}

};  // randomize

