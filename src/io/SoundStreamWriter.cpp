/**
 * @file SoundStreamExporter.cpp
 * @author jmmut 
 * @date 2015-09-13.
 */

#include "SoundStreamWriter.h"

namespace randomize {
namespace gemu {

SoundStreamWriter::SoundStreamWriter(int samplesPerSecond) : samplesPerSecond(samplesPerSecond) {

}

void SoundStreamWriter::write(randomize::gemu::Song song) {
    sound::SoundStream ss;
    ss.setFrequency(samplesPerSecond);
    for (Song::Iterator chordIt = song.begin(); chordIt != song.end(); ++chordIt) {
        double remain;
        Chord chord = *chordIt;
        if (chordIt.getMinRemainingNoteTime(remain)) {
            int samples = int(ss.getFrequency() * remain);    // samples = (samples/seconds) * (seconds)
            for (int sample = 0; sample < samples; sample++) {
                double sum = 0;
                int track = 0;
                double dt = double(sample)/samplesPerSecond;    // seconds = (samples/seconds) * (1/samples)

                for (const std::shared_ptr<ComplexNote> &note : *chordIt) {
//                for (double instant : chordIt.getInstants()) {
                    double cursor = chordIt.getInstants()[track] + dt;
                    sum += note->value(cursor);
                    track++;
                }
                ss << sum/ (*chordIt).size();
            }
        }
    }
}

};  // gemu
};  // randomize


