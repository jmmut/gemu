#ifndef _CANCION_H_
#define _CANCION_H_

/**
* clase para manejar audio en WAV
*
* limitaciones (a ir revisando):
*     monocanal
*     32 bits en UpdateOndas
*/

#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include "core/Song.h"
#include "log/log.h"
//#include <SDL.h>


namespace randomize {
namespace gemu {

struct WavHeader {
    unsigned int chunkid:32;
    unsigned int chunksize:32;  /// 36 + subchunk2size (data block)
    unsigned int format:32;

    unsigned int subchunk1id:32;
    unsigned int subchunk1size:32;
    unsigned int audioformat:16;
    unsigned int numchannels:16;
    unsigned int samplerate:32; /// 22050, 44100, etc: sample frequency : samples/s
    unsigned int byterate:32;   /// SampleRate * NumChannels * BitsPerSample/8 = B/s
    unsigned int blockalign:16; /// NumChannels * BitsPerSample/8: B/sampleBlock
    unsigned int bitspersample:16;

    unsigned int subchunk2id:32;
    unsigned int subchunk2size:32;  /// size of the next data block in bytes.

    WavHeader() {
        Init();
    }

    void Init() {
        chunkid = 0x46464952;   // "RIFF"
        format = 0x45564157;    // "WAVE"

        subchunk1id = 0x20746d66;   // "fmt "
        subchunk1size = 16; // for PCM 16 (field is 2 bytes)
        audioformat = 1;    // no compression

        subchunk2id = 0x61746164;   // "data"
    };

    WavHeader(unsigned int sc2size, unsigned int srate, unsigned int bpsample, unsigned int nchannels) {
        Init();
        chunksize = 36 + sc2size;
        numchannels = nchannels;
        samplerate = srate;
        byterate = nchannels * bpsample * srate / 8;
        blockalign = nchannels * bpsample / 8;
        bitspersample = bpsample;
        subchunk2size = sc2size;
    }
};

typedef WavHeader Cabecera;

/*
struct Cabecera
{
	unsigned int chunkid:32;
	unsigned int chunksize:32;	// 36 + subchunk2size (bloque de data)
	unsigned int format:32;

	unsigned int subchunk1id:32;
	unsigned int subchunk1size:32;
	unsigned int audioformat:16;
	unsigned int numchannels:16;
	unsigned int samplerate:32;	// 8000, 44100, etc: frec. muestreo
	unsigned int byterate:32;	//SampleRate * NumChannels * BitsPerSample/8 = B/s
	unsigned int blockalign:16;	//NumChannels * BitsPerSample/8
	unsigned int bitspersample:16; 

	unsigned int subchunk2id:32;
	unsigned int subchunk2size:32;	// tamaño del bloque de data siguiente en bytes
	
	Cabecera()	// rellenar los valores que siempre seran validos en este programa
	{
		Init();
	};
	void Init()
	{
		chunkid = 0x46464952;	// "RIFF"
		format = 0x45564157;	// "WAVE"

		subchunk1id = 0x20746d66;	// "fmt "
		subchunk1size = 16;	//para PCM 16 (el campo es de 2 bytes)
		audioformat = 1;	//sin compresion

		subchunk2id = 0x61746164;	// "data"
	};
	
	Cabecera (unsigned int sc2size, unsigned int srate, unsigned int bpsample, unsigned int nchannels)
	{
		Init();
		chunksize = 36 + sc2size;
		numchannels = nchannels;
		samplerate = srate;
		byterate = nchannels * bpsample * srate / 8;
		blockalign = nchannels * bpsample/8;
		bitspersample = bpsample;
		subchunk2size = sc2size;
	}
};
*/


/**
* To export a Song
*/
class WavSong {
private:
    unsigned int cursor_ini, cursor_fin;
    int virtual_cur_ini, virtual_cur_fin;
    int despl;

    Cabecera *header;
    void *data;
    Song song;
    double volume;      // not in Song
    double cursor;    // in seconds, e.g. for chained insertions

//		SDL_AudioSpec* deseado;
//		SDL_AudioSpec* obtenido;

public:

//		SDL_Surface *ondas;
//		Uint16 w, h;

private:
    void Init();

public:
    static const int DEFAULT_SAMPLES_PER_SECOND = 22050;
    static const int DEFAULT_BITS_PER_SAMPLE = 32;

    WavSong();    // solo crea punteros vacios
//    WavSong(float segundos, int muestras_por_segundo = 22050, int bits_por_muestra = 32, int num_canales = 1);

    WavSong(const Song &song, double volume = 1, unsigned int samplesPerSecond = DEFAULT_SAMPLES_PER_SECOND
            , unsigned int bitsPerSample = DEFAULT_BITS_PER_SAMPLE, unsigned int numChannels = 1);

    ~WavSong();

    void Empty();

    double writeNote(const ComplexNote &note);

    //ENTRADA/SALIDA
    bool CargarWAV(std::string nom_fich);

    bool Guardar(std::string nom_fich);

    //CREAR
    void CreateWAV(double segundos, unsigned int samplesPerSeconds = DEFAULT_SAMPLES_PER_SECOND
            , unsigned int bitsPerSample = DEFAULT_BITS_PER_SAMPLE, unsigned int numChannels = 1);
//		void CrearOndas (Sint16 w, Sint16 h);

    bool Sumar(const WavSong *orig);

    void Reproducir();

//		static void retrollamada (void *userdata, Uint8 *stream, int len);
    //AUXILIARES
    bool Usada();    //si esta usada o esta vacia

    /**
    * writes a string with data of the header of the song.
    *
    * @param degree [0-3] configurable (from zero to high) verbosity.
    */
    std::string headerToString(int degree);

    void UpdateOndas();

    void setVolumen(float volumen);

//		void setTramo(Sint16 ini, Sint16 fin, Sint16 max);
    void unsetTramo();

    void setDesplazamiento(int desplaz);

    int calculateVolume(double coefficient);
    bool setCursor(double cursor);
    double getCursor() const;
    const WavHeader &getHeader() const;

    double getSeconds() const;
    unsigned int getSamples() const;

    void print(std::ostream & o, double start = 0, double end = -1);
    void forEachSampleIndexed(std::size_t start, std::size_t end, std::function<void(size_t index, float sample)> userFunction) const;
    void forEachSample(std::function<void(float sample)> userFunction) const;
    /**
     * TODO: this is monochannel
     */
    void forEachSample(std::size_t start, std::size_t end, std::function<void(float)> userFunction) const;

    unsigned int getSampleCursor(double const seconds);

};

};  // gemu
};  // randomize


#endif /*_CANCION_H_*/
