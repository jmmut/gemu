/**
 * @file SongReader.cpp
 * @author jmmut 
 * @date 2016-03-23.
 */

#include "SongReader.h"

using randomize::utils::exception::StackTracedException;
using std::function;

namespace randomize {
namespace gemu{


void SongReader::setMetadata(std::string name, float tempo, double seconds) {
    this->name = name;
    this->tempo = tempo;
    this->seconds = seconds;
}

Song SongReader::read(std::istream &is) {
//    std::string message = "file bad formatted: ";
    std::string variable;
    Song song;

    readObject(is, [&](std::istream &is){
        readAttributeInto(is, "name", [&](std::istream & is) {
            name = readString(is);
            song.name = name;
        });
        readChar(is, COMMA);
        readAttributeInto(is, "tempo", [&](std::istream & is) {
            is >> tempo;
            song.tempo = tempo;
        });
        readChar(is, COMMA);
        readAttributeInto(is, "seconds", [&](std::istream & is) {
            is >> seconds;
            song.seconds = seconds;
        });
        readChar(is, COMMA);
        readAttributeInto(is, "tracks", [&](std::istream & is) {
            readArray(is, [&](std::istream &is) {
                std::string track = readString(is);
                song.tracks.push_back(parseTrack(track));
            });
        });
    });

    return song;
}

std::string SongReader::readString(std::istream &is) {
    std::string value;
    readChar(is, QUOTE);
    value = readUntil(is, QUOTE);
    value.pop_back();    // remove end quote of the string value
    return value;
}

void SongReader::readArray(std::istream &is, function<void(std::istream &is)> reader) {
    readChar(is, BRACKET_OPEN);
    char nextChar;
    is >> nextChar; // this may be a ']' if the array is empty, or a '"' which is the start of the attribute
    if (nextChar != BRACKET_CLOSE) {
        is.unget();
        while (nextChar != BRACKET_CLOSE) {
            reader(is);
            is >> nextChar; // this may be a ',' or a ']'
        }
    }

//    readChar(is, BRACKET_CLOSE);
}

void SongReader::readObject(std::istream & is, function<void(std::istream & is)> reader) {
    readChar(is, BRACES_OPEN);
    reader(is);
    readChar(is, BRACES_CLOSE);
}

void SongReader::readAttributeInto(std::istream & is, std::string attributeName,
        function<void(std::istream & is)> reader) {
    std::string variable = readString(is);
    readUntil(is, COLON);
    if (variable == attributeName) {
        reader(is);
    } else {
        throw StackTracedException(
                std::string("syntax error: expected attribute \"") + attributeName
                + "\" on position " + std::to_string(is.tellg()) + ", found \"" + variable + "\"");
    }
}

std::string SongReader::readUntil(std::istream & is, const char &MATCH) {
    char letter;
    std::string variable;
    is.get(letter);
    while (is && letter != MATCH) {
        variable += letter;
        is.get(letter);
    }
    variable += letter;
    if (!is) {
        throw StackTracedException("reached end of file too soon");
    }
    return variable;
}

char SongReader::readAnyCharOf(std::istream &is, const std::set<char> &MATCH) {
    char read;
    is >> read;
    while (IGNORE.npos != IGNORE.find(read)) {
        is >> read;
    }
    if (MATCH.find(read) == MATCH.end()) {
        std::string message("syntax error: expected any of [");
        std::string setString = accumulate(begin(MATCH), end(MATCH), std::string(), [] (std::string s, char elem) {
            return s + " '" + elem + "',";
        });
        std::string posString = std::to_string(is.tellg());
        throw StackTracedException(message + setString + "] on position " + posString + ", found " + read);
    }
    return read;
}

char SongReader::readChar(std::istream & is, const char &MATCH) {
    return readAnyCharOf(is, {MATCH});
}

/**
 * TODO: use move reference
 * Track && SongReader::parseTrack(std::string trackString);
 */
Track SongReader::parseTrack(std::string trackString) {
    std::string note;
    float pulses;
    std::stringstream ss;
    Track track;
    ss << trackString;

    while (ss) {
        bool moreNotes = readAnyCharOf(ss, {'<', '\0'}) == '<';
        if (!moreNotes) {
            break;
        }
        note = readUntil(ss, '>');
        note.pop_back();
        ss >> pulses;

        track.emplace_back(new SimpleNote(toSeconds(pulses, tempo), toNote(note), volume, timbre));
    }

    return track;
}

SongReader &SongReader::setVolume(unsigned int volume) {
    this->volume = volume;
    return *this;
}

SongReader &SongReader::setTimbre(unsigned int timbre) {
    this->timbre = timbre;
    return *this;
}

std::istream &operator>>(std::istream &is, Song &song) {
    song = SongReader().read(is);
    return is;
}



};  // gemu
};  // randomize


/*
{
"name": asdf
"tempo": 120
"seconds": 20
"tracks": [{
 "...",
 "..."
}]
}


*/

