/**
 * @file SongReader.h
 * @author jmmut 
 * @date 2016-03-23.
 */

#ifndef GEMU_SONGREADER_H
#define GEMU_SONGREADER_H

#include <fstream>
#include <functional>
#include <set>
#include "utils/exception/StackTracedException.h"
#include "core/Song.h"
#include <core/SimpleNote.h>

namespace randomize {
namespace gemu {

/**
 * use like this:

string filename("name");
MusicGenerator music;
ifstream f;
f.open(filename.c_str());
if (!f) {
    LOG_ERROR("file %s not found", filename.c_str())
} else {
    try {
        f >> music.getSong();
        filename = music.getSong().name;
    } catch (invalid_argument e) {
        throw std::invalid_argument(string("file ") + filename + " has invalid syntax: " + e.what());
    }
}

 */
class SongReader {

public:
    void setMetadata(std::string name, float tempo, double seconds);
    SongReader& setTimbre(unsigned int timbre);
    SongReader& setVolume(unsigned int volume);
    Song read(std::istream &is);
    friend std::istream& operator>>(std::istream &is, Song &song);

private:
//    Song song;
    std::string name;
    float tempo;
    double seconds;
    float volume = 1;
    unsigned int timbre = 1023;

    const std::string IGNORE = " \n\t";
    const char BRACES_OPEN = '{', BRACES_CLOSE = '}', BRACKET_OPEN = '[', BRACKET_CLOSE = ']',
            QUOTE = '"', COLON = ':', COMMA = ',';

    char readChar(std::istream & is, const char &MATCH);
    char readAnyCharOf(std::istream &is, const std::set<char> &MATCH);
    std::string readUntil(std::istream & is, const char &MATCH);

    void readArray(std::istream &istream, std::function<void(std::istream &)> reader);
    void readObject(std::istream & is, std::function<void(std::istream & is)> reader);
    void readAttributeInto(std::istream & is, std::string attributeName,
            std::function<void(std::istream & is)> reader);
    std::string readString(std::istream &is);

    Track parseTrack(std::string track);
};

std::istream& operator>>(std::istream &is, Song &song);

};  // gemu
};  // randomize


#endif //GEMU_SONGREADER_H
