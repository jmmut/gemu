/**
 * @file BmpWriter.hpp
 * @author jmmut 
 * @date 2016-06-15.
 */

#ifndef GEMU_BMPWRITER_HPP
#define GEMU_BMPWRITER_HPP

#include "SDL.h"
#include "log/log.h"


namespace randomize {


class BmpWriter {
public:
    BmpWriter(std::string filename,
            Uint32 actualWidth, Uint32 actualHeight, Uint32 logicalWitdth, Uint32 logicalHeight);
    void write(Uint8 red, Uint8 green, Uint8 blue);
    void flush();
    virtual ~BmpWriter();

private:
    std::string filename;
    Uint32 actualWidth, actualHeight, logicalWidth, logicalHeight, *map;
    Uint32 cursor;
    SDL_Surface *surface;
};

};  // randomize

#endif //GEMU_BMPWRITER_HPP
