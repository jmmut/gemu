/**
 * @file WavWriter.h
 * @author jmmut 
 * @date 2015-10-11.
 */

#ifndef GEMU_WAVWRITER_H
#define GEMU_WAVWRITER_H

#include "core/Song.h"
#include "SongWriter.h"

namespace randomize {
namespace gemu {


/**
 * Constructor: (std::string filename, int samplesPerSecond)
 */
class WavWriter : public SongWriter {
private:
    std::string name;
    double volume;
    int samplesPerSecond;
    bool debug;
    double start, end;

public:
    WavWriter(std::string filename, int samplesPerSecond);
    WavWriter& setDebug(bool printSamples, double start, double end);
    /**
     * This is the overall volume. Each track will be lowered so that the sum can not excess the parameter
     */
    WavWriter& setVolume(double volume);

    virtual void write(randomize::gemu::Song song) override;
};

};  // gemu
};  // randomize



#endif //GEMU_WAVWRITER_H
