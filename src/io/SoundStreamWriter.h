/**
 * @file SoundStreamExporter.h
 * @author jmmut 
 * @date 2015-09-13.
 */

#ifndef GEMU_SOUNDSTREAMEXPORTER_H
#define GEMU_SOUNDSTREAMEXPORTER_H

#include <cmath>
#include "core/Song.h"
#include "SongWriter.h"
#include "io/SoundStream.h"


namespace randomize {
namespace gemu {

class SoundStreamWriter : public SongWriter {
private:
    int samplesPerSecond;
public:
    SoundStreamWriter(int samplesPerSecond);
    void write(randomize::gemu::Song song) override;
};

};  // gemu
};  // randomize



#endif //GEMU_SOUNDSTREAMEXPORTER_H
