#ifndef __LilypondSong_H_
#define __LilypondSong_H_

#include <stdexcept>
#include "core/Song.h"
#include "SongWriter.h"

namespace randomize {
namespace gemu {

class LilypondWriter : public SongWriter {

private:
    std::string name;
    void writeMetadata(std::function<void(std::ofstream &stringSong)> tracksWriteFunc);
public:
    LilypondWriter(std::string filename) : name(filename) {
    }

    void write(Song song) override;
    void writeAsChords(Song song);

    static std::string toLilyName(int note);
    static int toLilyDuration(double seconds, float d);
};

//class LilypondSong {
//private:
//    Song song;
//public:
//    LilypondSong(Song song) : song(song) {
//    }
//};

};  // gemu
};  // randomize


#endif //__LilypondSong_H_
