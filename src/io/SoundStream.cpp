/**
 * @file SoundStream.cpp
 * @author jmmut 
 * @date 2015-09-12.
 */

#include <iostream>
#include "SoundStream.h"

namespace randomize {
namespace sound {

SoundStream::SoundStream() {

    if (!SDL_WasInit(SDL_INIT_AUDIO)) {
        if ( SDL_Init(SDL_INIT_AUDIO) < 0 ) {
            LOG_ERROR("Unable to initialize SDL: %s\n", SDL_GetError());
            throw std::runtime_error("Unable to initialize SDL: " + std::string(SDL_GetError()));
        }
    } else {
        LOG_WARN("SDL audio was already initialized. Take care of not using several instances of SoundStream");
    }

    started = false;
//    dev
    setFrequency(8000);

}

SoundStream::~SoundStream() {
    SDL_CloseAudioDevice(dev);
    LOG_DEBUG("SoundStream closed");
}



SoundStream &SoundStream::operator<<(double value) {
    return *this << (float)value;
}

SoundStream &SoundStream::operator<<(int value) {
    *this << (float) ((double) value / (double) 0x7FFFFFF);
    return *this;
}

SoundStream &SoundStream::operator<<(unsigned char value) {
    return *this << ((float)value / 0xFF)*2 - 0.5;
}

/**
 * This seems a bit bloated to me, but this is what the docs say:
 * http://en.cppreference.com/w/cpp/thread/condition_variable
 */
SoundStream &SoundStream::operator<<(float value) {
    std::unique_lock<std::mutex> lk(m_mutex);
    cond_var.wait(lk, [&](){return buffer.size() < MAX_ELEMS;});
    buffer.push(value);
    lk.unlock();

//    static int pushed = 0;
//    pushed++;
//    if (pushed %4096==0) {
//        LOG_DEBUG("pushed %d values, enough to do another batch", pushed);
//    }
    return *this;
}

void SoundStream::callback(Uint8 *stream, int len) {
    int written = 0;
    int bytes = sizeof(float);
    int i;
    int zeros = 0;
//    LOG_DEBUG("in callback");

    // next braces block is necessary, in order to notify only after the lock guard is destroyed
    {
        std::lock_guard<std::mutex> lock(m_mutex); // the mutex will be locked until lock is destroyed
        while (written < len) {
            if (buffer.size() > 0) {
                auto element = buffer.front();
                *((float*)&(stream[written])) = element;
                written += sizeof(element);
                buffer.pop();
            } else {
                for (i = 0; i < bytes; i++) {
                    stream[written++] = 0;
                }
                zeros += bytes;
            }
        }
    }
    cond_var.notify_one();

//    if (zeros != 0) {
//        LOG_DEBUG("in callback with not enough data, wrote %d zeros", zeros);
//    }
}


void SoundStream::setFrequency(int freq) {

    if (started) {
        SDL_CloseAudioDevice(dev);
    }

    SDL_AudioSpec want;

    SDL_zero(want);
    want.freq = freq;
    want.format = AUDIO_F32;
    want.channels = 1;
    want.samples = 4096;
    want.userdata = this;
    want.callback = [](void *userdata, Uint8 *stream, int len) {
        ((SoundStream *) userdata)->callback(stream, len);
    };

    start(want);
}

void SoundStream::start(SDL_AudioSpec want) {

    dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, 0);
    if (dev == 0) {
        LOG_ERROR("Failed to open audio: %s", SDL_GetError());
        throw std::runtime_error("Failed to open audio: " + std::string(SDL_GetError()));
    }

    // start audio playing. if the callback is called too soon, just silence will be played
    SDL_PauseAudioDevice(dev, 0);
    started = true;
}

int SoundStream::getFrequency() {
    return have.freq;
}

};  // sound
};  // randomize

