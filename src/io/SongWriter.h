/**
 * @file Exporter.h
 * @author jmmut 
 * @date 2015-09-13.
 */

#ifndef GEMU_EXPORTER_H
#define GEMU_EXPORTER_H

#include <iostream>
#include "core/Song.h"

namespace randomize {
namespace gemu {

class SongWriter {
public:
    virtual void write(randomize::gemu::Song song) = 0;
};

};  // gemu
};  // randomize


#endif //GEMU_EXPORTER_H
