
#include "LilypondWriter.h"

namespace randomize {
namespace gemu {
using namespace std;


void LilypondWriter::write(randomize::gemu::Song song) {
    writeMetadata([&](ofstream &stringSong){
        for (Track track : song.tracks) {
            stringSong << "    \\new Voice {\n      \\tempo 4=" << int(song.tempo)
            << " \\set midiInstrument = #\"flute\" \n      ";
            for (shared_ptr<ComplexNote> note: track) {
                stringSong << toLilyName(note->getNote()) << toLilyDuration(note->seconds, song.tempo) << " ";
            }
            stringSong << "\n    }\n";
        }
    });
}

void LilypondWriter::writeAsChords(Song song) {
    writeMetadata([&](ofstream &stringSong){
        stringSong << "    \\new Voice {\n      \\tempo 4=" << int(song.tempo)
        << " \\set midiInstrument = #\"flute\" \n      ";
        for (Chord chord : song) {
            if (chord.size() < 1) {
                throw randomize::utils::exception::StackTracedException(
                        "logic error: chords must have at least one note (or one silence)");
            }
            vector<int> notes;
            for (shared_ptr<ComplexNote> note: chord) {
                if (note->getNote() != -1) {
                    notes.push_back(note->getNote());
                }
            }
            if (notes.size() >= 1) {
                stringSong << "<";
                for (auto note : notes) {
                    stringSong << toLilyName(note) << " ";
                }
                stringSong << ">";
            } else {
                stringSong << "r";
            }

            stringSong << toLilyDuration(chord[0]->seconds, song.tempo) << " ";
        }
        stringSong << "\n    }\n";
    });
}

void LilypondWriter::writeMetadata(std::function<void(ofstream &stringSong)> tracksWriteFunc) {
    ofstream stringSong;
    stringSong.open(name);
    if (!stringSong) {
        LOG_ERROR("could not open file %s to write lilypond", name.c_str());
        throw runtime_error(name + " could not be opened to write lilypond");
    } else {
        stringSong << "\\version \"2.16.2\"\n\\language \"english\"\n\\score {\n  <<\n"; //"\\version \"2.18.2\"\n{\n"

        tracksWriteFunc(stringSong);
        stringSong << "\n  >>\n  \\layout { }\n";
        stringSong <<
        "  \\midi {\n"
                "    \\context {\n"
                "      \\Staff\n"
                "      \\remove \"Staff_performer\"\n"
                "    }\n"
                "    \\context {\n"
                "      \\Voice\n"
                "      \\consists \"Staff_performer\"\n"
                "      midiMinimumVolume = #0.8\n"
                "      midiMaximumVolume = #0.9\n"
                "    }\n"
                "  }\n"
                "}\n";

        stringSong.close();
    }
}

string LilypondWriter::toLilyName(int note) {
    if (note != -1) {
        int octave = toOctave(note);
        note = note % SEMITONES_PER_OCTAVE;
        std::string lastNote, auxName;

        for (auto entry: noteNames) {
            if (entry.first > note) {
                break;
            }
            lastNote = tolower(entry.second[0]);
            auxName = entry.second + std::to_string(octave);
        }
//    std::cout << "note = " << note << std::endl;
//    std::cout << "toOctave(note) = " << octave << std::endl;
//    std::cout << "lastNote = " << lastNote << std::endl;
//    std::cout << "auxName = " << auxName << std::endl;
//    std::cout << "toNote(auxname) = " << toNote(auxName) << std::endl << std::endl;
        if (toNote(auxName) != note + octave * SEMITONES_PER_OCTAVE) {
            lastNote += "s";
        }

        int octaveOffset = 3;
        if (octave < octaveOffset) {
            for (int i = octave; i < octaveOffset; ++i) {
                lastNote += ',';
            }
        } else {
            for (int i = octaveOffset; i < octave; ++i) {
                lastNote += '\'';
            }
        }
        return lastNote;
    } else {
        return "r"; // silence
    }
}


/**
* TODO allow longer notes
*/
int LilypondWriter::toLilyDuration(double seconds, float tempo) {
//    return int(round(60 * 4.0 / (duration * tempo)));
    int pulses = int(std::round(4.0/toPulses(seconds, tempo)));
    return max(pulses, 1);
}

};  // gemu
};  // randomize
