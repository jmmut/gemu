/**
 * @file WavWriter.cpp
 * @author jmmut 
 * @date 2015-10-11.
 */

#include "WavWriter.h"
#include "WavSong.h"

namespace randomize {
namespace gemu {


WavWriter::WavWriter(std::string filename, int samplesPerSecond)
        : name(filename), volume(0.5), samplesPerSecond(samplesPerSecond) {

}

/**
 * @return itself so this can be chained like this: WavWriter().setDebug().write()
 */
WavWriter& WavWriter::setDebug(bool printSamples, double start, double end) {
    debug = printSamples;
    this->start = start;
    this->end = end;
    return *this;
}

void WavWriter::write(randomize::gemu::Song song) {
    double cursor = 0.0;
    WavSong wavSong(song, volume/song.tracks.size(), samplesPerSecond);

    for (const Track &track : song.tracks) {
//        vector<shared_ptr<ComplexNote>>::const_iterator it;
//        for (it = track.begin(); it < track.end(); it++) {
//            LOG_DEBUG("about to write note %d:%f", (*it)->getNote(), (*it)->seconds )
//            cursor += wavSong.writeNote(**it);
        int i = 0;
        for (const std::shared_ptr<ComplexNote> note : track) {
            LOG_DEBUG("about to write note %s:%f", note->name().c_str(), note->seconds)
            cursor += wavSong.writeNote(*note);
            i++;
        }
        wavSong.setCursor(0.0);
    }

    if (!wavSong.Guardar(name)) {
        throw std::runtime_error("could not write wav in " + name);
    }
    std::stringstream output("");
    wavSong.print(output, start, end);
    writeToFile(name + ".dat", output.str());
}

WavWriter &WavWriter::setVolume(double volume) {
    this->volume = volume;
    return *this;
}


};  // gemu
};  // randomize

