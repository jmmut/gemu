/**************************************************
 *
 * autor: Jose Miguel Mut
 *
 * fecha: 2015-02-07 20:25:55
 *
 * descripcion: main.cpp
 *
 ***************************************************/

#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>
#include <map>
#include <iostream>
#include <algorithm>
#include <iterator>

#include "io/SongReader.h"
#include "utils/String.h"
#include "util/EntropyCalculator.h"
#include "io/SoundStreamWriter.h"
#include "io/WavWriter.h"
#include "io/TextSongWriter.h"
#include "io/LilypondWriter.h"
#include "core/MusicGenerator.h"
#include "utils/exception/StackTracedException.h"


using namespace std;

namespace randomize {
namespace gemu {

namespace cliOptions {

enum Values {
    HELP, SOURCE, ALGORITHM, VERBOSITY, FILENAME, DURATION, PERIOD, DISSONANCE_RANGE,
    DISSONANCE_MINIMUM, NOTE, TIMBRE, SAMPLE_RATE, EVALUATE
};
map<enum Values, string> names = {
        {HELP,                  "help"},
        {SOURCE,                "source"},
        {ALGORITHM,             "algorithm"},
        {VERBOSITY,             "verbosity"},
        {FILENAME,              "filename"},
        {DURATION,              "duration"},
        {PERIOD,                "period"},
        {DISSONANCE_RANGE,      "dissonanceRange"},
        {DISSONANCE_MINIMUM,    "dissonanceMinimum"},
        {NOTE,                  "note"},
        {TIMBRE,                "timbre"},
        {SAMPLE_RATE,           "sampleRate"},
        {EVALUATE,              "evaluate"},
};

namespace Source {
enum Values {
    FILE, GENERATE
};

map<enum Values, string> names = {
        {FILE, "file"},
        {GENERATE, "generate"},
};
};  // Source

namespace Algorithm {
enum Values {
    DECIDER, DISSONANCE, RECURRENCE, RECURRENCE2, GLITCH, SORT
};
map<enum Values, string> names = {
        {DECIDER,     "decider"},
        {DISSONANCE,  "dissonance"},
        {RECURRENCE,  "recurrence"},
        {RECURRENCE2, "recurrence2"},
        {GLITCH,      "glitch"},
        {SORT,        "sort"},
};
};  // algorithm

namespace Evaluate {
enum Values {
    DISSONANCE, ENTROPY, HIERARCHICAL_ENTROPY, RECURRENCE
};
map<enum Values, string> names = {
        {DISSONANCE,  "dissonance"},
        {ENTROPY,  "entropy"},
        {HIERARCHICAL_ENTROPY, "hierarchicalEntropy"},
        {RECURRENCE, "recurrence"},
};
};  // EVALUATE

};  // cliOptions

};  // gemu
};  //randomize

namespace po = boost::program_options;

using namespace std;
using namespace randomize;
using namespace gemu;

using namespace gemu::cliOptions;
namespace po = boost::program_options;

void setLogLevel(const string &logLevel);
double checkDuration(double duration);
set<Evaluate::Values> checkEvaluations(const string &evaluations) ;

template<typename ENUM> string concatenateNames(map<ENUM, string> names);
multimap<float, ComplexNote> sortDissonance(string note, unsigned int timbre);


/**
 * Ugly hack to avoid writing lots of `.c_str()` when adding options.
 */
class options_description_string_init : public po::options_description_easy_init {
public:
    explicit options_description_string_init(po::options_description* owner)
            : po::options_description_easy_init(owner) {}

    options_description_string_init &operator()(const std::string &name,
            const po::value_semantic *s,
            const std::string &description) {
        po::options_description_easy_init::operator()(name.c_str(), s, description.c_str());
        return *this;
    }
    options_description_string_init &operator()(const std::string &name,
            const std::string &description) {
        po::options_description_easy_init::operator()(name.c_str(), description.c_str());
        return *this;
    }
};

int main (int argc, const char *const *argv) {
    SigsegvPrinter::activate();

    po::options_description desc("This program generates and/or analyzes music.");

    string source;
    string algorithm;
    string filename;
    double duration;
    double recurrencePeriod;
    double dissonanceRange;
    double dissonanceMinimum;
    string note;
    unsigned int timbre;
    int sampleRate;
    set<Evaluate::Values> testsToEvaluate;
    bool evaluate;

    string GENERATE = Source::names[Source::GENERATE];
    options_description_string_init add_options(&desc);

    int defaultDuration = 20;
    add_options
            (names[HELP] + ",h", "produce help message")
            (names[VERBOSITY] + ",v",
                    po::value<string>()->default_value(log::levelNames[log::INFO_LEVEL]),
                    "set the log level. allowed values: " + concatenateNames(log::levelNames))
            (names[SOURCE] + ",s",
                    po::value<string>(&source)->default_value(GENERATE),
                    "where the song comes from. allowed values: " + concatenateNames(Source::names))
            (names[ALGORITHM] + ",a",
                    po::value<string>(&algorithm)->default_value(Algorithm::names[Algorithm::DISSONANCE]),
                    "used if source is \"" + GENERATE + "\". allowed values: " + concatenateNames(Algorithm::names))
            (names[FILENAME] + ",f",
                    po::value<string>(&filename)->default_value("song"),
                    "if source is \"" + Source::names[Source::FILE] + "\", this must be the \".json\" file")
            (names[DURATION] + ",d",
                    po::value<double>(&duration)->default_value(defaultDuration),
                    "duration in seconds (up to 3600), used if source is \"" + GENERATE + "\"")
            (names[PERIOD] + ",p",
                    po::value<double>(&recurrencePeriod)->default_value(defaultDuration/2),
                    "how many seconds to use as generation cycle. used if source is \"" + GENERATE + "\"")
            (names[DISSONANCE_RANGE],
                    po::value<double>(&dissonanceRange)->default_value(0.2),
                    "how much (between 0 and 1) can the dissonance change. used if source is \"" + GENERATE + "\"")
            (names[DISSONANCE_MINIMUM],
                    po::value<double>(&dissonanceMinimum)->default_value(0.3),
                    "minimum dissonance (between 0 and 1). serves as base for the dissonance range. "
                            "used if source is \"" + GENERATE + "\"")
            (names[NOTE] + ",n",
                    po::value<string>(&note)->default_value("C4"),
                    "in english notation (C0 to B7). used to evaluate")
            (names[TIMBRE] + ",t",
                    po::value<unsigned int>(&timbre)->default_value(1023),
                    "timbre from 1 to 1023, as an integer mask of which harmonics are present")
            (names[SAMPLE_RATE] + ",r",
                    po::value<int>(&sampleRate)->default_value(22050),
                    "in integer hertz, how many samples per second contains the sound")
            (names[EVALUATE] + ",e",
                    po::value<string>()->default_value(""),
                    "comma-separated evaluations to perform. allowed values: " + concatenateNames(Evaluate::names))
            ;

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    po::notify(vm);

    setLogLevel(vm[names[VERBOSITY]].as<string>());

    if (vm.count(names[HELP])) {
        cout << "Usage: " << argv[0] << " [options]\n";
        cout << desc;
        return 0;
    }

    testsToEvaluate = checkEvaluations(vm[names[EVALUATE]].as<string>());
    evaluate = not testsToEvaluate.empty();


    vector<bool> recurrenceTracking;
    MusicGenerator music;

    music.name() = filename;

//    music.generate(7, 1, 1, 1);
//    music.generateScaled(15, 1, 1, 1);

    if (source == Source::names[Source::FILE]) {    // read from file
        ifstream f;
        f.open(filename.c_str());
        if (!f) {
            LOG_ERROR("file %s not found", filename.c_str())
        } else {
            try {
                music.getSong() = SongReader().setTimbre(timbre).read(f);
                filename = music.getSong().name;
            } catch (const invalid_argument &e) {
                throw std::invalid_argument(string("file ") + filename + " has invalid syntax: " + e.what());
            }
        }
    } else {
        if (algorithm == Algorithm::names[Algorithm::DECIDER]) {
            music.generateDecided(duration);
        } else if (algorithm == Algorithm::names[Algorithm::DISSONANCE]) {
            music.generateDissonance(duration, recurrencePeriod, dissonanceMinimum, dissonanceRange);
        } else if (algorithm == Algorithm::names[Algorithm::RECURRENCE]) {
            music.generateRecurrence(duration);
        } else if (algorithm == Algorithm::names[Algorithm::RECURRENCE2]) {
            recurrenceTracking = music.generateRecurrence2(
                                duration, 4, recurrencePeriod, dissonanceRange, dissonanceMinimum);
        } else if (algorithm == Algorithm::names[Algorithm::GLITCH]) {
            int i = 0;
            music.generateGlitch<unsigned char(double)>(duration, [&i] (double time) {
                unsigned char value = (i >> 13) * i * (i * (i >> 10));
                i++;
                return value;
            });
        } else if (algorithm == Algorithm::names[Algorithm::SORT]) {
            toNote(note);   // tries to parse, throws on fail
            cout << "consonance with " << note << endl;
            for (auto &entry : sortDissonance(note, timbre)) {
                cout << entry.first << ", " << entry.second.name() << endl;
            }
            return 0;
        } else {
            throw runtime_error("unknown --" + names[ALGORITHM] + ", use one of " + concatenateNames(Algorithm::names));
        }
    }

    if (evaluate) {
        if (testsToEvaluate.count(Evaluate::DISSONANCE) == 1) {
//            writeToFile(filename + ".dissonance.dat", [&] (ofstream & file) {
//                for (vector<shared_ptr<ComplexNote>> chord : music) {
//                    double conson = consonance(chord);
//                    cout << "consonance(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote())
//                    << ") = " << conson << endl;
//                    file << conson << endl;
//                }
//            }); {
            stringstream data;
            for (vector<shared_ptr<ComplexNote>> chord : music) {
                int size = chord.size();
                double conson = consonance(chord);
                cout << "consonance/" << size << "(" << toName(chord[0]->getNote()) << ", " << toName(chord[1]->getNote())
                << ") = " << conson << endl;
                data << conson << endl;
            }

            writeToFile(filename + ".dissonance.dat", data.str());
        }
        if (testsToEvaluate.count(Evaluate::ENTROPY) == 1) {
            EntropyCalculator<string> entropy;
            for (Chord chord : music) {
                string symbol;
                for (shared_ptr<ComplexNote> chordNote : chord) {
                    symbol += "," + chordNote->name();
                }
                entropy.add(symbol);
            }
            writeToFile(filename + ".entropy.dat", to_string(entropy.compute()));
        }
        if (testsToEvaluate.count(Evaluate::HIERARCHICAL_ENTROPY) == 1) {
            HierarchicalEntropyCalculator<string> entropy;
            for (Chord chord : music) {
                string symbol;
                for (shared_ptr<ComplexNote> chordNote : chord) {
                    symbol += "," + chordNote->name();
                }
                entropy.add(symbol);
            }
            writeToFile(filename + ".h_entropy.dat", [&](ofstream & os) {
                for (auto value : entropy.getRootEntropiesNormalized()) {
                    os << value << endl;
                }
            });
        }
        if (testsToEvaluate.count(Evaluate::RECURRENCE) == 1) {
            writeToFile(filename + ".recurrence.dat", [&] (ofstream &file) {
                for (auto repeated : recurrenceTracking) {
                    file << repeated << endl;
                }
            });
        }
    }

    // from here it is assumed that you want to print music.getSong(). otherwise, do a return 0 before here

    WavWriter(filename + ".wav", sampleRate).write(music.getSong());
    LilypondWriter(filename + ".ly").write(music.getSong());
    TextSongWriter(filename + ".txt", TextSongWriter::BASIC).write(music.getSong());
    TextSongWriter(filename + ".json", TextSongWriter::JSON).write(music.getSong());
    SoundStreamWriter(sampleRate).write(music.getSong());

    return 0;
}

void setLogLevel(const string &logLevel) {
    try {
        LOG_LEVEL(log::parseLogLevel(logLevel.c_str()));
    } catch (exception e) {
        throw runtime_error("unknown --" + names[VERBOSITY] + ", use one of " + concatenateNames(log::levelNames));
    }
}

double checkDuration(double duration) {
    if (duration > 3600 || duration < 0) {  // one  hour of music? really?
        invalid_argument argument("cowardly refusing to make more than one hour of music. "
                "Specify duration in seconds");
        throw argument;
    }
    return duration;
}

set<Evaluate::Values> checkEvaluations(const string &evaluations) {
    set<Evaluate::Values> testsToEvaluate;
    for (string test : Utils::String::split(evaluations, ",")) {
        bool invalidTest = true;
        for(pair<Evaluate::Values, string> entry : Evaluate::names) {
            if (entry.second == test) {
                testsToEvaluate.insert(entry.first);
                invalidTest = false;
            }
        }
        if (invalidTest) {
            throw utils::exception::StackTracedException("invalid test for evaluations: " + test);
        }
    }
    return testsToEvaluate;
}

template<typename ENUM>
string concatenateNames(map<ENUM, string> names) {
    stringstream ss;
    ss << "{ ";
    for (auto &entry : names) {
        ss << entry.second << " ";
    }
    ss << "}";
    return ss.str();
}


multimap<float, ComplexNote> sortDissonance(string note, unsigned int timbre) {
    multimap<float, ComplexNote> sorted;
    using entry = multimap<float, ComplexNote>::value_type;
    SimpleNote base(1, toNote(note), 1, timbre);
    Chord chord;

    chord.push_back(make_shared<SimpleNote>(1, toNote(note), 1, timbre));
    chord.push_back(make_shared<SimpleNote>(1, toNote(note), 1, timbre));

    for (int i = 0; i <= SEMITONES_PER_OCTAVE; ++i) {
        sorted.insert(entry(consonance(chord), *chord[1]));
        chord[1] = make_shared<SimpleNote>(1, toNote(note) + i+1, 1, timbre);
    }
    return sorted;
}

