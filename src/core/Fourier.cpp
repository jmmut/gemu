/**
 * @file Fourier.cpp
 * @author jmmut 
 * @date 2016-04-20.
 */
#include <memory>
#include "Fourier.h"
#include "SimpleNote.h"

using std::vector;
using std::function;

namespace randomize {
namespace gemu {

void Fourier::transform() {
    signal = new double[samplesCount];
    result = new fftw_complex[samplesCount];

    plan = fftw_plan_dft_r2c_1d(samplesCount,
            signal,
            result,
            FFTW_ESTIMATE);
    planned = true;

    int i = 0;
    if (wavSong.getSamples() != samplesCount) {
        throw randomize::utils::exception::StackTracedException("bad use of Fourier class. don't "
                "merge full song version and batch version usages. full: {fourier(wavSong); transform(); foreach()}."
                "batch based: {fourier(wavSong); setBatchSize(); transformNextBatch()}");
    }
    wavSong.forEachSample([&](float sample){
        signal[i++] = sample;
    });

    fftw_execute(plan);
}

static const int REAL = 0;
static const int IMAG = 1;

void Fourier::forEachReal(function<void(double)> userFunction) {
    for (unsigned int i = 0; i < samplesCount/2+1; ++i) {
        userFunction(result[i][REAL]);
    }
}

void Fourier::forEachImaginary(std::function<void(double)> userFunction) {
    for (unsigned int i = 0; i < samplesCount/2+1; ++i) {
        userFunction(result[i][IMAG]);
    }
}

Fourier::~Fourier() {
    if (signal != nullptr) {
        delete[] signal;
    }
    if (result != nullptr) {
        delete[] result;
    }
    if (planned) {
        fftw_destroy_plan(plan);
    }
}

void Fourier::forEachComplex(function<void(double real, double imaginary, unsigned int index)> userFunction) const {
    for (unsigned int i = 0; i < samplesCount/2+1; ++i) {
        userFunction(result[i][REAL], result[i][IMAG], i);
    }
}

//vector<unsigned int> Fourier::guessNotes() {
//    return guessNotes(0.1);
//}

Fourier::BatchAnalysis Fourier::guessNotes(double normalizedThreshold) const {
    Chord chord;
    vector<double> volumes;
    vector<double> values;
    double integral = 0;
    double volumeNormalizer = 1.0 / (samplesCount / 2 + 1); // see reminder 4.
    double seconds = samplesCount / static_cast<double>(wavSong.getHeader().samplerate);

    double max = 0;
    long maxIndex = -1;
    forEachComplex([&max, &maxIndex] (double real, double imaginary, unsigned int i) {
        double module = std::sqrt(real * real + imaginary * imaginary);
        if (module > max) {
            max = module;
            maxIndex = i;
        }
    });

    double threshold = max * normalizedThreshold;   // threshold without normalize
    if (threshold * volumeNormalizer < 0.001) {
        LOG_WARN("taking as threshold a very low value. It's likely that here there is silence, "
                "and if that's the case, the values given at %f s are plainly wrong, so I will skip this part",
                getLastBatchStartCursor());
    } else {
        forEachComplex([&](double real, double imaginary, unsigned int i) {
            double module = std::sqrt(real * real + imaginary * imaginary);
            values.push_back(module * volumeNormalizer);
            integral += module * volumeNormalizer;
            if (threshold <= module) {
                if (i != 0) {   // TODO: handle this. frequency==0 may mean frequency==2*samplerate
                    double freq = i/seconds;
                    int noteIdx = toNote(freq);
                    unsigned int unsignedNoteIdx = static_cast<unsigned int>(noteIdx);
                    if (noteIdx >= 0 && unsignedNoteIdx < frequencies.size()) {
                        if (chord.size() != 0 && chord.back()->getNote() == noteIdx) {
                            // to remove repeated notes that appear as slightly different frequencies
                            volumes.back() += module * volumeNormalizer;
//                        std::shared_ptr<ComplexNote> lastNote = chord.back();
                            chord.pop_back();
                        } else {
                            volumes.push_back(module * volumeNormalizer);
                        }
                        chord.emplace_back(std::make_shared<SimpleNote>(
                                seconds, unsignedNoteIdx, volumes.back(), timbre));

                    }
                }
            }
        });
    }

    if (volumes.size() != chord.size()) {
        std::stringstream ss;
        ss << "postcondition violated: sizes mismatch. volumes.size(): " << volumes.size()
        << ", chord.size(): " << chord.size();
        LOG_ERROR("%s", ss.str().c_str());
        throw randomize::utils::exception::StackTracedException(ss.str());
    }

    return {volumes, chord, max*volumeNormalizer, integral, values};
}

void Fourier::setBatchSize(double seconds) {
    samplesCount = seconds * wavSong.getHeader().samplerate;
}

std::pair<long, long> Fourier::getSpectrumSizes() {
    size_t batches = wavSong.getSamples() / samplesCount;
    return {samplesCount/2 +1, batches};
}

bool Fourier::transformNextBatch() {
    size_t batchSize = samplesCount;
    size_t alreadyProcessedSamples = currentBatch * samplesCount;
    size_t remainingSamples = wavSong.getSamples() - alreadyProcessedSamples;
    bool moreDataToProcess = true;

    if (remainingSamples == 0) {
        moreDataToProcess = false;
        return moreDataToProcess;
    }

    if (remainingSamples < samplesCount) {
        // replan and execute last batch (smaller than batchSize requested)
        batchSize = remainingSamples;
        makePlan(remainingSamples);
        moreDataToProcess = false;
    }

    if (!planned) {
        makePlan(batchSize);
    }

    size_t i = 0;
    wavSong.forEachSample(alreadyProcessedSamples, alreadyProcessedSamples + batchSize,
            [&](float sample){
                signal[i++] = sample;
            }
    );

    fftw_execute(plan);

    currentBatch++;

    return moreDataToProcess;
}

void Fourier::makePlan(size_t batchSize) {
    if (planned) {
        //replan

        if (signal != nullptr) {
            delete[] signal;
        }
        if (result != nullptr) {
            delete[] result;
        }
        if (planned) {
            fftw_destroy_plan(plan);
        }
    }

    signal = new double[batchSize];
    result = new fftw_complex[batchSize];

    plan = fftw_plan_dft_r2c_1d(batchSize,
            signal,
            result,
            FFTW_ESTIMATE);
    planned = true;
}

double Fourier::getLastBatchStartCursor() const {
    return (currentBatch - 1) * samplesCount / static_cast<double>(wavSong.getHeader().samplerate);
}


double Fourier::getLastBatchEndCursor() const {
    return currentBatch * samplesCount / static_cast<double>(wavSong.getHeader().samplerate);
}

void Fourier::setTimbre(int timbre) {
    this->timbre = timbre;
}


};  // gemu
};  // randomize







