/**
 * @file BeatsPerMinute.h
 * @author jmmut 
 * @date 2016-05-16.
 */

#ifndef GEMU_BEATSPERMINUTE_H
#define GEMU_BEATSPERMINUTE_H

#include <io/WavSong.h>

namespace randomize {
namespace gemu {

double periodInSamples(size_t nesting, std::function<void(std::function<void(size_t, float)> next)> iteratorFunction);

class BeatsPerMinute {
public:

    BeatsPerMinute(WavSong &wavSong, size_t start, size_t end)
            : wavSong(wavSong), start(start), end(end) { }
    double bpm(double periodInSeconds);
    double periodInSamples(size_t nesting);
    double periodInSeconds(double periodInSamples);
private:
    WavSong & wavSong;

    std::size_t start, end;
};


};  // gemu
};  // randomize

#endif //GEMU_BEATSPERMINUTE_H
