#include "core/Gemu.h"

using namespace std;
using randomize::utils::exception::StackTracedException;

namespace randomize {
namespace gemu {


std::array<double, 12 * 8> frequencies = generateFrequencies();
std::map<int, std::string> noteNames = {{0, "C"}, {2, "D"}, {4, "E"}, {5, "F"}, {7, "G"}, {9, "A"}, {11, "B"}};//{{"A", 0}, {"B", 2}, {"C", 3}, {"D", 5}, {"E", 7}, {"F", 8}, {"G", 10}};

std::array<double, 12 * 8> generateFrequencies () {
    std::array<double, 12 * 8> frequencies;
    frequencies[0] = FREQUENCY_C0;
    for (int i = 1; i < 12 * 8; ++i) {
        frequencies[i] = frequencies[i-1] * SQRT_12_2;
    }
    return frequencies;
}


/**
* note must be format [A-G][fsb#]?[0-7]
* A-G: mandatory note name
* fsb#: optional flat, sharp, bemol, sostenido (#)
* 0-7: mandatory octave of the note
*/
int toNote(std::string name) {
    int octave = static_cast<int>(name[name.size()-1]) - static_cast<int>('0');
    if (octave < 0 || octave > 9) {
        StackTracedException e(name + " could not be parsed to octave [0-9]");
        throw e;
    }
    int note = -1;
    int noteName = toupper(name[0]);
    for (auto entry : noteNames) {
        if (entry.second[0] == noteName) {
            note = entry.first;
            break;
        }
    }
    if (note == -1) {
        StackTracedException e(name + " is not a parseable note");
        throw e;
    }
    if (name.size() == 3){
        if (name[1] == '#' || name[1] == 's') {
            note++;
        } else if (name[1] == 'f' || name[1] == 'b') {
            note--;
        } else {
            StackTracedException e(name + " has not a valid modifier /[fsb#]/");
            throw e;
        }
    }
    return note + octave*SEMITONES_PER_OCTAVE;
//    return operator "" _n(name.c_str(), name.size());
}


std::string toFullName(int note, double duration) {
    std::string name("");
    name += toName(note);
    if (toNote(name) != note) {
        name += "#";
    }
    name += std::to_string((float)duration);
    return name;
}

std::string toName(int note) {
    int octave = toOctave(note);
    note = note % SEMITONES_PER_OCTAVE;
    std::string lastNote;

    for (auto entry: noteNames) {
        if (entry.first > note) {
            break;
        }
        lastNote = entry.second;
    }
//    std::cout << "note = " << note << std::endl;
//    std::cout << "toOctave(note) = " << octave << std::endl;
//    std::cout << "lastNote = " << lastNote << std::endl;
    std::string auxName(lastNote + std::to_string(octave));
//    std::cout << "auxName = " << auxName << std::endl;
//    std::cout << "toNote(auxname) = " << toNote(auxName) << std::endl << std::endl;
    if (toNote(auxName) != note + octave*SEMITONES_PER_OCTAVE) {
        lastNote += "#";
    }
    lastNote += std::to_string(octave);
    return lastNote;
}

double toFrequency(int note) {
    return note >= 0 && (unsigned int)note < frequencies.size()? frequencies[note] : 1.0;
}


int toNote(double frequency) {
//    cout << "frequency/FREQUENCY_A0 = " << frequency/FREQUENCY_A0 << endl;
//    cout << "log2(frequency/FREQUENCY_A0) = " << log2(frequency/FREQUENCY_A0) << endl;
//    cout << "round(log2(frequency/FREQUENCY_A0)*12) = " << round(log2(frequency/FREQUENCY_A0)*12) << endl;
    if (frequency <= 0) {
        std::stringstream ss;
        ss << "domain error: frequency: " << frequency;
        throw randomize::utils::exception::StackTracedException(ss.str());
    }
    return std::round(std::log2(frequency / FREQUENCY_C0) * 12); /// f = 2^{n/12} * f_0
}

double toFrequency(std::string name) {
    if (name.size() == 2 || name.size() == 3) {
        return frequencies[toNote(name)];
    } else {
        return 1;
    }
}
std::string toName(double frequency) {
    return toName(toNote(frequency));
}

int toOctave(int note) {
    return note/SEMITONES_PER_OCTAVE;
}

double toPulses(double seconds, float tempo) {
    return seconds * tempo / 60.0;
}

double toSeconds(double pulses, float tempo) {
    return pulses * 60.0 / tempo;
}

bool writeToFile(string name, string content) {
    return writeToFile(name, [&] (ofstream& file) {
        file << content;
    });
}

bool writeToFile(string name, istream &content) {
    return writeToFile(name, [&] (ofstream& file) {
        file << content.rdbuf();
    });
}

bool writeToFile(string name, function<void(ofstream& file)> writeFunction) {
    ofstream f;

    f.open(name.c_str());
    if (!f) {
        LOG_ERROR("could not write to file %s", name.c_str());
        return false;
    }

    writeFunction(f);

    f.close();
    return (bool) f;
}


void openFileOrThrow(string name, ofstream &f) {
    f.open(name.c_str());
    if (!f) {
        string message = "could not write to file";
        message += name;
        LOG_ERROR("%s", message.c_str());
        throw StackTracedException(message);
    }
}

};  // gemu
};  // randomize


