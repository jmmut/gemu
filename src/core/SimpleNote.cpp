
#include "core/SimpleNote.h"
using namespace std;
namespace randomize {

namespace gemu {

double SimpleNote::value(double time) const {
    return ComplexNote::value(time);
}

int SimpleNote::getNote() const {
    return note;
}

std::string SimpleNote::name() const {
    return toName(note);
}


};  // gemu
};  // randomize
