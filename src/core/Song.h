/**
 * @file Song.h
 * @author jmmut 
 * @date 2015-09-13.
 */

#ifndef GEMU_SONG_H
#define GEMU_SONG_H

#include <algorithm>
#include <stdexcept>
#include <sstream>
#include "ComplexNote.h"
#include "log/log.h"
#include "utils/Walkers.h"


namespace randomize {
namespace gemu {
/**
* Common container for a logical song.
*
* Does not include obvious (at first sight) concepts like duration in seconds, or master volume, because they are only
* needed in some representations. A lilypond score does not need the volume like a WavSong does.
*/
struct Song {
    double seconds;
    float tempo;
    std::string name;
    std::vector<Track> tracks;

    Song() {
    }

    Song(double seconds, float tempo, std::string &&name) : seconds(seconds), tempo(tempo), name(name), tracks() {
    }

    Song(double seconds, float tempo, std::string &name, std::vector<Track> const &tracks)
            : seconds(seconds), tempo(tempo), name(name), tracks(tracks) {
    }

    std::string toString();
    std::string toVerboseString();

    /**
     * This works for any number of tracks, not only 2
     */
    class Iterator {
    private:
        Song * song;
        std::vector<double> instants; ///< time since the start of a note
        std::vector<double> cursors; ///< accumulated time until last note
    public:
        std::vector<size_t> chord;  ///< indices to notes


        Iterator(Song *song);
        Iterator(Song *song, double instant);

        bool operator!=(Iterator it2);
        void operator++();
        Chord operator*();
        std::vector<double> getCursors();
        std::vector<double> getInstants();
        /**
         * each element `i` of `chord` is an integer index to a note of the track `i`.
         */
        void finished();


        /**
         * @param seconds, passed by reference
         * @return true if the value is valid, if false, the parameter is not changed.
         */
        bool getMinRemainingNoteTime(double &seconds);
        /**
         * Can return a vector of size different to number of tracks if some track is finished
         */
        std::vector<double> getRemainingNoteTime();
        /**
         * returns a chord with all null notes except the note that changes first in the chord the iterator is pointing to.
         * Note that several notes may change first, at the same time.
         *    [a a|c|d]
         *    [e|f f|g]
         * it:   ^     : returns [c|null]
         * it:     ^   : returns [d|g]
         */
        Chord getNextNote();
    };

    class Appender {
    private:
        Song *song;
        Song::Iterator it;


    public:
        Appender(Song *song);

        Song::Iterator &getIterator() {
            return it;
        }

        void pushBack(unsigned int track, std::shared_ptr<ComplexNote> note);
        Chord getLastCompleteChord();
        std::shared_ptr<ComplexNote> getLast(int track);

        Chord getDraftOfNewChord();
    };

    Song::Iterator begin();
    Song::Iterator end();


};

};  // gemu
};  // randomize



#endif //GEMU_SONG_H
