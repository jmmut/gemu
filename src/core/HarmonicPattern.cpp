//
// Created by jmmut on 2015-04-09.
//

#include "core/HarmonicPattern.h"

namespace randomize {
namespace gemu {


std::shared_ptr<ComplexNote> HarmonicPattern::newComplexNote(double duration, int note, float volume) const {
    std::shared_ptr<ComplexNote> complexNote(new ComplexNote());
    complexNote->setConfig(
            duration,
            std::unique_ptr<LinearInterpolator>(new LinearInterpolator({toFrequency(note)})),
            std::unique_ptr<LinearInterpolator>(new LinearInterpolator({volume})),
            harmonics);
    return complexNote;
}

namespace instruments {

const HarmonicPattern SINE{{Harmonic(1.0, 1.0)}};
const HarmonicPattern SQUARE{ComplexNote::buildHarmonics(0x55555555)};  // 0b ... 0101 along 32 bits
const HarmonicPattern SOFT_QUARE{ComplexNote::buildHarmonics(0x00005555)};  // clipping high frequencies, no high beep

const HarmonicPattern FLUTE
{{
//        Harmonic(1.0/16, 0.5),
//        Harmonic(1, 1.0/1),
//        Harmonic(2, 1.0/2),
//        Harmonic(3, 1.0/3),
//        Harmonic(4, 1.0/4)
//        Harmonic(1.0/16, 0.031623), // from http://sooeet.com/music/musical-instrument-spectrum.php
        Harmonic(1.0/16, 0.0295121),
        Harmonic(1, 0.0638263),
        Harmonic(2, 0.042658),
        Harmonic(3, 0.0229087),
        Harmonic(4, 0.00776247),
        Harmonic(5, 0.0044157),
        Harmonic(6, 0.00316228),
        Harmonic(7, 0.00138038),
        Harmonic(8, 0.000582103),
        Harmonic(9, 0.0011749),
        Harmonic(10, 0.000457088),
        Harmonic(10.534138741, 0.000141254),
        Harmonic(11, 0.000251189),
        Harmonic(11.363568388, 0.000175792),
        Harmonic(12, 0.000177828),
        Harmonic(12.621090756, 8.91251e-05),
        Harmonic(13, 7.16143e-05),
        Harmonic(13.190606965, 0.00011885),
        Harmonic(14, 8.81049e-05),
        Harmonic(14.35, 7.94328e-05),
        Harmonic(14.63, 0.000149624),
        Harmonic(15, 0.000133352),
        Harmonic(15.25, 0.000146218)
}};


}   // instruments

};  // gemu
};  // randomize


