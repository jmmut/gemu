#ifndef __Harmonic_H_
#define __Harmonic_H_

#include <memory>
#include "util/Interpolator.h"

namespace randomize {
namespace gemu {


class Harmonic {
private:
    std::shared_ptr<Interpolator> frequencyCoefficient;
    std::shared_ptr<Interpolator> amplitudeCoefficient;

public:
//    Harmonic() {
//    }

    Harmonic(const double &frequencyCoefficient, const double &amplitudeCoefficient);

    void setFrequencyCoef(double const &coef);

//    void setFrequencyCoef(Interpolator &coef);

    void setAmplitudeCoef(double const &coef);

//    void setAmplitudeCoef(Interpolator &coef);

    double amplitudeCoef(double time) const;

    double frequencyCoef(double time) const;

};

};  // gemu
};  // randomize


#endif //__Harmonic_H_
