
#include "core/ComplexNote.h"


using namespace std;

namespace randomize {
namespace gemu {



// ComplexNote

double ComplexNote::value(double time) const {
    double value = 0;
    double maxAmplitude = 0;
    const double normalizedTime = time/ seconds;
    double amplitude = this->amplitude->value(normalizedTime);
    double frequency = this->frequency->value(normalizedTime);
    for (const Harmonic &harmonic : harmonics) {
        double amplitudeCoef = harmonic.amplitudeCoef(normalizedTime);
        maxAmplitude += amplitudeCoef;
        value += amplitudeCoef * cos(2 * M_PI * harmonic.frequencyCoef(normalizedTime) * frequency * time);
    }
    value = value / maxAmplitude * amplitude;
    return value;
}

void ComplexNote::setConfig(double duration, std::shared_ptr<Interpolator> frequency,
        std::shared_ptr<Interpolator> amplitude, std::vector<Harmonic> harmonics) {
    this->seconds = duration;
    this->frequency = frequency;
    this->amplitude = amplitude;
    this->harmonics = harmonics;
}


void ComplexNote::setConfig(double duration, double frequency, double amplitude, unsigned int harmonics) {
    vector<Harmonic> harmonicsVector = buildHarmonics(harmonics);
    auto freqInterpolator = std::shared_ptr<LinearInterpolator>(new LinearInterpolator{frequency});
    auto ampInterpolator = std::shared_ptr<LinearInterpolator>(new LinearInterpolator(
            {{0, 0}, {0.1, 1 * amplitude}, {0.8, 0.9 * amplitude}, {1, 0}}));

    this->setConfig(duration, freqInterpolator, ampInterpolator, harmonicsVector);
}

vector<Harmonic> ComplexNote::buildHarmonics(unsigned int harmonics) {
    vector<Harmonic> harmonicsVector = std::vector<Harmonic>();
    for (unsigned int i = 0; i < sizeof(int)*8; i++) {
        if (harmonics & (1 << i)) {
            Harmonic harmonic(i+1, 1.0 / (i+1));    // i-th harmonic: baseFrequency * i, baseAmplitude / i
            harmonicsVector.push_back(harmonic);
        }
    }
    return harmonicsVector;
}

void ComplexNote::setConfig(double duration, double frequency) {
    this->setConfig(duration, frequency, DEFAULT_AMPLITUDE, DEFAULT_TIMBRE);
}

double ComplexNote::getBaseFrequency() const {
    return frequency->value(0);
}

int ComplexNote::getNote() const {
    return toNote(getBaseFrequency());
}

std::vector<Harmonic> const &ComplexNote::getHarmonics() const {
    return harmonics;
}

std::vector<double> ComplexNote::getFrequencies() const {
    double instant = 0;
    std::vector<double> frequencies;
    for (const Harmonic &harmonic : harmonics) {
        frequencies.push_back(harmonic.frequencyCoef(instant) * getBaseFrequency());
    }
    return frequencies;
}

std::string toString(const Track &track, float tempo) {
    std::stringstream stringSong;
    for (const auto &note : track) {
        if (note == nullptr) {
            stringSong << "null ";
        } else {
            stringSong << "<" << note->name() << ">" << toPulses(note->seconds, tempo) << " ";
        }
    }
    return stringSong.str();
}

std::string ComplexNote::name() const {
    int noteNum = toNote(getBaseFrequency());
    return toName(noteNum);
}

};  // gemu
};  // randomize
