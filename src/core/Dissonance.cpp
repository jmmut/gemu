#include "core/Dissonance.h"

using namespace std;
namespace randomize {
namespace gemu {

double consonance(vector<shared_ptr<ComplexNote>> chord) {
    vector<double> instants = vector<double>(chord.size(), 0);
    for (double &instant : instants) {
        instant = 0;
    }
    return dissonance(chord, instants, EPSILON_DISSONANCE);
}

double dissonance(vector<shared_ptr<ComplexNote>> chord, vector<double> instants, double epsilon) {
//
//double consonance(const vector<shared_ptr<ComplexNote>> &chord) {
//    vector<double> instants = vector<double>(chord.size(), 0);
//    for (double &instant : instants) {
//        instant = 0;
//    }
//    return dissonance(chord, instants, EPSILON_DISSONANCE);
//}
//
//double dissonance(const vector<shared_ptr<ComplexNote>> &chord, const vector<double> &instants, double epsilon) {

    if (chord.size() != 2) {
        return 1;   // TODO unimplemented
    }

    std::size_t firstNoteHarmonic = 0;
    std::size_t secondNoteHarmonic = 0;
    int coincidences = 0;
    int dissonances = 0;
    std::vector<Harmonic> const &firstNoteHarmonics = chord[0]->getHarmonics();
    std::vector<Harmonic> const &secondNoteHarmonics = chord[1]->getHarmonics();
    while (firstNoteHarmonic < firstNoteHarmonics.size() && secondNoteHarmonic < secondNoteHarmonics.size()) {
        double firstFrequencyCoef = firstNoteHarmonics[firstNoteHarmonic].frequencyCoef(instants[0]);
        double firstFundamentalFrequency = firstFrequencyCoef * chord[0]->getBaseFrequency();
        double secondFrequencyCoef = secondNoteHarmonics[secondNoteHarmonic].frequencyCoef(instants[1]);
        double secondFundamentalFrequency = secondFrequencyCoef * chord[1]->getBaseFrequency();
        if (isEquivalentFrequency(firstFundamentalFrequency, secondFundamentalFrequency, epsilon)) {
            coincidences += 2;
            firstNoteHarmonic++;
            secondNoteHarmonic++;
        } else {
            if (firstFundamentalFrequency < secondFundamentalFrequency) {
                firstNoteHarmonic++;
                dissonances++;
            } else {
                secondNoteHarmonic++;
                dissonances++;
            }
        }
    }
    return coincidences / double(coincidences + dissonances);
}


/**
 * here I am using ::log because it collides with randomize::log, and as
 * we are inside namespace randomize, the logger is used by default.
 * Otherwise you get a compiler error:
 *
 * src/core/Dissonance.cpp: In function ‘bool randomize::gemu::isEquivalentFrequency(double, double, double)’:
 * src/core/Dissonance.cpp:51:19: error: expected primary-expression before ‘(’ token
 *      return abs(log(firstFrequency) - log(secondFrequency)) < epsilon;
 *                    ^
 */
bool isEquivalentFrequency(double firstFrequency, double secondFrequency, double epsilon) {
    return abs(::log(firstFrequency) - ::log(secondFrequency)) < epsilon;
}

};  // gemu
};  // randomize

