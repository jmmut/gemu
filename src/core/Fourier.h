/**
 * @file Fourier.h
 * @author jmmut 
 * @date 2016-04-20.
 */

#ifndef GEMU_FOURIER_H
#define GEMU_FOURIER_H

#include <io/WavSong.h>
#include <fftw3.h>
#include <complex>

namespace randomize {
namespace gemu {

/**
 * This class uses FFTW (Fastest Fourier Transform from the West, although I like more
 * Fourier For The Win) to transform a sound signal into its frequency spectrum.
 *
 * Some reminders:
 * 1. the result is half long (length*0.5 + 1) than the input, as it's split in real and imaginary.
 * 2. in the result, the index is the frequency in hertz of the whole signal. If the signal
 *      is X seconds, you have to divide the index by X to get the real frequency.
 * 3. in the result, each value is the real or imaginary volume of a frequency. The actual
 *      amount is the modulus, so use that if you don't use the angle.
 * 4. The maximum amplitude of the fourier signal is its length. So if the source signal is a
 *      sine with full amplitude, 44100 samples long, the fourier signal will be a complex signal
 *      of length 22051 with a modulus peak of 22051.
 */
class Fourier {
public:
    struct BatchAnalysis;

    Fourier(const WavSong &wavSong)
            : result(nullptr), signal(nullptr), wavSong(wavSong),
            samplesCount(wavSong.getSamples()), currentBatch(0), planned(false), timbre(1) { }
    ~Fourier();

    void setBatchSize(double seconds);
    std::pair<long, long> getSpectrumSizes();
    /** returns if couldn't transform because there's no more data to process */
    bool transformNextBatch();
    void transform();

    void forEachReal(std::function<void(double)> userFunction);
    void forEachImaginary(std::function<void(double)> userFunction);
    void forEachComplex(std::function<void(double real, double imaginary, unsigned int index)> userFunction) const;


//    std::vector<unsigned int> guessNotes();
    /**
     * threshold is the volume in range [0, 1], where 0 is silence and 1 is the
     * loudest frequency in the batch. This allows for different dynamics.
     * Post-condition: chord.size() == volumes.size()
     * TODO: allow absolute threshold as well
     */
    BatchAnalysis guessNotes(double threshold) const;

    double getLastBatchStartCursor() const;
    double getLastBatchEndCursor() const;

    void setTimbre(int timbre);

private:
    fftw_complex * result;
    double * signal;
    fftw_plan plan;
    const WavSong &wavSong;
    std::size_t samplesCount;       ///< size in samples of a batch
    size_t currentBatch;
    bool planned;
    int timbre;

    void makePlan(size_t batchSize);

};

struct Fourier::BatchAnalysis {
    std::vector<double> volumes;
    Chord chord;
    double maxVolume;
    double integral;
    std::vector<double> transform;
};

};  // gemu
};  // randomize


#endif //GEMU_FOURIER_H
