#ifndef _MUSIC_GENERATOR_H_
#define _MUSIC_GENERATOR_H_

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <random>
#include <memory>
#include <algorithm>
#include "core/Song.h"
#include "core/SimpleNote.h"
#include "core/GlitchNote.h"
#include "util/Decider.h"
#include "core/Dissonance.h"
#include "core/HarmonicPattern.h"
#include "utils/SignalHandler.h"
#include "util/Walkers.h"
#include "io/SongWriter.h"



namespace randomize {
namespace gemu {

struct GenerationConfig{
    double duration;    // in seconds
    int numTracks;
    std::vector<Decider<float>> noteLengths;
    std::vector<int> pitchAverages;
    std::vector<function<function<int()> (std::normal_distribution<>&, std::mt19937&)>> pitchGenerators;
    std::vector<float> pitchDeviations;
    std::vector<long unsigned int> timbres;
    std::vector<float> volumes;
    int minNote;
    int maxNote;
};
/**
* Class to generate music.
*
* The level of description to give is not intended to be note to note nor happy or sad.
* Instead, this is focus in some mid-level as period of the cycle of tension-resolution,
* or minimum and maximum levels of dissonance.
*
* TODO: generify to more than 2 tracks. See Appender
*/
class MusicGenerator {
private:
    Song song;

    std::random_device rd;
    std::mt19937 gen;
    struct MusicStats {
        int randSum;
        int correctionSum;
        int basicIncrementSum;
    } stats;
public:
    MusicGenerator();
    ~MusicGenerator();

    std::string &name();
    float &tempo();
    const double &seconds();
    Song & getSong();

    ComplexNote* nextGaussianNote(double seconds, double volume, unsigned int timbre, int previousNote, int minNote,
            int maxNote, function<int()> randomFunc);
    Track generateTrack(double &duration, Decider<float> &durationDecider, float volume, int firstNote, int maxNote, int minNote, unsigned int timbre, function<int()> &randomFunc);
    int roundToScale(int note, int maxNote, int minNote);
    void resetStats();

//    void writeTrack(Track track, )
//    void generate(float seconds, float cyclePeriod, float maxDissonance, float minDissonance);
//    void generateScaled(float seconds, float cyclePeriod, float maxDissonance, float minDissonance);
    void generate(GenerationConfig config);
    void generateDecided(double duration);
    std::shared_ptr<ComplexNote> nextConsonantNote(int consonanceTrack, int baseTrack,
            std::shared_ptr<ComplexNote> &previousNote, double seconds, uint32_t timbre, double volume, int minNote, int maxNote,
            std::normal_distribution<> &dist, double targetConsonance, double maxAllowedError,
            Chord &chord, double &noteError);
    void generateDissonance(double duration);
    void generateDissonance(double duration, function<double(double)> fitting);
    void generateDissonance(double duration, double period, double dissonanceMinimum,
        double dissonanceRange);
    void generateHarmonicSequenceTest();
    void generateHarmonicNoteTest();
    void generateHarmonicPatternTest();
    void generateTest();

    std::vector<bool> generateRecurrence(double duration, function<double(double)> fitting);
    std::vector<bool> generateRecurrence(double duration);
    std::vector<bool> generateRecurrence2(double duration, function<double(double)> fitting,
            int deviation, double recurrencePeriod);
    std::vector<bool> generateRecurrence2(double duration, int deviation);
    std::vector<bool> generateRecurrence2(double duration, int deviation,
            double recurrencePeriod, double dissonanceRange, double minimumDissonance);

    template<typename T>
    void generateGlitch(double duration, std::function<T> func) {
        Track track;
        track.emplace_back(new GlitchNote(duration, func));

        this->song.tracks.push_back(track);
//    this->song.tracks.push_back({{new GlitchNote(duration, func)}});
        this->song.seconds = duration;
    }



    void write(std::shared_ptr<SongWriter> exporter);

    Song::Iterator begin();
    Song::Iterator end();

};

};  // gemu
};  // randomize

#endif // _MUSIC_GENERATOR_H_

