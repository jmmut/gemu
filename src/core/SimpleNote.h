#ifndef _NOTE_H_
#define _NOTE_H_

#include <cmath>
#include <iostream>
#include "core/ComplexNote.h"

namespace randomize {
namespace gemu {

class SimpleNote : public ComplexNote {
private:
public:
    int note;
    float volume;
    uint32_t timbre; /// mask of harmonics. (n-1)th bit tells the presence of fundamental*n harmonic with 1/n volume.
    // envelope? more complex timbres (different pattern of volumes)?
public:
    SimpleNote() {
    }

    SimpleNote(double duration, int note, float volume, uint32_t timbre)
            : note(note), volume(volume), timbre(timbre) {
        this->seconds = duration;
        this->setConfig(duration, toFrequency(note), volume, timbre);
    }

    double value(double time) const override;
    std::string name() const override;
    int getNote() const override;
};

class SilenceNote : public ComplexNote {
public:
    SilenceNote(double seconds) { ComplexNote::seconds = seconds; }

    virtual double value(double time) const override {
        return 0.0;
    }

    virtual std::string name() const override {
        return "silence";
    }

    virtual int getNote() const override {
        return -1;
    }

private:


};
};  // gemu
};  // randomize

#endif // _NOTE_H_
