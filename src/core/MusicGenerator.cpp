#include <cassert>
#include <io/LilypondWriter.h>
#include "core/MusicGenerator.h"


using namespace std;

namespace randomize {
namespace gemu {

MusicGenerator::MusicGenerator()
        : song(0.0, 120.0, string("name.wav")), rd(), gen(rd()) {
    resetStats();
    SigsegvPrinter::activate();
}

MusicGenerator::~MusicGenerator() {

}

std::string &MusicGenerator::name() {
    return song.name;
}

float &MusicGenerator::tempo() {
    return song.tempo;
}

const double &MusicGenerator::seconds() {
    return song.seconds;
}
int MusicGenerator::roundToScale(int rawNote, int maxNote, int minNote) {
    rawNote = max(minNote, min(rawNote, maxNote));
    int module = rawNote % SEMITONES_PER_OCTAVE;
    int octave = (rawNote / SEMITONES_PER_OCTAVE) * SEMITONES_PER_OCTAVE;
    int correctedNote = (--noteNames.upper_bound(module))->first + octave;
    return correctedNote;
}

/**
 * @param randomFunc must give a random increment over last note, centered in 0.
 * @return ComplexNote* pointer to the gaussian generated note.
*/
ComplexNote* MusicGenerator::nextGaussianNote(double seconds, double volume, unsigned int timbre, int previousNote,
        int minNote, int maxNote, function<int()> randomFunc) {
    int randNote(randomFunc());
    int rawNote = previousNote + randNote;
    int correctedNote = roundToScale(rawNote, maxNote, minNote);
    LOG_EXP(seconds, "%f")
    LOG_EXP(previousNote, "%d")
    LOG_EXP(randNote, "%d")
    if (correctedNote != rawNote) {
        LOG_DEBUG("changing note! %d to %d", rawNote, correctedNote)
    }
    SimpleNote *simpleNote = new SimpleNote(seconds, correctedNote, volume, timbre);
    stats.randSum += randNote;
    stats.correctionSum += correctedNote - rawNote;
    stats.basicIncrementSum += correctedNote - previousNote;
    return simpleNote;
}

Track MusicGenerator::generateTrack(double &duration, Decider<float> &durationDecider, float volume
        , int firstNote, int maxNote, int minNote, unsigned int timbre, function<int()> &randomFunc) {
    double cursor;
    int i;
    double noteDuration;
    ComplexNote *complexNote;
    Track track;
    int previousNote = firstNote;
    for (cursor = 0, i = 0; cursor < duration; cursor += noteDuration, i++) {
        noteDuration = durationDecider();
        complexNote = nextGaussianNote(noteDuration, volume, timbre, previousNote, minNote, maxNote, randomFunc);
        track.emplace_back(complexNote);
        previousNote = track[i]->getNote();
    }
    duration = cursor;
    return track;
}

void MusicGenerator::resetStats() {
    memset(&stats, 0, sizeof(MusicStats));
}

void MusicGenerator::generate(GenerationConfig config) {
    double trackDuration;
    double maxDuration = config.duration;
    for (int i = 0; i < config.numTracks; ++i) {
        trackDuration = config.duration;
        std::normal_distribution<> normal_dist(0, config.pitchDeviations[i]);
        function<int ()> randomNote = config.pitchGenerators[i](normal_dist, gen);

//        function<int ()> randomNote = [&normal_dist, this]() {
//            return int(std::round(normal_dist(gen) + 5.0/12));  //  correcting out-of-scale down-rounding
//        };
        song.tracks.push_back(generateTrack(trackDuration, config.noteLengths[i], config.volumes[i]
                , config.pitchAverages[i], config.maxNote, config.minNote, config.timbres[i], randomNote));
        maxDuration = max(maxDuration, trackDuration);

        LOG_EXP(stats.basicIncrementSum, "%d")
        LOG_EXP(stats.randSum, "%d")
        LOG_EXP(stats.correctionSum, "%d")
        LOG_EXP(config.pitchAverages[i], "%d")
        LOG_EXP(trackDuration, "%f")
        LOG_EXP(song.seconds, "%f");

        resetStats();
    }
    config.duration = song.seconds = maxDuration;
}

/**
* generates *at least* _duration_ seconds of music.
*/
void MusicGenerator::generateDecided(double duration) {
    GenerationConfig config;

    config.duration = duration;
    config.numTracks = 2;

    vector<Decider<float>> durationDeciders;

    Decider<float> decider1(toSeconds(0.25, song.tempo), toSeconds(0.25, song.tempo), toSeconds(0.5, song.tempo)
            , 0.375, 0.125
            , [](float &value) {
                value /= 2;
            }
            , [](float &value) {
                value *= 2;
            });
    durationDeciders.push_back(decider1);

    Decider<float> decider2(toSeconds(2, song.tempo));
    durationDeciders.push_back(decider2);

    config.noteLengths = durationDeciders;
    function<function<int ()> (normal_distribution<>&, mt19937&)> noteGenerator = [](normal_distribution<> &dist, mt19937 &gen) {
        return [&dist, &gen]() {
            return int(std::round(dist(gen) + 5.0 / 12));  //  correcting out-of-scale down-rounding
        };
    };
    config.pitchGenerators = vector<function<function<int ()> (normal_distribution<>&, mt19937&)>>{noteGenerator, noteGenerator};
//        function<int ()> randomNote = [&normal_dist, this]() {
//            return int(std::round(normal_dist(gen) + 5.0/12));  //  correcting out-of-scale down-rounding
//        };
    config.pitchAverages = vector<int>{toNote("E4"), toNote("E3")};
    config.pitchDeviations = vector<float>{4,4};
    config.timbres = vector<long unsigned int>{gen(), gen()};
    config.volumes = vector<float>{0.25, 0.25};
    config.minNote = 30;
    config.maxNote = 70;

    generate(config);

};

/**
 * @param [out] noteError
 * @return shared_ptr<ComplexNote>
 */
shared_ptr<ComplexNote> MusicGenerator::nextConsonantNote(int consonanceTrack,
        int baseTrack,
        shared_ptr<ComplexNote> &previousNote,
        double seconds,
        uint32_t timbre,
        double volume,
        int minNote,
        int maxNote,
        normal_distribution<> &dist,
        double targetConsonance,
        double maxAllowedError,
        Chord &chord,
        double &noteError
) {
    int tries = 0;
    double chordConsonance = 0;
    double minErrorAchieved = 2;
    shared_ptr<ComplexNote> notePtr;
    shared_ptr<ComplexNote> nearestNote = chord[consonanceTrack];
    int pitch;
    do {
        tries++;
        if (tries > 30) {
            LOG_INFO("could not achieve target consonance: %f, left with %f, chord=%s,%s", targetConsonance
            , consonance(vector<shared_ptr<ComplexNote>>{chord[baseTrack], nearestNote})
            , toName(nearestNote->getNote()).c_str(), toName(chord[baseTrack]->getNote()).c_str());
            break;
        }
        if (chord[consonanceTrack] == nullptr) {
            LOG_ERROR("ein? note consonance is null")
        }
        if (chord[baseTrack] == nullptr) {
            LOG_ERROR("ein? note base is null")
        }
        int increment = int(::std::round(dist(gen)));
        pitch = previousNote->getNote() + increment;  // horizontal relation
        if (pitch < minNote || pitch > maxNote) {
            noteError = 2;
            continue;
        }
        notePtr = shared_ptr<ComplexNote>(new SimpleNote(seconds, pitch, volume, timbre));
        chord[consonanceTrack] = notePtr;
        chordConsonance = consonance(chord);
        LOG_DEBUG("chord: %s has consonance: %f; target: %f", toString(chord, tempo()).c_str(), chordConsonance, targetConsonance);
        noteError = abs(chordConsonance - targetConsonance);
        if (noteError < minErrorAchieved) {
            nearestNote = notePtr;
            minErrorAchieved = noteError;
        }
    } while (noteError > maxAllowedError);     // vertical relation

    noteError = minErrorAchieved;
    chord[consonanceTrack] = nearestNote;
    return nearestNote;
}

/**
* trace:
* add note from scratch
* create iterator to first note
* {
*   correct note
*   create note from previous
*   advance iterator to second note
* }
* correct note...
*
 * @param duration in seconds.
 * @param fitting dissonance will fit this function. domain of [0, 1], and image is best in [0.1, 0.5].
*/
void MusicGenerator::generateDissonance(double duration, function<double (double)> fitting) {
    GenerationConfig config;

    config.duration = duration;
    config.numTracks = 1;

    vector<Decider<float>> durationDeciders{Decider<float>(toSeconds(2, song.tempo))};
    config.noteLengths = durationDeciders;

    function<function<int ()> (normal_distribution<>&, mt19937&)> noteGenerator = [](normal_distribution<> &dist, mt19937 &gen) {
        return [&dist, &gen]() {
            return int(std::round(dist(gen) + 5.0 / 12));  //  correcting out-of-scale down-rounding
        };
    };
    config.pitchGenerators = vector<function<function<int ()> (normal_distribution<>&, mt19937&)>>{noteGenerator};
//        function<int ()> randomNote = [&normal_dist, this]() {
//            return int(std::round(normal_dist(gen) + 5.0/12));  //  correcting out-of-scale down-rounding
//        };
    config.pitchAverages = vector<int>{toNote("E3")};
    config.pitchDeviations = vector<float>{4};
    config.timbres = vector<long unsigned int>{1023};
    config.volumes = vector<float>{0.25};
    config.minNote = 30;
    config.maxNote = 70;

    generate(config);
    LOG_DEBUG("generate Dissonance start");


    int i = 0;
    int consonanceTrack = 0;
    int baseTrack = 1;
    song.tracks.resize(2);
    song.tracks[baseTrack] = song.tracks[0];    // moving to second track the generated one
    song.tracks[consonanceTrack].clear();

    uint32_t timbre = (1 << 10) - 1;    // 1023, to explicitly say that it's all 1s
    double volume = 0.25;
    double deviation = 4;
    std::normal_distribution<> dist(0, deviation);
    double maxAllowedError = 0.10;
    int minNote = 30;
    int maxNote = 70;

    song.tracks[consonanceTrack].emplace_back(new SimpleNote(toSeconds(1, song.tempo), toNote("E4"), volume, timbre));
    Chord chord;
    shared_ptr<ComplexNote> nearestNote = song.tracks[consonanceTrack][0];
    double cursor = 0;
    for (Song::Iterator it(begin()); it != end(); ++it) {
        LOG_DEBUG("---------------iteration %d\n", i);
        chord = *it;
        double noteError;
        double targetConsonance = fitting(cursor);
        shared_ptr<ComplexNote> &previousNote = song.tracks[consonanceTrack][max(i-1, 0)];    // explain max(i-1,0): in the first note there is not a previous one
        nearestNote = nextConsonantNote(consonanceTrack, baseTrack, previousNote, toSeconds(1, song.tempo), timbre, volume,
                minNote, maxNote, dist, targetConsonance, maxAllowedError, chord, noteError);

        song.tracks[consonanceTrack][i] = nearestNote;

        cursor += nearestNote->seconds;
        LOG_DEBUG("cursor to: %f", cursor);

        i++;
        if (i > 100 || cursor >= song.seconds) {
            LOG_DEBUG("enough chords. size of the tracks: %lu, %lu", song.tracks[0].size(), song.tracks[1].size());
            break;
        }

        song.tracks[consonanceTrack].push_back(nearestNote);    // prepare place for next note: this will likely be rewritten
    }
}

void MusicGenerator::generateDissonance(double duration) {
    generateDissonance(duration, duration, 0.05, 0.4);
}

void MusicGenerator::generateDissonance(double duration, double period, double dissonanceMinimum,
        double dissonanceRange) {
    function<double(double)> fitting = [=](double independentVariable) {
        // a cosine in range [dissonanceMinimum, dissonanceMinimum + dissonanceRange]
        return (1+ cos(2*M_PI*independentVariable/period))/2 * dissonanceRange + dissonanceMinimum;
    };
    generateDissonance(duration, fitting);
}
/*

bool isValidConsonance(double consonance, funtion<double ()> evaluation, double targetConsonance, double allowedError) {
    double value = evaluation(consonance);
    if (value)
}
*/

void MusicGenerator::generateHarmonicSequenceTest() {
    double volume = 0.25;
    int track = 0;
//    int octave = 36;
//    int timbre = 3;//timbreDis(gen);
    double noteDuration = 2;
    int note = toNote("C4");
    int maxNote = 60;
    int minNote = 15;

    song.tracks.resize(1);
    int numTimbres = 21;
    int i;
    for (i = 1; i <= numTimbres; ++i) {
        int correctedNote = roundToScale(note, maxNote, minNote);
        song.tracks[track].emplace_back(new SimpleNote(noteDuration, correctedNote, volume, i));
    }
//    LOG_EXP(note, "note: %d")
    song.seconds = max(song.seconds, numTimbres*noteDuration);

}

void MusicGenerator::generateHarmonicNoteTest() {
    double volume = 0.125;
    int track = 0;
    double noteDuration = 2;
    int note = toNote("C3");
    int maxNote = 60;
    int minNote = 15;

    song.tracks.resize(1);
    vector<int> timbres = {8191, 5461, 4681};
    for (auto timbre : timbres) {
        int correctedNote = roundToScale(note, maxNote, minNote);
        song.tracks[track].emplace_back(new SimpleNote(noteDuration, correctedNote, volume, timbre));
    }
    note = toNote("C4");
    for (auto timbre : timbres) {
        int correctedNote = roundToScale(note, maxNote, minNote);
        song.tracks[track].emplace_back(new SimpleNote(noteDuration, correctedNote, volume, timbre));
    }
//    LOG_EXP(note, "note: %d")
    song.seconds = timbres.size()*2*noteDuration;

}

void MusicGenerator::generateHarmonicPatternTest() {
    double volume = 0.25;
    int track = 0;
    double noteDuration = 5;
    song.tracks.resize(1);

    int note = toNote("C3");
    song.tracks[track].emplace_back(instruments::FLUTE.newComplexNote(noteDuration, note, volume));

    note = toNote("C4");
    song.tracks[track].emplace_back(instruments::FLUTE.newComplexNote(noteDuration, note, volume));
    //
//    LOG_EXP(note, "note: %d")
    song.seconds =2*noteDuration;

}

void MusicGenerator::generateTest() {
    double volume = 0.25;
    int track = 0;
//    int octave = 36;
    int timbre = 1;//timbreDis(gen);
    double noteDuration = 1;
    int note = toNote("G2");
    int maxNote = 60;
    int minNote = 15;

    song.tracks.resize(1);
    int i;
    for (i = 0; i < 25; i++, note++) {
        LOG_EXP(note, "note: %d")
        int correctedNote = roundToScale(note, maxNote, minNote);
        song.tracks[track].emplace_back(new SimpleNote(noteDuration, correctedNote, volume, timbre));
    }
    song.seconds = max(song.seconds, i*noteDuration);

}

/**
 * the way this works is by using the iterator.
 * as the iterator ends when there are no more notes, a fake note is created at the end of each track, in order to
 * allow the iterator to point to the place to generate next notes.
 */
vector<bool> MusicGenerator::generateRecurrence(double duration, function<double(double)> fitting) {
    function<void(int&)> negate([](int &value) {
        value = !value;
    });
    Decider<int> createOrRepeat(0, 0, 1, 0.125, 0.125, negate, negate);
    vector<int> notes{toNote("E4"), toNote("E3")};
    vector<double> lengths{toSeconds(0.5, song.tempo), toSeconds(2, song.tempo)};
    float volume(0.125);
    uint32_t timbre((1 << 10) -1);
    song.tracks.resize(notes.size());

    int i(0);
    for (Track &track : song.tracks) {  // first note on all tracks
        track.emplace_back(new SimpleNote(lengths[i], notes[i], volume, timbre));
        i++;
    }

    int consonanceTrack = 0;
    int baseTrack = 1;
    double maxAllowedError = 0.10;
    int minNote = 30;
    int maxNote = 70;
    double deviation = 4;
    std::normal_distribution<> dist(0, deviation);
    function<int ()> randomNote = [&dist, this]() {
        return int(std::round(dist(gen) + 5.0/12));  //  correcting out-of-scale down-rounding
    };
    double recurrencePeriod(8);
    Song::Iterator delayedIt(begin());
    double cursor(0);
    vector<double> cursors{0, 0};
    cout << song.toString() << endl;
    int consonanceIdx = 0;
    int baseIdx = 0;
    vector<bool> recurrenceTracking;
    for (Song::Iterator it(begin()); it != end() && cursor < duration; ++it) {
        Chord chord = *it;

        bool repeat(createOrRepeat() == 1);
        if (repeat && cursor > recurrencePeriod) {
            LOG_INFO("recurrence active")
            recurrenceTracking.push_back(true);
            for (unsigned int j(0); j < chord.size(); j++) {
                song.tracks[j][song.tracks[j].size()-1] = (*delayedIt)[j];  // overwrite last note
            }
        } else {
            LOG_INFO("dissonance active")
            recurrenceTracking.push_back(false);
            // create base track
            chord[baseTrack] = shared_ptr<ComplexNote>(
                    nextGaussianNote(lengths[baseTrack], volume, timbre,
                            song.tracks[baseTrack][max(baseIdx - 1, 0)]->getNote(), minNote, maxNote, randomNote));
            // create consonance track
            shared_ptr<ComplexNote> &previousNote = song.tracks[consonanceTrack][max(consonanceIdx-1, 0)];    // max(i-1,0) explained: in the first note there is not a previous one
            double targetConsonance = fitting(cursor);
            double noteError;
            chord[consonanceTrack] = nextConsonantNote(consonanceTrack, baseTrack, previousNote,
                    lengths[consonanceTrack], timbre, volume, minNote, maxNote, dist, targetConsonance, maxAllowedError,
                    chord, noteError);

            song.tracks[baseTrack][baseIdx] = chord[baseTrack];
            song.tracks[consonanceTrack][consonanceIdx] = chord[consonanceTrack];
            LOG_DEBUG("disonance chosen: %s, %s, in chord: %s, %s, consonance: %f, target: %f, error: %f",
                    toName(song.tracks[consonanceTrack][consonanceIdx]->getNote()).c_str(),
                    toName(song.tracks[baseTrack][baseIdx]->getNote()).c_str(),
                    toName(chord[consonanceTrack]->getNote()).c_str(),
                    toName(chord[baseTrack]->getNote()).c_str(),
                    consonance(chord), targetConsonance, noteError)
        }
        baseIdx++;
        consonanceIdx++;
        if (cursor > recurrencePeriod) {
            ++delayedIt;
        }

        i = 0;
        for (Track &track : song.tracks) {  // did we finish the song?
            cursors[i] += track[track.size()-1]->seconds;
            cursor = max(cursor, cursors[i]);
            track.push_back(track[track.size()-1]);    // repeat last note: prepare place for next note: this will likely be rewritten
            i++;
        }
        LOG_EXP(cursor, "%f")
        cout << song.toString() << endl;
    }
    for (auto &track : song.tracks) {
        track.pop_back();
    }
    song.seconds = cursor;
    return recurrenceTracking;
}

vector<bool> MusicGenerator::generateRecurrence(double duration) {
    function<double(double)> fitting = [duration](double independentVariable) {
        return (1+ cos(2*M_PI*independentVariable/duration))/2 * 0.4 + 0.1;
    };
    return generateRecurrence(duration, fitting);
}

/**
 *
 */
vector<bool> MusicGenerator::generateRecurrence2(double duration,
        function<double(double)> fitting, int deviation, double recurrencePeriod) {
    function<void(int&)> negate([](int &value) {    // std::logical_not returns the value, this lambda uses reference
        value = !value;
    });
    function<void(float&)> multiplyByTwo([](float &value) {
        value *= 2;
    });
    function<void(float&)> divideByTwo([](float &value) {
        value /= 2;
    });
    Decider<int> createOrRepeat(0, 0, 1, 0.05, 0.05, negate, negate);
    vector<int> defaultNotes{toNote("E4"), toNote("E3")};
    vector<Decider<float>> lengths{
            Decider<float>(toSeconds(0.5, song.tempo), toSeconds(0.25, song.tempo), toSeconds(1, song.tempo)
                    , 0.1, 0.1, divideByTwo, multiplyByTwo),
            Decider<float>(toSeconds(2, song.tempo), toSeconds(0.5, song.tempo), toSeconds(2, song.tempo)
                    , 0.1, 0.1, divideByTwo, multiplyByTwo)};

    float volume(0.125);
    uint32_t timbre((1 << 10) -1);
    song.tracks.resize(defaultNotes.size());

    int consonanceTrack = 0;
    int baseTrack = 1;
    double maxAllowedError = 0.10;
    int minNote = 30;
    int maxNote = 70;
    std::normal_distribution<> dist(0, deviation);
    function<int ()> randomNote = [&dist, this]() {
        return int(std::round(dist(gen) + 5.0/12));  //  correcting out-of-scale down-rounding
    };
    Song::Iterator delayedIt(begin());
    double cursor(0);
    vector<double> cursors{0, 0};
    cout << song.toString() << endl;
    int consonanceIdx = 0;
//    int baseIdx = 0;
    vector<bool> recurrenceTracking;
    double noteError;

    Song::Appender appender(&song);
    shared_ptr<ComplexNote> firstBaseNote(nextGaussianNote(lengths[baseTrack](), volume, timbre,
            defaultNotes[baseTrack], minNote, maxNote, randomNote));
    appender.pushBack(baseTrack, firstBaseNote);


    shared_ptr<ComplexNote> previousNote(new SimpleNote(
            lengths[consonanceTrack](), defaultNotes[consonanceTrack], volume, timbre));

    Chord chord;
    chord.resize(2);
    chord[baseTrack] = song.tracks[baseTrack][0];
    chord[consonanceTrack] = previousNote;
    shared_ptr<ComplexNote> firstConsonantNote(nextConsonantNote(
            consonanceTrack, baseTrack, previousNote, lengths[consonanceTrack](), timbre, volume,
            minNote, maxNote, dist, fitting(0), maxAllowedError, chord, noteError));
    appender.pushBack(consonanceTrack, firstConsonantNote);


    Chord draft;
    Chord last;
    LOG_DEBUG("starting recurrence2 loop:");
    int iters = 0;
    cursor = appender.getIterator().getCursors()[0];
//    int appended = 0;
    while (cursor < duration) {
        bool repeat(createOrRepeat() == 1);
        if (repeat && cursor > recurrencePeriod) {
            recurrenceTracking.push_back(true);
            Chord delayedChord = delayedIt.getNextNote();
            draft = appender.getDraftOfNewChord();
            for (unsigned int j = 0; j < draft.size(); j++) {
                if (draft[j] == nullptr) {
                    appender.pushBack(j, delayedChord[j]);
                }
            }
            LOG_INFO("recurrence active, copying %s", toString(delayedChord, tempo()).c_str());
        } else {
            recurrenceTracking.push_back(false);
            last = appender.getLastCompleteChord();
            draft = appender.getDraftOfNewChord();
            if (draft[baseTrack] == nullptr) {
                LOG_DEBUG("choosing base track, cursor=%f, lastchord=%s", cursor,
                        toString((Track) last, song.tempo).c_str());
                last[baseTrack] = shared_ptr<ComplexNote>(nextGaussianNote(
                        lengths[baseTrack](), volume, timbre, last[baseTrack]->getNote(),
                        minNote, maxNote, randomNote));
                appender.pushBack(baseTrack, last[baseTrack]);
            }
            draft = appender.getDraftOfNewChord();
            if (draft[consonanceTrack] == nullptr) {
                LOG_INFO("choosing consonance track, note %d cursor=%f, lastchord=%s", ++consonanceIdx, cursor,
                        toString((Track) last, song.tempo).c_str())

                draft[consonanceTrack] = previousNote;  // nextConsonantNote doesn't admit nulls
                if (draft[baseTrack] == nullptr) {
                    draft[baseTrack] = last[baseTrack];
                }
                if (draft[consonanceTrack] == nullptr) {
                    LOG_ERROR("draft[consonanceTrack] is null");
                }
                if (draft[baseTrack] == nullptr) {
                    LOG_ERROR("draft[baseTrack] is null");
                }
                if (last[consonanceTrack] == nullptr) {
                    LOG_ERROR("last[consonanceTrack] is null");
                }
                appender.pushBack(consonanceTrack, shared_ptr<ComplexNote>(nextConsonantNote(
                        consonanceTrack, baseTrack, last[consonanceTrack], lengths[consonanceTrack](), timbre,
                        volume, minNote, maxNote, dist, fitting(cursor), maxAllowedError, draft, noteError)));
            }
        }
        if (++iters > 1000) {
            LOG_ERROR("Loop too long, likely an endless loop, exiting...");
            assert(iters < 1000);
            return recurrenceTracking;
        }
        cursor = appender.getIterator().getCursors()[0];
        if (cursor >= recurrencePeriod) {
            ++delayedIt;
        }
    }
    song.seconds = cursor;
    song.tracks[0].pop_back();
    song.tracks[1].pop_back();

    return recurrenceTracking;
}

vector<bool> MusicGenerator::generateRecurrence2(double duration, int deviation) {
    return generateRecurrence2(duration, deviation, duration/2, 0.2, 0.3);
}

vector<bool> MusicGenerator::generateRecurrence2(double duration, int deviation,
        double recurrencePeriod, double dissonanceRange, double minimumDissonance) {
    function<double(double)> fitting = [=](double independentVariable) {
        return (1 + cos(2 * M_PI * independentVariable / recurrencePeriod)) / 2 * dissonanceRange +
               minimumDissonance;
    };
    return generateRecurrence2(duration, fitting, deviation, recurrencePeriod);

}

/*

void MusicGenerator::generateScaled(float seconds, float cyclePeriod, float maxDissonance, float minDissonance) {
//    vector<string> notes = {"A", "B", "C", "D", "E", "F", "G"};
    float noteDuration;
    float trackDuration = 0;
    float volume;
    int i, track;
    int amplitude = 11;
    int maxNote = 60;
    int minNote = 15;
    int octave;
    int timbre;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<short int> noteDis(0, amplitude);
    std::uniform_int_distribution<int> timbreDis(0, 65535);
    int highMean = "E3"_n;
    int lowMean = "E2"_n;
    std::normal_distribution<> normal_dist_high(highMean, 7);
    std::normal_distribution<> normal_dist_low(lowMean, 5);

//    cout << "song.tracks.capacity() = " << song.tracks.capacity() << endl;
//    cout << "song.tracks.size() = " << song.tracks.size() << endl;
    song.tracks.resize(2);
    noteDuration = 0.5;
    volume = 0.125;
    track = 0;
    octave = 24;
    timbre = timbreDis(gen);
    song.tracks[track].push_back(SimpleNote(noteDuration
            , roundToScale(std::round(normal_dist_low(gen)), maxNote, minNote)
            , volume, timbre));
    for (float cursor = noteDuration, i = 0; cursor < seconds; cursor += noteDuration, i++) {
        int random = std::round(normal_dist_low(gen));
//        cout << "random = " << random << endl;
        int rawNote = song.tracks[track][i].note + random - lowMean;
        int correctedNote = roundToScale(random, maxNote, minNote);
        song.tracks[track].push_back(SimpleNote(noteDuration, correctedNote, volume, timbre));
        trackDuration += noteDuration;
    }

    seconds = max(seconds, trackDuration);
    trackDuration = 0;
    noteDuration = 0.125;
    volume = 0.25;
    track = 1;
    octave = 36;
    timbre = timbreDis(gen);
    song.tracks[track].push_back(SimpleNote(noteDuration
            , roundToScale(std::round(normal_dist_high(gen)), maxNote, minNote)
            , volume, timbre));
    for (float cursor = noteDuration, i = 0; cursor < seconds; cursor += noteDuration, i++) {
        int random = std::round(normal_dist_high(gen));
//        cout << "random = " << random << endl;
        int rawNote = song.tracks[track][i].note + random - highMean;
        int correctedNote = roundToScale(random, maxNote, minNote);
        song.tracks[track].push_back(SimpleNote(noteDuration, correctedNote, volume, timbre));
        trackDuration += noteDuration;
    }
    seconds = max(seconds, trackDuration);

}
*/

/*
void MusicGenerator::generate(float seconds, float cyclePeriod, float maxDissonance, float minDissonance) {
    float noteDuration;
    double trackDuration = 0;
    float volume;
    int i = 0;
    song.tracks.resize(2);
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<short int> dis(0, 7);
    std::uniform_int_distribution<short int> dis2(0, 7);

//    cout << "song.tracks.capacity() = " << song.tracks.capacity() << endl;
//    cout << "song.tracks.size() = " << song.tracks.size() << endl;
    noteDuration = 1;
    volume = 0.125;
    for (float cursor = 0; cursor < seconds; cursor += noteDuration, i++) {
        song.tracks[0].push_back(SimpleNote(noteDuration, "C2"_n + dis2(gen), volume, 0));
        trackDuration += noteDuration;
    }
    seconds = max(seconds, trackDuration);
    trackDuration = 0;
    noteDuration = 0.25;
    volume = 0.25;
    for (float cursor = 0; cursor < seconds; cursor += noteDuration, i++) {
        song.tracks[1].push_back(SimpleNote(noteDuration, "C3"_n + dis(gen), volume, 0));
        trackDuration += noteDuration;
    }
    seconds = max(seconds, trackDuration);
}
*/
void MusicGenerator::write(shared_ptr<SongWriter> exporter) {
    exporter->write(song);
}

Song &MusicGenerator::getSong() {
    return song;
}

Song::Iterator MusicGenerator::begin() {
    return song.begin();
}

Song::Iterator MusicGenerator::end() {
    return song.end();
}

};  // gemu
};  // randomize
