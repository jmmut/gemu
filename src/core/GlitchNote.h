/**
 * @file GlitchNote.h
 * @author jmmut 
 * @date 2015-10-09.
 */

#ifndef GEMU_GLITCHNOTE_H
#define GEMU_GLITCHNOTE_H

#include <stdexcept>
#include "core/ComplexNote.h"
namespace randomize {
namespace gemu {


class GlitchNote : public ComplexNote {
private:
    std::function<double (double)> callback;

public:
    GlitchNote() : callback(nullptr) { }

    GlitchNote(double seconds, const std::function<double (double)> &callback)
            : callback(callback) {
        this->seconds = seconds;
    }

    GlitchNote(double seconds, const std::function<unsigned char (double)> &userCallback) {
        this->seconds = seconds;
        callback = [userCallback](double value) -> double {
            unsigned char userValue = userCallback(value);
            // normalize amplitude to [0, 1]. `(sizeof(value) *8)` is the number of bits of `value`, and `(1 << numBits) -1` is max value
            // TODO encapsulate this in the glitch class. if a double is asked, do this, else give native integers
//        double normalized = float(value) / ((1 << (sizeof(value) * 8)) - 1);
            double normalized = float(userValue) / 255.0;
            return normalized;
        };
    }

    virtual double value(double time) const override;

    virtual std::string name() const override;

    virtual int getNote() const override;
};

};  // gemu
};  // randomize

#endif //GEMU_GLITCHNOTE_H
