#ifndef _GEMU_H_
#define _GEMU_H_

#include <array>
#include <map>
#include <iostream>
#include <fstream>
#include <cmath>
#include <memory>
#include <exception>
#include <sstream>
#include "utils/exception/StackTracedException.h"
#include "log/log.h"



namespace randomize {
namespace gemu {


constexpr double SQRT_12_2 = 1.05946309435929526456;
const int SEMITONES_PER_OCTAVE = 12;
constexpr double FREQUENCY_A0 = 27.5;
constexpr double FREQUENCY_C0 = FREQUENCY_A0*SQRT_12_2*SQRT_12_2*SQRT_12_2*0.5;
extern std::array<double, 12*8> frequencies;
extern std::map<int, std::string> noteNames;
const double FLOAT_DELTA = 0.00001;

std::array<double, 12*8> generateFrequencies();

/*
constexpr int operator "" _n(const char *str, std::size_t len) {
//    static_assert(len >= 2 || len <= 3, "note must be format [A-G][b#]?[0-7]");
    return (static_cast<int>(str[len == 2?1:2]) - 48) * 12
            + (str[0] == 'A'? 0
            : str[0] == 'B'? 2
                    : str[0] == 'C'? 3
                            : str[0] == 'D'? 5
                                    : str[0] == 'E'? 7
                                            :str[0] == 'F'? 8
                                                    :str[0] == 'G'? 10
                                                            :0)
            + (len != 3? 0
            : str[1] == '#'? 1
                    : str[1] == 'b'? -1
                            : 0);
}
*/


int toNote(std::string name);
std::string toFullName(int note, double duration);
std::string toName(int note);
double toFrequency(int note);
int toNote(double frequency);
double toFrequency(std::string name);
std::string toName(double frequency);

int toOctave(int note);

double toPulses(double seconds, float tempo);
double toSeconds(double pulses, float tempo);

bool writeToFile(std::string name, std::string content);
bool writeToFile(std::string name, std::istream &content);
bool writeToFile(std::string name, std::function<void(std::ofstream& file)> writeFunction);
void openFileOrThrow(std::string name, std::ofstream &f);

};  // gemu
};  // randomize


#endif  // _GEMU_H_


