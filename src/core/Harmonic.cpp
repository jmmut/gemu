#include "core/Harmonic.h"

namespace randomize {
namespace gemu {

Harmonic::Harmonic(const double &frequencyCoefficient, const double &amplitudeCoefficient)
        : frequencyCoefficient(std::unique_ptr<LinearInterpolator>(new LinearInterpolator({frequencyCoefficient}))),
        amplitudeCoefficient(std::unique_ptr<LinearInterpolator>(new LinearInterpolator({amplitudeCoefficient}))) {

    //    setFrequencyCoef(frequencyCoefficient);
//    setAmplitudeCoef(amplitudeCoefficient);
}

void Harmonic::setFrequencyCoef(double const &coef) {
    frequencyCoefficient->setPoints({coef});
}

//void Harmonic::setFrequencyCoef(Interpolator &coef) {
//    frequencyCoefficient = std::unique_ptr<LinearInterpolator>(new LinearInterpolator({coef}));
//}

void Harmonic::setAmplitudeCoef(double const &coef) {
    amplitudeCoefficient->setPoints({coef});
}

//void Harmonic::setAmplitudeCoef(Interpolator &coef) {
//    amplitudeCoefficient = std::unique_ptr<LinearInterpolator>(new LinearInterpolator({coef}));
//}

double Harmonic::amplitudeCoef(double time) const {
    return amplitudeCoefficient->value(time);
}

double Harmonic::frequencyCoef(double time) const {
    return frequencyCoefficient->value(time);
}

};  // gemu
};  // randomize
