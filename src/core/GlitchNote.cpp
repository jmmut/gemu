/**
 * @file GlitchNote.cpp
 * @author jmmut 
 * @date 2015-10-09.
 */

#include <typeinfo>
#include "GlitchNote.h"

double randomize::gemu::GlitchNote::value(double time) const {
    if (callback == nullptr) {
        throw std::invalid_argument("GlitchNote needs a callback to generate values");
    }
    return callback(time);
}

std::string randomize::gemu::GlitchNote::name() const {
    return typeid(callback).name();
}

int randomize::gemu::GlitchNote::getNote() const {
    return -1;
}
