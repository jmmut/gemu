//
// Created by jmmut on 2015-04-09.
//

#ifndef _GEMU_HARMONICPATTERN_H_
#define _GEMU_HARMONICPATTERN_H_

#include <memory>
#include "core/ComplexNote.h"
#include "core/Harmonic.h"
#include "util/Interpolator.h"

namespace randomize {
namespace gemu {



class HarmonicPattern {
public:
    std::vector<Harmonic> harmonics;


    HarmonicPattern(std::vector<Harmonic> const &harmonics) : harmonics(harmonics) { }
    std::shared_ptr<ComplexNote> newComplexNote(double duration, int note, float volume) const;
};


namespace instruments {

extern const HarmonicPattern SINE;
extern const HarmonicPattern SQUARE;
extern const HarmonicPattern SOFT_QUARE;
extern const HarmonicPattern FLUTE;

};  // instruments

};  // gemu
};  // randomize

#endif //_GEMU_HARMONICPATTERN_H_
