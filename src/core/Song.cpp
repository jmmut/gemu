/**
 * @file Song.cpp
 * @author jmmut 
 * @date 2015-09-13.
 */

#include <util/Walkers.h>
#include "Song.h"

namespace randomize {
namespace gemu {

using std::stringstream;
using std::shared_ptr;
using std::vector;

std::string Song::toString() {
    stringstream stringSong;
    int i = 0;
    for (const Track &track : tracks) {
        stringSong << "\ntrack " << i++ << ":\n";
        stringSong << gemu::toString(track, tempo);
    }
    stringSong << "\nend of song.\n";
    return stringSong.str();
}

std::string Song::toVerboseString() {
    stringstream stringSong;
    int i = 0;
    for (const Track &track : tracks) {
        stringSong << "\n\ntrack " << i++ << ":\n";
        for (const shared_ptr<ComplexNote> &note : track) {
            double frequency = note->getBaseFrequency();
            int noteNum = toNote(frequency);
            stringSong << "<" << toName(noteNum) << ">" << toPulses(note->seconds, tempo) << ": {";
            for (const Harmonic &harmonic : note->getHarmonics()) {
                stringSong << "(" << harmonic.frequencyCoef(0)*frequency << ", " << harmonic.amplitudeCoef(0) << "), ";
            }
            stringSong << "}\n";
        }
    }
    stringSong << "\n\nend of song.\n";
    return stringSong.str();
}

// --------- Iterator ----------

Song::Iterator Song::begin() {
    return Song::Iterator(this);
}

Song::Iterator Song::end() {
    return Song::Iterator(this, seconds+1);
}


// Iterator
Song::Iterator::Iterator(Song * song) : song(song) {
    for (std::size_t i = 0; i < song->tracks.size(); ++i) {
        chord.push_back(0); // note number 0
        instants.push_back(0);   // cursor time = 0
        cursors.push_back(0);
    }
}

Song::Iterator::Iterator(Song *song, double instant)
        : song(song) {
    Track *track;
    for (unsigned int i = 0; i < song->tracks.size(); ++i) {
        track = &song->tracks[i];
        if (instant >= song->seconds) {
            chord.push_back(track->size());    // put index after last note
            this->instants.push_back(0);
            cursors.push_back(0);
        } else {
            std::logic_error exception("Song::Iterator with (instant != end): not implemented yet. do not use");
            throw exception;
            // TODO implement this
//            double cursor = 0;
//            int j = 0;
//            while (j < track->size() && cursor < instant) {
//                ++j;
//                cursor += (*track)[i]->seconds;
//            }
//            this->chord.push_back()
//            this->instant.push_back(instant);
        }
    }
}

bool Song::Iterator::operator!=(Song::Iterator it2) {
    if (this->song->tracks.size() != it2.song->tracks.size()) { // if chord has different number of tracks
        return true;
    }
    for (unsigned int i = 0; i < this->song->tracks.size(); i++) { // if some pointer to note is different
        if (chord[i] != it2.chord[i]) {
            return true;
        }
    }

    return false;
}

bool Song::Iterator::getMinRemainingNoteTime(double &seconds) {
    vector<double> remain = getRemainingNoteTime();
    auto min_it = std::min_element(std::begin(remain), std::end(remain));
    if (min_it != std::end(remain)) {
        seconds =*min_it;
        return true;
    }
    return false;
}

vector<double> Song::Iterator::getRemainingNoteTime() {
    vector<double> remain;

    for (unsigned int i = 0; i < chord.size(); ++i) {    // find out remaining time of each current note
        if (chord[i] < song->tracks[i].size()) {
            remain.push_back(song->tracks[i][chord[i]]->seconds - instants[i]);
        }
    }

    return remain;
}

void Song::Iterator::operator++() {
    vector<double> remain(getRemainingNoteTime());

    if (remain.size() > 0) {    // increase time of the chord to the next note
        auto min_it = std::min_element(std::begin(remain), std::end(remain));   // which note ends sooner?
        std::size_t index = min_it - std::begin(remain);
        double dt = *min_it;

        cursors[index] += song->tracks[index][chord[index]]->seconds;
        instants[index] = 0; // shorter note is consumed
        chord[index]++; // the track of the shorter note advances to the next note

        for (std::size_t i = 0; i < remain.size(); ++i) {
            if (i != index) {   // the other notes increase their instant
                instants[i] += dt;
                if (instants[i] >= song->tracks[i][chord[i]]->seconds) {   // any of the other notes are consumed too
                    cursors[i] += song->tracks[i][chord[i]]->seconds;
                    instants[i] = 0; // this can only happen if several notes shared the minimum remaining time
                    chord[i]++;
                }
            }
        }
    }

    for (size_t i = 0; i < chord.size(); ++i) {    // if any track is finished, finish all tracks
        if (chord[i] >= song->tracks[i].size()) {
            finished();
            return;
        }
    }
}

/**
 * TODO: see if a Chord&& can be returned
 */
Chord Song::Iterator::operator*() {
    Chord returnedChord;
    int i = 0;
    for (std::size_t noteIndex : chord) {
//        assert(noteIndex < song->tracks[i].size());
        if (noteIndex >= song->tracks[i].size()) {
            throw std::out_of_range("trying to iterate past the end of the song: chord note indices: "
                + randomize::utils::container_to_string(chord)
                + ", tracks sizes : "
                + randomize::utils::container_to_string(
                    change<int, Track>(song->tracks, [] (Track vec) {return vec.size();}))
                + ". A note index should be always strictly less than the track size. If this message is printed, "
                + "there is a bug in Song::iterator or it's being used wrong."
            );
        }
        returnedChord.push_back(song->tracks[i][noteIndex]);
        i++;
    }
    return returnedChord;
}

vector<double> Song::Iterator::getCursors() {
    std::vector<double> times(chord.size());
    for (std::size_t i = 0; i < chord.size(); ++i) {
        times[i] = cursors[i] + instants[i];
    }
    return times;
}

vector<double> Song::Iterator::getInstants() {
    return instants;
}

void Song::Iterator::finished() {
    for (std::size_t i = 0; i < chord.size(); ++i) {
        chord[i] = (int) song->tracks[i].size();    // mark as finished, after every track
    }
}

Chord Song::Iterator::getNextNote() {
    vector<double> remain(getRemainingNoteTime());

    if (remain.size() != song->tracks.size()) {
        LOG_ERROR("iterator error, getnextNote, sizes dont match");
    }

    auto min_it = std::min_element(std::begin(remain), std::end(remain));

    Chord returnedChord;

    for (std::size_t i = 0; i < song->tracks.size(); ++i) {
        if (std::abs(remain[i] - (*min_it)) < FLOAT_DELTA) { // notes that end first in the current chord
            if (song->tracks[i].size() > chord[i]) {
                returnedChord.push_back(song->tracks[i][chord[i]]);
            } else {
                returnedChord.push_back(shared_ptr<ComplexNote>());
            }
        } else {    // notes that end later
            returnedChord.push_back(shared_ptr<ComplexNote>());
        }
    }
    return returnedChord;
}
//////////////////////// Appender
/*
Song::Appender::Appender(MusicGenerator *music)
        : this(music, Chord{nullptr, nullptr}) {

}*/

Song::Appender::Appender(Song * song)
        : song(song), it(song->begin()) {
}

Chord Song::Appender::getDraftOfNewChord() {
    vector<double> remain(it.getRemainingNoteTime());

    if (remain.size() != song->tracks.size()) {
        LOG_ERROR("appender error, getDraftOfNewChord, sizes dont match");
    }

    auto min_it = std::min_element(std::begin(remain), std::end(remain));

    Chord chord;

    for (unsigned int i = 0; i < song->tracks.size(); ++i) {
        if (std::abs(remain[i] - (*min_it)) < FLOAT_DELTA) {
            // notes that end first in the last chord
            if (it.chord[i] + 1 < song->tracks[i].size()) {
                // the next note is available
                chord.push_back(song->tracks[i][it.chord[i] + 1]);
            } else {
                chord.push_back(shared_ptr<ComplexNote>());
            }
        } else {
            // notes that end later, and are still valid from the current iterator
            chord.push_back((*it)[i]);
        }
    }

    return chord;
}

void Song::Appender::pushBack(unsigned int track, shared_ptr<ComplexNote> note) {
    if (song->tracks.size() <= track) {
        stringstream msg;
        msg << "Song::Appender::pushBack: song->tracks.size()=" << song->tracks.size()
        << ", and tried to write in tracks[" << track << "]. hint: tracks.push_back(Track())";
        throw std::out_of_range(msg.str());
    }
    song->tracks[track].push_back(note);


    // advance iterator if we can
    vector<double> remain(it.getRemainingNoteTime());

    if (remain.size() != song->tracks.size()) {
//        LOG_ERROR("appender error, pushBack, sizes dont match: ");
//        LOG_EXP(remain.size(), "%ld");
//        LOG_EXP(song->tracks.size(), "%ld");
    } else {
        auto min_it = std::min_element(std::begin(remain), std::end(remain));
        bool shouldAdvance = true;
        // now we should advance the iterator only if all the shortest notes have notes after them

        for (unsigned int i = 0; i < song->tracks.size(); ++i) {
            if (std::abs(remain[i] - (*min_it)) < FLOAT_DELTA) {
                // if this is one of those shortest notes

                if (it.chord[i] + 1 >= song->tracks[i].size()) {

                    // this shortest note is the last in this track: we can not advance more
                    shouldAdvance = false;
                }
            }
        }
        if (shouldAdvance) {
            ++it;
        }
    }

}

Chord Song::Appender::getLastCompleteChord() {
    return *it;
}

shared_ptr<ComplexNote> Song::Appender::getLast(int track) {
    return (*it)[track];
}

};  // gemu
};  // randomize


