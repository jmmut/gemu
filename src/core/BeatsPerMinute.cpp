/**
 * @file BeatsPerMinute.cpp
 * @author jmmut 
 * @date 2016-05-16.
 */

#include <vector>
#include <deque>
#include "BeatsPerMinute.h"

using std::function;
using std::vector;
using std::deque;

namespace randomize {
namespace gemu {

double BeatsPerMinute::bpm(double periodInSeconds) {
    return 60.0/periodInSeconds;
}

double BeatsPerMinute::periodInSeconds(double periodInSamples) {
    return periodInSamples / wavSong.getHeader().samplerate * wavSong.getHeader().numchannels;
}

struct Trio {
    Trio(function<void(size_t, float, float, float)> single) : Trio(0, single) { }
    Trio(size_t cursor, function<void(size_t, float, float, float)> single)
            : cursor(cursor), previous(), iterationsToSkip(2), next(single) { }

    void operator()(size_t index, float sample) {
        if (iterationsToSkip > 0) {
            iterationsToSkip--;
        } else {
            next(index-1, previous[0], previous[1], sample);
            previous.pop_front();
        }
        previous.push_back(sample);
    }
    size_t cursor;
    deque<float> previous;
    int iterationsToSkip;
    function<void(size_t, float, float, float)> next;
};

double periodInSamples(size_t nesting, function<void(function<void(size_t, float)>)> iteratorFunction) {

    auto maxExtractor = [](function<void(size_t, float)> nextFunction) {
        return [nextFunction](size_t index, float previousSample, float sample, float nextSample) {
            if (sample > previousSample && sample > nextSample) {
                nextFunction(index, sample);
            }
        };
    };

    auto ignoreSecond = [] (function<void(size_t)> nextFunction)
            -> function<void(size_t, float)> {
        return [nextFunction] (size_t index, float sample) {
            nextFunction(index);
        };
    };

    size_t previous = 0;
    auto difference = [&previous] (function<void(size_t)> nextFunction) -> function<void(size_t)> {
        return [nextFunction, &previous] (size_t index) {
            nextFunction(index - previous);
            previous = index;
        };
    };


    double accum = 0;
    int elems = 0;
    bool first = true;
    auto average = [&] (size_t elem) {
        if (not first) {
            accum += elem;
            elems++;
        }
        first = false;
    };

    vector<function<void(size_t, float)>> compounds;
    compounds.push_back(ignoreSecond(difference(average)));
    for (size_t i = 0; i < nesting; ++i) {
        compounds.push_back(Trio(maxExtractor(compounds.back())));
    }

//    iteratorFunction(Trio(maxExtractor(Trio(maxExtractor(ignoreSecond(difference(average)))))));
    iteratorFunction(compounds.back());

    double mean = accum / elems;

    return mean;
}

double BeatsPerMinute::periodInSamples(size_t nesting) {
    return gemu::periodInSamples(nesting,
            [this](function<void(size_t index, float sample)> nextFunction) {
                wavSong.forEachSampleIndexed(start + 1, end, nextFunction);
            }
    );
}



};  // gemu
};  // randomize

