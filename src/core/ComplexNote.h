#ifndef __ComplexNote_H_
#define __ComplexNote_H_

#include <cmath>
#include <vector>
#include <array>
#include <map>
#include <iostream>
#include <string>
#include <sstream>
#include "core/Gemu.h"
#include "core/Harmonic.h"
#include "util/Interpolator.h"

namespace randomize {

namespace gemu {


/**
* Defines a complex note.
*
* Gives the value of the sound wave depending on the instant. Allows as configuration the base frequency, the base
* volume, and possibly transitions from those based on time. Also the timbre may be defined giving the frequencies of
* the harmonics and their volumes.
*/
class ComplexNote {
public:
    static const int DEFAULT_AMPLITUDE = 1;
    static const int DEFAULT_TIMBRE = 1;
    double seconds;
    std::shared_ptr<Interpolator> frequency;
    std::shared_ptr<Interpolator> amplitude;
private:
    std::vector<Harmonic> harmonics;
public:

    /**
    * calculates the value of the wave in the instant <t>.
    *
    * main function of this class.
    */
    virtual double value(double time) const;
    virtual std::string name() const;
    virtual int getNote() const;

    void setConfig(double duration, std::shared_ptr<Interpolator> frequency,
            std::shared_ptr<Interpolator> amplitude, std::vector<Harmonic> harmonics);

    void setConfig(double duration, double frequency, double amplitude, unsigned int harmonics);

    void setConfig(double duration, double frequency);

    double getBaseFrequency() const;

    std::vector<Harmonic> const & getHarmonics() const;

    std::vector<double> getFrequencies() const;

    static std::vector <Harmonic> buildHarmonics(unsigned int harmonics);
};

//using Track = std::vector<std::shared_ptr<ComplexNote>>;
//using Chord = std::vector<std::shared_ptr<ComplexNote>>;
class Track;
using Chord = Track;

std::string toString(const Track &track, float tempo);

//std::string Track::toString(float tempo) const;
//
//std::string Track::toString(float tempo) const {
//        return randomize::gemu::toString(*this, tempo);
//    }

class Track : public std::vector<std::shared_ptr<ComplexNote>> {
public:
    std::string toString(float tempo) const {
        return randomize::gemu::toString(*this, tempo);
    }
};

};  // gemu
};  // randomize

#endif //__ComplexNote_H_
