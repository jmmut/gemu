#ifndef __Dissonance_H_
#define __Dissonance_H_

#include <vector>
#include "core/ComplexNote.h"

namespace randomize {
namespace gemu {

static const double EPSILON_DISSONANCE = SQRT_12_2 * 0.03;   // 3 cents of a semitone


double consonance(std::vector<std::shared_ptr<ComplexNote>> notes);
double dissonance(std::vector<std::shared_ptr<ComplexNote>> notes,
        std::vector<double> instants, double epsilon);
//double consonance(const std::vector<std::shared_ptr<ComplexNote>> &notes);
//double dissonance(const std::vector<std::shared_ptr<ComplexNote>> &notes,
//        const std::vector<double> &instants, double epsilon);
bool isEquivalentFrequency(double firstFrequency, double secondFrequency, double epsilon = EPSILON_DISSONANCE);


};  // gemu
};  // randomize


#endif //__Dissonance_H_
