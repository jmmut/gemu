#!/bin/bash

if [ "$1" == "" ]; then
	DURATION=15;
else
	DURATION=$1;
fi

NOMBRE=$(name-generator.sh);
FULLNOMBRE=txts/${NOMBRE};
./jmmut_gemu_src_core_main $DURATION ${FULLNOMBRE};
lilypond ${FULLNOMBRE}.ly;
caja ${FULLNOMBRE}.wav;
caja ${FULLNOMBRE}.pdf;
